import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Menu} from "../components/menu";
import {TreeNode} from "primeng/api";
import {MenuPermissionModel} from "../../feature/menus";

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient) {
  }

  findAllMenus(): Observable<Menu[]> {
    return this.http.get<Menu[]>(`${environment.apiUrl}/api/auth/menus`);
  }

  findAllMyMenus(): Observable<Menu[]> {
    return this.http.get<Menu[]>(`${environment.apiUrl}/api/auth/menus/my`);
  }

  findAllMenusForTreeNode(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/menus/tree_view`);
  }

  updateBudgetHeadStatus(status: string, menuId: number): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/menus/${menuId}/${status}`, null);
  }

  fetchAllMenusForPermission(): Observable<MenuPermissionModel[]> {
    return this.http.get<MenuPermissionModel[]>(`${environment.apiUrl}/api/auth/menus/permission`);
  }

  submitMenuPermission(detail: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/menus/permission`, detail);
  }

  findAllMenusPermissionByUserGroupId(event: any): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/menus/permission/${event}`);
  }

  updateMenu(value: any, selectedMenuId: number): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/menus/${selectedMenuId}`, value);
  }

  updateMenuPermission(detail: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/menus/permission/update`, detail);
  }
}
