export  class Menu {
  menuId: number;
  name: string;
  url: string;
  icon: string;
  menuResources: Menu[];
}
