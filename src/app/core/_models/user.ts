import {Role} from './role';

export class User {
  accessToken?:string;
  tokenType: string;
  role: Array<any>;
  userId: number;
  fullName: string;
  branchId: number;
  canTransferBudget: boolean;
  canReject: boolean;

}
