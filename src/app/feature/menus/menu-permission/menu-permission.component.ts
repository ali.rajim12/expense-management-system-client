import {Component, OnInit} from '@angular/core';
import {MenuService} from "../../../core/_services";
import {Menu} from "../../../core/components/menu";
import {ColumnName} from "../../../shared/constant/column-name";
import {MenuPermissionModel} from "../model/menu.permission.model";
import {TreeNode} from "primeng/api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserGroup} from "../../users/model/user-group.model";
import {UserGroupService} from "../../users/service/user-group.service";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {CustomMessages} from "../../../shared/constant/custom-messages";

@Component({
  selector: 'app-menu-permission',
  templateUrl: './menu-permission.component.html',
  styleUrls: ['./menu-permission.component.sass']
})
export class MenuPermissionComponent implements OnInit {
  columnName: any[];
  treeViewMenus: TreeNode[];
  selectedNodes: TreeNode[] = [];
  showConfirmation: boolean = false;
  confirmationMenuForm: FormGroup;
  loading: boolean = false;
  selectSomethingMessage: boolean = false;
  userGroups: UserGroup[];
  submitted: boolean = false;
  selectedMenuIds: number[] = [];


  constructor(private menuService: MenuService,
              private userGroupService: UserGroupService,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder) {
    this.confirmationMenuForm = fb.group({
      userGroup: [null, Validators.required]
    })
  }

  get f() {
    return this.confirmationMenuForm.controls;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.MENU_PERMISSION_TABLE_HEADINGS;
    // this.reloadTable();
    this.reloadTreeMenuTable();
    this.getAllUserGroups();
  }

  reloadTreeMenuTable() {
    this.menuService.findAllMenusForTreeNode()
      .subscribe((data) => {
        this.treeViewMenus = data.data;
      }, error => {
        console.log(error)
      });
  }

  getAllUserGroups() {
    this.userGroupService.getAllUserGroups().subscribe((data) => {
      this.userGroups = data;
    }, error1 => {
      console.log(error1);
    });
  }

  resetForm() {
    this.loading = false;
    this.showConfirmation = false;
    this.confirmationMenuForm.reset();
  }

  getMenuIds() {
    this.selectedNodes.forEach((value => {
      this.selectedMenuIds.push(value.data.menuId)
    }))
  }

  submit() {
    this.submitted = true;
    if (this.confirmationMenuForm.invalid) {
      return;
    }
    this.loading = true;
    // const selectedMenuIds = this.selectedNodes.map(nodes => ({
    //   menuId: nodes.data.menuId
    // }));
    this.getMenuIds();
    let detail = {
      userGroupId: this.confirmationMenuForm.value.userGroup.userGroupId,
      menuId: this.selectedMenuIds
    }
    this.menuService.submitMenuPermission(detail)
      .subscribe((data) => {
        this.loading = false;
        this.showConfirmation = false;
        this.customMessageService.sendSuccessErrorMessage(data.success_message,
          'success', true);
        this.selectedMenuIds = [];
        this.selectedNodes = [];
      }, error => {
        this.showConfirmation = false;
        this.confirmationMenuForm.reset();
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(error, 'error',
          true);
        this.selectedMenuIds = [];
        this.selectedNodes = [];
      })
  }

  addMenuPermission() {
    if (this.selectedNodes) {
      this.selectSomethingMessage = false;
      this.showConfirmation = true;
    } else {
      this.selectSomethingMessage = true;
      return;
    }
  }

  trackParent(event: any) {
    if (event[event.length - 1].child === true) {
      this.selectedNodes.push(event[event.length - 1].parent);
    }
    console.log(this.selectedNodes);
  }
}
