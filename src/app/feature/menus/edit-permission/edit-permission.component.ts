import { Component, OnInit } from '@angular/core';
import {TreeNode} from "primeng/api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserGroup} from "../../users/model/user-group.model";
import {MenuService} from "../../../core/_services";
import {UserGroupService} from "../../users/service/user-group.service";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {ColumnName} from "../../../shared/constant/column-name";

@Component({
  selector: 'app-edit-permission',
  templateUrl: './edit-permission.component.html',
  styleUrls: ['./edit-permission.component.sass']
})
export class EditPermissionComponent implements OnInit {

  columnName: any[];
  treeViewMenus: TreeNode[];
  selectedNodes: TreeNode[] = [];
  showConfirmation: boolean = false;
  confirmationMenuForm: FormGroup;
  loading: boolean = false;
  selectSomethingMessage: boolean = false;
  userGroups: UserGroup[];
  submitted: boolean = false;
  selectedMenuIds: number[] = [];
  userGroup: number;
  dataArray: string[] = [];
  selectedGroupId: number;


  constructor(private menuService: MenuService,
              private userGroupService: UserGroupService,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder) {
    this.confirmationMenuForm = fb.group({
      userGroup: [null]
    })
  }

  get f() {
    return this.confirmationMenuForm.controls;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.MENU_PERMISSION_TABLE_HEADINGS;
    // this.reloadTable();
    this.reloadTreeMenuTable();
    this.getAllUserGroups();
    this.dataArray = ['name', 'url', 'icon', 'status'];
  }

  reloadTreeMenuTable() {
    this.menuService.findAllMenusForTreeNode()
      .subscribe((data) => {
        this.treeViewMenus = data.data;
      }, error => {
        this.customMessageService.sendSuccessErrorMessage(error, 'error',
          false);
      });
  }

  getAllUserGroups() {
    this.userGroupService.getAllUserGroups().subscribe((data) => {
      this.userGroups = data;
    }, error1 => {
      console.log(error1);
    });
  }

  resetForm() {
    this.loading = false;
    this.showConfirmation = false;
    this.confirmationMenuForm.reset();
  }

  getMenuIds() {
    this.selectedNodes.forEach((value => {
      this.selectedMenuIds.push(value.data.menuId)
    }))
  }
  submit() {
    this.submitted = true;
    if (this.confirmationMenuForm.invalid) {
      return;
    }
    this.loading = true;
    // const selectedMenuIds = this.selectedNodes.map(nodes => ({
    //   menuId: nodes.data.menuId
    // }));
    this.getMenuIds();
    let detail = {
      userGroupId:  this.selectedGroupId,
      menuId: this.selectedMenuIds
    }
    this.menuService.updateMenuPermission(detail)
      .subscribe((data)=> {
        this.loading = false;
        this.showConfirmation = false;
        this.customMessageService.sendSuccessErrorMessage(data.success_message,
          'success', true);
        this.selectedMenuIds = [];
        this.selectedNodes = [];
      }, error => {
        this.showConfirmation = false;
        this.confirmationMenuForm.reset();
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(error, 'error',
          true);
        this.selectedMenuIds = [];
        this.selectedNodes = [];
      })
  }

  addMenuPermission() {
    if (this.selectedNodes) {
      this.selectSomethingMessage = false;
      this.showConfirmation = true;
    } else {
      this.selectSomethingMessage = true;
      return;
    }
  }

  searchMenuPermission(event: any) {
    this.selectedGroupId = event;
    this.menuService.findAllMenusPermissionByUserGroupId(event)
      .subscribe((data) => {
        this.selectedNodes = (data.data);
        // if(data.data.children.lenght) {
        //   data.data.children.forEach((value) => {
        //     this.selectedNodes.push(value)
        //   })
        // }
      }, error => {
        this.customMessageService.sendSuccessErrorMessage(error, 'error',
          false);
      });
  }

  checkNode(nodes:TreeNode[], str:string[]) {
    nodes.forEach(node => {
      //check parent
      if(str.includes(node.data.name)) {
        this.selectedNodes.push(node);
      }

      if(node.children != undefined){
        node.children.forEach(child => {
          //check child if the parent is not selected
          if(str.includes(child.data.name) && !str.includes(child.data.name)) {
            node.partialSelected = true;
            child.parent = node;
          }

          //check the child if the parent is selected
          //push the parent in str to new iteration and mark all the childs
          if(str.includes(node.label)){
            child.parent = node;
            str.push(child.label);
          }
        });
      }else{
        return;
      }

      this.checkNode(node.children, str);

      node.children.forEach(child => {
        if(child.partialSelected) {
          node.partialSelected = true;
        }
      });
    });
  }

}
