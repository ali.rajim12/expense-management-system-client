import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {MainLayoutComponent} from "../../layout";
import {MenuListComponent} from "./menu-list/menu-list.component";
import {MenuPermissionComponent} from "./menu-permission/menu-permission.component";
import {AddMenuComponent} from "./add-menu/add-menu.component";
import {EditPermissionComponent} from "./edit-permission/edit-permission.component";

const routes: Routes = [
  {
    path: 'menus',
    component: MainLayoutComponent,
    children: [
      {
        path: 'all',
        component: MenuListComponent
      },
      {
        path: 'permission',
        component: MenuPermissionComponent
      },
      {
        path: 'edit-permission',
        component: EditPermissionComponent
      },
      {
        path: 'add',
        component: AddMenuComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModules {

}
