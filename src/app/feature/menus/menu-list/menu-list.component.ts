import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {PreApprovalResponse} from "../../pre-approval";
import {BillPaymentService} from "../../bill-payment";
import {CommonVariableService} from "../../../shared";
import {ColumnName} from "../../../shared/constant/column-name";
import {MenuService} from "../../../core/_services";
import {Menu} from "../../../core/components/menu";
import {TreeNode} from "primeng/api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomMessageService} from "../../../shared/service/custom-message.service";

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.sass']
})
export class MenuListComponent implements OnInit {

  data$: TreeNode[];
  selectedItems: PreApprovalResponse;
  columnName: any[];
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  showConfirmation: boolean = false;
  updateMenuForm: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  selectedMenuId: number;

  constructor(private menuService: MenuService,
              private commonVariableService: CommonVariableService,
              private fb: FormBuilder,
              private customMessageService: CustomMessageService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.updateMenuForm = fb.group({
      name: [null, Validators.required],
      icon: [null, Validators.required]
    })
  }

  get f() {
    return this.updateMenuForm.controls;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.MENU_TABLE_HEADINGS;
    this.reloadData();
  }

  reloadData() {
    this.menuService.findAllMenusForTreeNode()
      .subscribe((data) => {
        this.data$ = data.data;
      }, error => {
        console.log(error)
      });
  }

  handleChange($event: any, menuId: number) {
    this.menuService.updateBudgetHeadStatus($event.checked ? 'ACTIVE' : 'INACTIVE',
      menuId).subscribe(() => {
      this.reloadData();
    }, error1 => {
      console.log(error1);
    });
  }

  updateMenu(detail : any) {
    this.showConfirmation = true;
    this.selectedMenuId = detail.menuId;
    this.updateMenuForm.controls.name.patchValue(detail.name);
    this.updateMenuForm.controls.icon.patchValue(detail.icon);
  }

  resetForm() {
    this.showConfirmation = false;
    this.loading = false;
    this.updateMenuForm.reset();
  }

  submit() {
    this.submitted = true;
    if(this.updateMenuForm.invalid) {
      return;
    }
    this.loading = true;
    this.menuService.updateMenu(this.updateMenuForm.value, this.selectedMenuId)
      .subscribe((data)=> {
        this.data$ = data.data;
        this.loading = false;
        this.showConfirmation = false;
        this.customMessageService.sendSuccessErrorMessage("Menu Updated Successfully.",
          'success', true);
        this.reloadData();
      }, error => {
        this.showConfirmation = false;
        this.updateMenuForm.reset();
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(error, 'error', true);
        this.reloadData();
      })
  }

}
