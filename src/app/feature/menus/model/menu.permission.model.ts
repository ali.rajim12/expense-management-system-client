export class MenuPermissionModel {
  menuId:  number;
  name: string;
  url: string;
  icon: string;
  status: string;
  parentMenu: string;
}
