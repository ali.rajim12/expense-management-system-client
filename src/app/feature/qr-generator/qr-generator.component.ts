import {Component, OnInit} from '@angular/core';
import {NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels} from '@techiediaries/ngx-qrcode';
import {QrService} from "./service/qr.service";
import {ColumnName} from "../../shared/constant/column-name";
import {Observable} from "rxjs";
import {ExpenditureModel} from "../pre-approval/models/expenditure.model";
import {QrDetail} from "./model/qr-detail.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomMessages} from "../../shared/constant/custom-messages";
import {CustomMessageService} from "../../shared/service/custom-message.service";


@Component({
  selector: 'app-qr-generator',
  templateUrl: './qr-generator.component.html',
  styleUrls: ['./qr-generator.component.css']
})
export class QrGeneratorComponent implements OnInit {

  data$: Observable<QrDetail[]>;
  elementType = NgxQrcodeElementTypes.IMG;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.LOW;
  ngxQrcodeVersionType = '1';
  value: string = '';
  searchName: string = '';
  columnName: any[];
  totalRecords: number;
  page: number = 0;
  itemsPerPage: number = 25;
  display: boolean = false;
  addEditType: String;
  addEditQRDetailFrom: FormGroup;
  submitted: boolean = false;
  loading: boolean = false;
  showQRCode: boolean = false;
  selectedQRDetailId: number = 0;
  selectedName: string;

  constructor(private qrService: QrService,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder) {
    this.addEditQRDetailFrom = fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      title: [null, Validators.required],
      street: null,
      zipCode: null,
      city: null,
      country: null,
      company: null,
      email: null,
      emailBusiness: null,
      phonePersonal: null,
      phoneMobile: null,
      phoneBusiness: null,
      website: null,
    });
  }

  ngOnInit(): void {
    this.columnName = ColumnName.QRCODE_TABLE_HEAD;
    this.reloadData();
  }

  get f() {
    return this.addEditQRDetailFrom.controls;
  }

  reloadData() {
    this.qrService.fetchAllQRDetails(this.searchName, this.page, this.itemsPerPage)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }


  searchAccountHead(name: string) {
    this.searchName = name;
    this.reloadData();
  }

  addEditQRDetail(detail: QrDetail, isAdd: boolean) {
    this.addEditType = isAdd ? 'Add' : 'Edit';
    this.display = true;
    this.selectedName = detail.firstName + ' ' + detail.lastName;
    if (this.addEditType === 'Edit') {
      this.selectedQRDetailId = detail.qrDetailId;
      this.addEditQRDetailFrom.controls.firstName.patchValue(detail.firstName);
      this.addEditQRDetailFrom.controls.lastName.patchValue(detail.lastName);
      this.addEditQRDetailFrom.controls.title.patchValue(detail.title);
      this.addEditQRDetailFrom.controls.street.patchValue(detail.street);
      this.addEditQRDetailFrom.controls.zipCode.patchValue(detail.zipCode);
      this.addEditQRDetailFrom.controls.city.patchValue(detail.city);
      this.addEditQRDetailFrom.controls.country.patchValue(detail.country);
      this.addEditQRDetailFrom.controls.company.patchValue(detail.company);
      this.addEditQRDetailFrom.controls.email.patchValue(detail.email);
      this.addEditQRDetailFrom.controls.emailBusiness.patchValue(detail.emailBusiness);
      this.addEditQRDetailFrom.controls.phonePersonal.patchValue(detail.phonePersonal);
      this.addEditQRDetailFrom.controls.phoneMobile.patchValue(detail.phoneMobile);
      this.addEditQRDetailFrom.controls.phoneBusiness.patchValue(detail.phoneBusiness);
      this.addEditQRDetailFrom.controls.website.patchValue(detail.website);
    }
  }

  confirm() {
    this.submitted = true;
    if (this.addEditQRDetailFrom.invalid) {
      return;
    }
    this.loading = true;
    if (this.selectedQRDetailId === 0) {
      this.qrService.submitQRDetail(this.addEditQRDetailFrom.value)
        .subscribe((response) => {
          this.display = false;
          this.reloadData();
          this.customMessageService.sendSuccessErrorMessage(response.success_message,
            'success', true);
          this.loading = false;
        }, error1 => console.log(error1));
    } else {
      this.qrService.editQRDetail(this.addEditQRDetailFrom.value, this.selectedQRDetailId)
        .subscribe((response) => {
          this.display = false;
          this.reloadData();
          this.customMessageService.sendSuccessErrorMessage(response.success_message,
            'success', true);
          this.loading = false;
        }, error1 => console.log(error1));
    }

    this.loading = false;
  }

  cancel() {
    this.display = false;
    this.addEditQRDetailFrom.reset();
    this.submitted = false;
    this.value = '';
    this.showQRCode = false;
  }

  preview() {
    console.log(this.addEditQRDetailFrom.value.firstName)
    if(this.addEditQRDetailFrom.value.firstName === null) {
      return;
    }
    this.showQRCode = true;
    this.value = this.addEditQRDetailFrom.value.firstName + ' '
      + this.addEditQRDetailFrom.value.lastName + ', '
      + this.addEditQRDetailFrom.value.title + ', '
      + this.addEditQRDetailFrom.value.company + ', '
      + this.addEditQRDetailFrom.value.city + ', '
      + this.addEditQRDetailFrom.value.country + ', '
      + this.addEditQRDetailFrom.value.phoneMobile + ', '
      + this.addEditQRDetailFrom.value.phoneBusiness + ', '
      + this.addEditQRDetailFrom.value.emailBusiness;
  }

  download(name: string) {
    var x = document.getElementsByClassName("bshadow")[0].innerHTML;
    this.downloadImage(x.split('=')[1].replace('"', ''), name);
  }

  async downloadImage(imageSrc, name) {
    const image = await fetch(imageSrc)
    const imageBlog = await image.blob()
    const imageURL = URL.createObjectURL(imageBlog)

    const link = document.createElement('a')
    link.href = imageURL
    link.download = name + '.png'
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }


}
