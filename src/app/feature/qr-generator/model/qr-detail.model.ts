export class QrDetail {
  qrDetailId: number;
  createdDate: string;
  firstName: string;
  lastName: string;
  title: string;
  street: string;
  zipCode: string;
  city: string;
  country: string;
  company: string;
  email: string;
  emailBusiness: string;
  phonePersonal: string;
  phoneMobile: string;
  phoneBusiness: string;
  website: string;
  status: string;
}
