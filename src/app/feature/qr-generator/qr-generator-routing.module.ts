import {NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {QrGeneratorComponent} from "./qr-generator.component";

const routes: Routes = [
  {
    path: 'generateQR',
    component: MainLayoutComponent,
    // canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: QrGeneratorComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QrGeneratorRoutingModule {

}
