import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QrService {

  constructor(private http: HttpClient) { }


  fetchAllQRDetails(searchName: string, page: number, itemPerPage: number): Observable<any>  {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/qr`, {
      params:
        {
          firstName: searchName,
          page: page + '',
          size: itemPerPage + ''
        },
    });

  }

  submitQRDetail(value: any) : Observable<any>{
    return this.http.post(`${environment.apiUrl}/api/auth/qr`, value);
  }

  editQRDetail(value: any, selectedQRDetailId: number): Observable<any>{
    return this.http.post(`${environment.apiUrl}/api/auth/qr/${selectedQRDetailId}`, value);
  }
}
