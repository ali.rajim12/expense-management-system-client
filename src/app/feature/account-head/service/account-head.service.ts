import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vendor} from '../../vendors/model/vendor.model';
import {environment} from '../../../../environments/environment';
import {retry} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {AccountHead} from '..';

@Injectable({
  providedIn: 'root'
})

export class AccountHeadService {

  constructor(private http: HttpClient) { }

  fetchAllAccountHead(): Observable<AccountHead[]> {
    return this.http.get<AccountHead[]>(`${environment.apiUrl}/api/auth/account_head`);
  }
  getAllAccountHeadsWithParmas(name: string): Observable<AccountHead[]> {
    return this.http.get<AccountHead[]>(`${environment.apiUrl}/api/auth/account_head/all`,
      {
        params: {
          name: name
        }
      });
  }

  getAllAccountHeadList(): Observable<AccountHead[]> {
    return this.http.get<AccountHead[]>(`${environment.apiUrl}/api/auth/account_head`);
  }

  getAllAccountHeads(): Observable<AccountHead[]> {
    return this.http.get<AccountHead[]>(`${environment.apiUrl}/api/auth/account_head/all`);
  }

  updateAccountHeadStatus(status: string, accountHeadId: number): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/account_head/${accountHeadId}/${status}`, null);
  }

  addUpdateAccountHeadDetail(value: any, selectedAccountHeadId: number, type: string): Observable<any> {
    if(type === 'Add') {
      return this.http.post(`${environment.apiUrl}/api/auth/account_head`, value);
    } else {
      return this.http.put(`${environment.apiUrl}/api/auth/account_head/${selectedAccountHeadId}`, value);
    }
  }

  fetchAllAccountHeadByBranchBudgetId(budgetHeadId: number): Observable<AccountHead[]> {
    return this.http.get<AccountHead[]>(`${environment.apiUrl}/api/auth/account_head/${budgetHeadId}/all`);
  }
}
