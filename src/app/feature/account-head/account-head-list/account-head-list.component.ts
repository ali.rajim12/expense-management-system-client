import {Component, OnInit} from '@angular/core';
import {ColumnName} from '../../../shared/constant/column-name';
import {AccountHeadService} from '../service/account-head.service';
import {AccountHead} from '../model/account-head.model';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BudgetHead, BudgetHeadService} from "../../budget-head";
import {ExpenditureType} from "../../pre-approval/models/expenditure-type.model";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {CustomMessages} from "../../../shared/constant/custom-messages";
import {CustomMessageService} from "../../../shared/service/custom-message.service";

@Component({
  selector: 'app-account-head-list',
  templateUrl: './account-head-list.component.html',
  styleUrls: ['./account-head-list.component.sass']
})
export class AccountHeadListComponent implements OnInit {

  data$: AccountHead[];
  columnName: any[];
  totalRecords: number;
  page: number;
  addEditAccountHeadForm: FormGroup;
  display: boolean = false;
  addEditType: string;
  budgetHeads: BudgetHead[];
  submitted: boolean = false;
  expenditureTypes: ExpenditureType[];
  loading: boolean = false;
  accountNumberValidated: boolean = false;
  detail: any;
  selectedAccountHeadId: number;
  searchedName: string = '';

  constructor(private accountHeadService: AccountHeadService,
              private budgetHeadService: BudgetHeadService,
              private preApprovalRequestService: PreApprovalService,
              private preApprovalService: PreApprovalService,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder) {
    this.addEditAccountHeadForm = fb.group({
      staffAccount: [true],
      budgetHeadId: [0, Validators.required],
      description: [null, Validators.required],
      code: [null, Validators.required],
      accountNumber: [null, Validators.required],
      type: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.columnName = ColumnName.ACCOUNT_HEAD_TABLE;
    this.reloadData();
    this.fetchAllExpenditureTypes();
  }

  get f() {
    return this.addEditAccountHeadForm.controls;
  }

  fetchAllExpenditureTypes() {
    this.preApprovalRequestService.getAllExpenditureTypes()
      .subscribe((response) => {
        this.expenditureTypes = response;
      }, error1 => console.log(error1));
  }

  reloadData() {
    this.accountHeadService.getAllAccountHeadsWithParmas(this.searchedName)
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = this.data$.length;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
  }

  addEditBudgetHead(type: string, detail: any) {
    this.display = true;
    this.addEditType = type;
    if (type === 'Edit') {
      this.selectedAccountHeadId = detail.accountHeadId;
      this.accountNumberValidated = true;
      this.addEditAccountHeadForm.controls.staffAccount.patchValue(detail.staffAccount);
      this.addEditAccountHeadForm.controls.description.patchValue(detail.description);
      this.addEditAccountHeadForm.controls.code.patchValue(detail.code);
      this.addEditAccountHeadForm.controls.accountNumber.patchValue(detail.accountNumber);
      this.addEditAccountHeadForm.controls.type.patchValue(this.getExpenditureType(detail.type));
      this.addEditAccountHeadForm.controls.budgetHeadId.patchValue(this.getBudgetHead(detail.budgetHeadId));
    }
  }

  private getBudgetHead(budgetHeadId: number) {
    let requiredValue: number = null;
    this.budgetHeads.forEach(value => {
      if (value.budgetHeadId === budgetHeadId) {
        requiredValue = value.budgetHeadId;
      }
    });
    return requiredValue;
  }

  private getExpenditureType(expenditureType: string) {
    let requiredValue: string = null;
    this.expenditureTypes.forEach((value) => {
      if (value.type === expenditureType) {
        requiredValue = value.type;
      }
    });
    return requiredValue;
  }

  handleChange(event: any, accountHeadId: number) {
    this.accountHeadService.updateAccountHeadStatus(event.checked ? 'ACTIVE' : 'INACTIVE',
      accountHeadId).subscribe(() => {
      this.reloadData();
    }, error1 => {
      console.log(error1);
    });
  }

  cancel() {
    this.display = false;
    this.accountNumberValidated = false;
    this.detail = null;
    this.submitted = false;
  }

  submit() {
    this.submitted = true;
    if (this.addEditAccountHeadForm.invalid) {
      return;
    }
    if (this.addEditAccountHeadForm.value.staffAccount) {
      this.validateAccountNumber()
    }
  }

  confirm() {
    if(this.accountNumberValidated === true) {
      this.submitted = true;
      if (this.addEditAccountHeadForm.invalid) {
        return;
      }
    }
    this.loading = true;
    this.accountHeadService.addUpdateAccountHeadDetail(this.addEditAccountHeadForm.value,
      this.selectedAccountHeadId, this.addEditType).subscribe((data) => {
        this.loading = false;
        this.display = false;
        this.reloadData();
        this.customMessageService.sendSuccessErrorMessage(this.addEditType === 'Add' ?
          CustomMessages.ACCOUNT_HEAD_ADDED_SUCCESSFULLY : CustomMessages.ACCOUNT_HEAD_UPDATED_SUCCESSFULLY,
          'success', true);
      }, error => {
        this.loading = false;
        this.display = false;
        this.customMessageService.sendSuccessErrorMessage(error, 'error',
          false);
      }
    )
  }

  validateAccountNumber() {
    this.preApprovalService.getAccountDetail(
      this.addEditAccountHeadForm.value.accountNumber
    ).subscribe((data) => {
      this.accountNumberValidated = true;
      this.detail = data;
    });
  }

  fetchBudgetHeads(event: any) {
    if(event === true) {
      this.accountNumberValidated = false;
    } else {
      this.accountNumberValidated = true;
      this.budgetHeadService.getAllBudgetHeads().subscribe((data) => {
        this.budgetHeads = data;
      }, error1 => {
        console.log(error1);
      });
    }

  }

  searchAccountHead(name: string){
    this.searchedName = name;
    this.reloadData();
  }
}
