export class AccountHead {
  accountHeadId: number;
  code: string;
  description: string;
  type: string;
  accountNumber: string;
  budgetHeadId: number;
  budgetHead: string;
  status: string;
  staffAccount: boolean;
}
