import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {NgModule} from '@angular/core';
import {AccountHeadListComponent} from './account-head-list/account-head-list.component';

const routes: Routes = [
  {
    path : 'account-head',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: AccountHeadListComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountHeadRouting {
}
