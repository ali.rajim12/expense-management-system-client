import {Component, Input, OnInit} from '@angular/core';
import {BudgetDetailResource} from '../model/budget-detail-resource';
import {BranchBudgetService} from '../service/branch-budget.service';
import {BudgetExpenseDetail} from '../model/budget-expense-detail';

@Component({
  selector: 'app-view-budget-detail',
  templateUrl: './view-budget-detail.component.html',
  styleUrls: ['./view-budget-detail.component.sass']
})
export class ViewBudgetDetailComponent implements OnInit {
  budgetDetail: BudgetDetailResource[];
  @Input() public expenditureDetailId;

  constructor(private branchBudgetService: BranchBudgetService) { }

  ngOnInit(): void {
    this.getBranchBudgetDetail(this.expenditureDetailId);
  }

  private getBranchBudgetDetail(expenditureDetailId: number) {
    this.branchBudgetService.fetchBranchBudgetDetailByExpDetailId(expenditureDetailId)
      .subscribe(data => {
        this.budgetDetail = data;
      })
  }

  getStatus(ytdBudgetBranch: BudgetExpenseDetail) {
    if(ytdBudgetBranch.totalExpense > ytdBudgetBranch.totalBudget) {
      return 'NO';
    } else {
      return 'YES';
    }
  }

  getGrowth(detail: BudgetDetailResource) {
    if(detail.thisMonthBudget.totalExpense === 0) {
      return 0;
    }
    return ((detail.thisMonthBudget.totalExpense - detail.previousMonthBudget.totalExpense)/(detail.thisMonthBudget.totalExpense))*100;
  }
}
