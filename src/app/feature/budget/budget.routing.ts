import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {NgModule} from '@angular/core';
import {ViewBudgetDetailComponent} from './view-budget-detail/view-budget-detail.component';
import {ListBranchBudgetComponent} from './list-branch-budget/list-branch-budget.component';
import {ListBudgetLimitComponent} from './budget-limit/list-budget-limit/list-budget-limit.component';
import {AddEditBudgetLimitComponent} from './budget-limit/add-edit-budget-limit/add-edit-budget-limit.component';
import {TransferBudgetComponent} from './transfer-budget/transfer-budget.component';
import {TransferDetailComponent} from './transfer-detail/transfer-detail.component';

const routes: Routes = [
  {
    path: 'budget',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: ViewBudgetDetailComponent
      }
    ]
  },
  {
    path: 'branch-budget',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: ListBranchBudgetComponent
      },
      {
        path: 'transfer',
        component: TransferBudgetComponent
      },
      {
        path: 'transfer/:entryId',
        component: TransferDetailComponent
      }

    ]
  },
  {
    path: 'budget-limit',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: ListBudgetLimitComponent
      },
      {
        path: 'create/:budgetLimitId',
        component: AddEditBudgetLimitComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetRouting {
}
