import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Users} from '../../../users/model/users.model';
import {UserService} from '../../../users/service/user.service';
import {CommonVariableService} from '../../../../shared';
import {ColumnName} from '../../../../shared/constant/column-name';
import {UserGroupService} from '../../../users/service/user-group.service';
import {UserGroup} from '../../../users/model/user-group.model';
import {AccountHead, AccountHeadService} from '../../../account-head';
import {BudgetHead, BudgetHeadService} from '../../../budget-head';
import {BudgetLimitService} from '../../service/budget-limit.service';
import {BudgetLimit} from '../../model/budget-limit.model';

@Component({
  selector: 'app-list-budget-limit',
  templateUrl: './list-budget-limit.component.html',
  styleUrls: ['./list-budget-limit.component.sass']
})
export class ListBudgetLimitComponent implements OnInit {

  data$: Observable<BudgetLimit[]>;
  columnName: any[];
  page: number = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<number>;
  totalRecords: number;
  selectedItems: Users;
  userName: string = '';
  userGroupId: number = 0;
  budgetHeadId: number = 0;
  accountHeadId: number = 0;
  userGroups: UserGroup[];
  accountHeads: AccountHead[];
  budgetHeads: BudgetHead[];

  constructor(private userService: UserService,
              private commonVariableService: CommonVariableService,
              private userGroupService: UserGroupService,
              private accountHeadService: AccountHeadService,
              private budgetHeadService: BudgetHeadService,
              private budgetLimitService: BudgetLimitService
  ) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.BUDGET_LIMIT_TABLE;
    this.reloadData(this.page, this.itemsPerPage);
    this.getAllUserGroups();
    this.getAccountHead();
    this.getBudgetHead();
  }

  reloadData(page: number, size: number) {
    this.budgetLimitService.fetchAllBranchBudgetLimit(this.budgetHeadId,
      this.accountHeadId,
      this.userGroupId,
      page,
      size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
        // this.totalRecords = this.data.length;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }


  getAllUserGroups() {
    this.userGroupService.getAllUserGroups().subscribe((data) => {
      this.userGroups = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAccountHead() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }


  getBudgetHead() {
    this.budgetHeadService.getAllBudgetHeads().subscribe((data) => {
      this.budgetHeads = data;
    }, error1 => {
      console.log(error1);
    });
  }

  Search() {
    this.reloadData(0, this.itemsPerPage);
  }


}
