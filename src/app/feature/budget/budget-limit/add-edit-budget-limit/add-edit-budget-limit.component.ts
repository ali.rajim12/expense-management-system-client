import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserGroupTypeService} from '../../../users/service/user-group-type.service';
import {UserGroupType} from '../../../users/model/user-group-type.model';
import {UserGroupService} from '../../../users/service/user-group.service';
import {UserGroup} from '../../../users/model/user-group.model';
import {AccountHead, AccountHeadService} from '../../../account-head';
import {of} from 'rxjs';
import {map} from 'rxjs/operators';
import {BudgetLimitService} from '../../service/budget-limit.service';

@Component({
  selector: 'app-add-edit-budget-limit',
  templateUrl: './add-edit-budget-limit.component.html',
  styleUrls: ['./add-edit-budget-limit.component.sass']
})
export class AddEditBudgetLimitComponent implements OnInit {
  type: string;
  budgetLimitId: string;
  groupTypes: UserGroupType[];
  userGroups: UserGroup[];
  accountHeads: AccountHead[];
  addEditBudgetLimitForm: FormGroup;
  showConfirmation: boolean = false;
  userGroupName: string;
  groupTypeName: string;
  accountHead: string;

  constructor(private fb: FormBuilder,
              private router: Router,
              private activeRoute: ActivatedRoute,
              private groupTypeService: UserGroupTypeService,
              private userGroupService: UserGroupService,
              private accountHeadService: AccountHeadService,
              private budgetLimitService: BudgetLimitService
  ) {
    this.addEditBudgetLimitForm = fb.group({
      accountHeadId: 0,
      userGroupId: 0,
      monthlyCelling: null,
      thisMonthExpense: null,
      lastMonthExpense: null,
      amountCelling: null,
      fixedCellingAmount: null,
      groupTypeId: 0
    });
  }

  ngOnInit(): void {
    this.budgetLimitId = this.activeRoute.snapshot.params.budgetLimitId;
    this.type = this.budgetLimitId === 'new' ? 'Add' : 'Edit';
    this.getAllGroupTypes();
    this.getAllUserGroups();
    this.getAccountHead();
    if (this.type !== 'new') {
      this.showEditForm();
    }
  }

  showEditForm() {
    if( this.budgetLimitId === 'new') {
      return;
    }
    this.budgetLimitService.findBudgetLimitDetailById(this.budgetLimitId)
      .subscribe((data) => {
        if(this.accountHeads !== undefined) {
          of(this.accountHeads)
            .pipe(map(results => results
              .filter(r => r.accountHeadId === data.accountHeadId)))
            .subscribe(value => {
              this.addEditBudgetLimitForm.controls.accountHeadId.patchValue(value[0].accountHeadId);
            });
        }
        of(this.userGroups)
          .pipe(map(results => results
            .filter(r => r.userGroupId === data.groupId)))
          .subscribe(value => {
            this.addEditBudgetLimitForm.controls.userGroupId.patchValue(value[0].userGroupId);
          });
        if(data.groupTypeName !== null) {
          of(this.groupTypes)
            .pipe(map(results => results
              .filter(r => r.userGroupTypeId === data.groupTypeId)))
            .subscribe(value => {
              this.addEditBudgetLimitForm.controls.groupTypeId.patchValue(value[0].userGroupTypeId);
            });
        }
        this.addEditBudgetLimitForm.controls.monthlyCelling.patchValue(data.monthlyCelling);
        this.addEditBudgetLimitForm.controls.lastMonthExpense.patchValue(data.previousMonthExpense);
        this.addEditBudgetLimitForm.controls.amountCelling.patchValue(data.cellingAmount);
        this.addEditBudgetLimitForm.controls.fixedCellingAmount.patchValue(data.fixedCellingAmount);
        this.addEditBudgetLimitForm.controls.thisMonthExpense.patchValue(data.thisMonthExpense);

    }, error1 => {
      console.log(error1);
    });
  }

  getAllUserGroups() {
    this.userGroupService.getAllUserGroups().subscribe((data) => {
      this.userGroups = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAllGroupTypes() {
    this.groupTypeService.getAllUserGroupTypes()
      .subscribe((data) => {
        this.groupTypes = data;
      }, error1 => {
        console.log(error1);
      });
  }

  getAccountHead() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  confirmDetail() {
    this.showConfirmation = true;
    this.getUserGroupName();
    if (this.addEditBudgetLimitForm.value.groupTypeId !== undefined ||
      this.addEditBudgetLimitForm.value.groupTypeId !== null) {
      this.getUserGroupTypeName();
    }
    this.getAccountHeadName();
  }

  onSubmit() {
    if(this.type === 'Edit') {
      this.budgetLimitService.updateBudgetLimit(this.budgetLimitId,
        this.addEditBudgetLimitForm.value)
        .subscribe((data) => {
          this.addEditBudgetLimitForm.reset();
          this.router.navigate(['/budget-limit']);
        }, error1 => {
          console.log(error1);
        });
    } else {
      this.budgetLimitService.submitBudgetLimit(this.addEditBudgetLimitForm.value)
        .subscribe((data) => {
          this.addEditBudgetLimitForm.reset();
          this.router.navigate(['/budget-limit']);
        }, error1 => {
          console.log(error1);
        });

    }

  }

  getAccountHeadName() {
    return of(this.accountHeads)
      .pipe(map(results => results
        .filter(r => r.accountHeadId === this.addEditBudgetLimitForm.value.accountHeadId)))
      .subscribe(value => {
        this.accountHead = (value[0]).description;
      });
  }

  getUserGroupTypeName() {
    return of(this.groupTypes)
      .pipe(map(results => results
        .filter(r => r.userGroupTypeId === this.addEditBudgetLimitForm.value.groupTypeId)))
      .subscribe(value => {
        this.groupTypeName = (value[0]).name;
      });
  }


  private getUserGroupName() {
    return of(this.userGroups)
      .pipe(map(results => results
        .filter(r => r.userGroupId === this.addEditBudgetLimitForm.value.userGroupId)))
      .subscribe(value => {
        this.userGroupName = (value[0]).description;
      });
  }
}
