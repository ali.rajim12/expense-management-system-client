import {BudgetAccountHead} from './budget-account-head';
import {BudgetExpenseDetail} from './budget-expense-detail';

export class BudgetDetailResource {
  budgetAccountHead: BudgetAccountHead;
  thisMonthBudget: BudgetExpenseDetail;
  previousMonthBudget: BudgetExpenseDetail;
  ytdBudgetBranch: BudgetExpenseDetail;
  countryThisMonthBudget: BudgetExpenseDetail;
  countryYTDBudget: BudgetExpenseDetail;
  countryTotalBudget: BudgetExpenseDetail;
}
