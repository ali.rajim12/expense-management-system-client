import {UserMini} from "../../users/model/user-mini.model";

export class BudgetTransferEntryModel {
  requestedBy: string;
  assignedTo: string;
  count: number;
  entryId: number;
  status: string;
  transferStatus: string;
  remarks: string;
  requestedDate: any;
  requestedByUserId: number;
  assignedToUserId: number;
}
