import {BranchMiniResource} from '../../branch';

export class BudgetAccountHead {
  budgetHeadId: number;
  budgetHead: string;
  accountHead: string;
  accountHeadId: number;
  mainCode: string;
  branchTotalBudget: number;
  month: string;
  expenditureDetailId: number;
  debitBranch: BranchMiniResource;
}
