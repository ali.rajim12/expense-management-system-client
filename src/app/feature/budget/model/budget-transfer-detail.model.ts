export class BudgetTransferDetailModel {
  budgetTransferDetailId: number;
  fromFiscalYear: string;
  fromMonth: string;
  fromBranch: string;
  fromAccountHead: string;

  toFiscalYear: string;
  toMonth: string;
  toBranch: string;
  toAccountHead: string;

  amount: number;
  status: string;
  transferStatus: string;

  requestedBy: number;
  assignedTo: number;
}
