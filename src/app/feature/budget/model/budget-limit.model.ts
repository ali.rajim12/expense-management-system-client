export class BudgetLimit {
  budgetLimitId: number;
  budgetHead: string;
  accountHead: string;
  accountHeadId: number;
  groupName: string;
  groupId: number;
  monthlyCelling: number;
  previousMonthExpense: number;
  cellingAmount: number;
  fixedCellingAmount: number;
  thisMonthExpense: number;
  groupTypeName: string;
  groupTypeId: number;
}
