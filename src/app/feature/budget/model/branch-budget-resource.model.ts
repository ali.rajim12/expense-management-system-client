import {BranchMiniResource} from '../../branch';

export class BranchBudgetResource {
  branchMiniResource: BranchMiniResource;
  fiscalYear: string;
  fiscalYearId: number;
  month: string;
  budgetHead: string;
  accountHead: string;
  accountHeadId: number;
  budgetAmount: number;
  expenseTillDate: number;
  preApprovedAmount: number;
  branchBudgetId: number;
}
