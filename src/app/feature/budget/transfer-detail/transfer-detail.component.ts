import {Component, OnInit} from '@angular/core';
import {BudgetTransferEntryModel} from "../model/budget-transfer-entry.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UserMini} from "../../users/model/user-mini.model";
import {CommonVariableService} from "../../../shared";
import {PeriodicPaymentService} from "../../periodic-payment/service/periodic.payment.service";
import {ActivatedRoute} from "@angular/router";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {Branch, BranchService} from "../../branch";
import {BranchBudgetService} from "..";
import {UserService} from "../../users/service/user.service";
import {ColumnName} from "../../../shared/constant/column-name";
import {BehaviorSubject} from "rxjs";
import {User} from "../../../core/_models/user";
import * as FileSaver from "file-saver";
import {BudgetTransferDetailModel} from "../model/budget-transfer-detail.model";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {BudgetHead} from "../../budget-head";
import {AccountHead, AccountHeadService} from "../../account-head";
import {Month} from "../../pre-approval/models/month.model";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";

@Component({
  selector: 'app-transfer-detail',
  templateUrl: './transfer-detail.component.html',
  styleUrls: ['./transfer-detail.component.sass']
})
export class TransferDetailComponent implements OnInit {

  columnName: any[];
  entryId: number;
  data$: BudgetTransferDetailModel[];
  currentUserId: number;
  requestedByUserId: number;
  showEditAmountForm: boolean = false;
  editAmountFormDetail: any;
  updateBudgetTransferDetailForm: FormGroup;
  submitted: boolean = false;
  loading: boolean = false;
  displayBudgetTransferForm: boolean = false;
  budgetTransferForm: FormGroup;

  fiscalYears: FiscalYear[];
  branches: Branch[];
  accountHeads: AccountHead[];
  months: Month[];

  constructor(private activatedRoute: ActivatedRoute,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder,
              private branchService: BranchService,
              private branchBudgetService: BranchBudgetService,
              private preApprovalRequestService: PreApprovalService,
              private accountHeadService: AccountHeadService) {
    this.updateBudgetTransferDetailForm = this.fb.group({
      amount: [null, Validators.required]
    })
    this.entryId = this.activatedRoute.snapshot.params.entryId;
    this.currentUserId = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')))
      .getValue().userId;

    this.budgetTransferForm = fb.group({
      fromAccountHeadId: [0, Validators.required],
      fromBranchId: [0, Validators.required],
      fromFiscalYearId: [0, Validators.required],
      fromMonth: [0, Validators.required],
      toAccountHeadId: [0, Validators.required],
      toBranchId: [0, Validators.required],
      toFiscalYearId: [0, Validators.required],
      toMonth: [0, Validators.required],
      amount: [null, Validators.required],
    })
  }

  get f() {
    return this.updateBudgetTransferDetailForm.controls;
  }

  get ff() {
    return this.budgetTransferForm.controls;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.TRANSFER_BUDGET_DETAIL;
    this.reloadData();
    this.currentUserId = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')))
      .getValue().userId;
  }

  reloadData() {
    this.branchBudgetService.fetchAllTransferBudgetDetailByEntryId(this.entryId)
      .subscribe((data) => {
        this.data$ = data;
        if (this.data$.length > 0) {
          this.requestedByUserId = this.data$[0].requestedBy;
        }
      }, error => {
      })
  }


  cancelSubmit() {
    this.showEditAmountForm = false;
    this.updateBudgetTransferDetailForm.reset();
  }

  submitUploadForm() {
    this.submitted = true;
    if (this.updateBudgetTransferDetailForm.invalid) {
      return;
    }
    this.loading = true;
    this.branchBudgetService.updateTransferDetailAmount(this.editAmountFormDetail.budgetTransferDetailId,
      this.updateBudgetTransferDetailForm.value.amount
    ).subscribe((data) => {
      this.loading = false;
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        false);
      this.loading = false;
      this.updateBudgetTransferDetailForm.reset();
      this.showEditAmountForm = false;
      this.reloadData();
    }, error => {
      this.customMessageService.sendSuccessErrorMessage(error,
        'error', false);
      this.loading = false;
      this.updateBudgetTransferDetailForm.reset();
      this.showEditAmountForm = false;
    })
  }

  handleChange($event: any, detailId: number) {
    this.branchBudgetService.updateTransferBudgetDetailStatus(
      $event.checked ? 'ACTIVE' : 'INACTIVE',
      detailId).subscribe(() => {
      this.reloadData();
    }, error1 => {
      console.log(error1);
    });
  }

  editAmount(detail: any) {
    this.showEditAmountForm = true;
    this.editAmountFormDetail = detail;
    this.updateBudgetTransferDetailForm.controls.amount.patchValue(detail.amount);
  }

  addNewEntry() {
    this.displayBudgetTransferForm = true;
    this.submitted = false;
    this.fetchAllFiscalYears();
    this.getAllBranch();
    this.fetchAllMonths();
    this.fetchAllAccountHeads();
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  getAllBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.branches = data;
    }, error1 => {
      console.log(error1);
    });
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonths()
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }

  fetchAllAccountHeads() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  cancelTransfer() {
    // this.submitted = false;
    this.displayBudgetTransferForm = false;
    // this.budgetTransferForm.reset();
  }


  submitTransfer() {
    this.submitted = true;
    if (this.budgetTransferForm.invalid) {
      return;
    }
    this.loading = true;
    this.branchBudgetService.addNewBudgetTransferDetail(this.budgetTransferForm.value,
      this.entryId)
      .subscribe((data) => {
        this.loading = false;
        this.displayBudgetTransferForm = false;
        setTimeout(() => {
          this.customMessageService.sendSuccessErrorMessage(
            data.success_message,
            'success', true);
        }, 2000);
        this.reloadData();
        this.budgetTransferForm.reset();
      }, error1 => {
        this.loading = false;
        this.displayBudgetTransferForm = false;
        this.customMessageService.sendSuccessErrorMessage(error1, 'error', true);
      });
  }
}
