import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vendor} from '../../vendors/model/vendor.model';
import {environment} from '../../../../environments/environment';
import {retry} from 'rxjs/operators';
import {BudgetDetailResource} from '../model/budget-detail-resource';
import {BranchBudgetResource} from "../model/branch-budget-resource.model";
import {BudgetTransferEntryModel} from "../model/budget-transfer-entry.model";
import {BudgetTransferDetailModel} from "../model/budget-transfer-detail.model";

@Injectable({
  providedIn: 'root'
})
export class BranchBudgetService {

  constructor(private http: HttpClient) {
  }

  fetchAllVendors(): Observable<Vendor[]> {
    return this.http.get<Vendor[]>(`${environment.apiUrl}/api/auth/vendors`)
      .pipe(retry(3), null);
  }

  fetchSearchedVendor(vendorName: string): Observable<Vendor[]> {
    let params = new HttpParams();
    params = params.append('q', vendorName);
    return this.http.get<Vendor[]>(`${environment.apiUrl}/api/auth/vendors/search`, {params: params});
  }

  fetchBranchBudgetDetailByExpDetailId(expenditureDetailId: number): Observable<BudgetDetailResource[]> {
    return this.http.get<BudgetDetailResource[]>(`${environment.apiUrl}/api/auth/budget/${expenditureDetailId}`);
  }

  fetchAllBranchBudgets(branchId: number,
                        fiscalYearId: number,
                        budgetHeadId: number,
                        accountHeadId: number,
                        selectedQuarterId: number,
                        page: number,
                        size: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/branch_budget`, {
      params:
        {
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          budget_head_id: budgetHeadId + '',
          account_head_id: accountHeadId + '',
          quarter_id: selectedQuarterId + '',
          page: page + '',
          size: size + ''
        }
    });

  }

  submitBranchBudget(value: any, selectedItem: BranchBudgetResource): Observable<any> {
    if (selectedItem !== undefined) {
      if (selectedItem.branchBudgetId !== undefined && selectedItem.branchBudgetId !== null) {
        return this.http.put(`${environment.apiUrl}/api/auth/branch_budget/${selectedItem.branchBudgetId}`, value);
      }
    } else {
      return this.http.post(`${environment.apiUrl}/api/auth/branch_budget`, value);
    }
  }

  getBranchBudgetDetail(fiscalYearId: number, month: string, branchId: number, accountHeadId: number): Observable<BudgetDetailResource> {
    return this.http.get<BudgetDetailResource>(`${environment.apiUrl}/api/auth/budget`, {
      params:
        {
          fiscal_year_id: fiscalYearId + '',
          month: month + '',
          branch_id: branchId + '',
          account_head_id: accountHeadId + ''
        }
    })
  }

  getBranchBillBudgetDetail(expenditureDetailId: number, expenditureTxnDetailId: number): Observable<BudgetDetailResource> {
    return this.http.get<BudgetDetailResource>(`${environment.apiUrl}/api/auth/budget/${expenditureDetailId}/${expenditureTxnDetailId}`)
  }

  transferBranchBudget(value: any) {
    return this.http.post(`${environment.apiUrl}/api/auth/branch_budget/transfer_budget`, value);
  }

  getTransferBudgetSampleDoc() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })

    return this.http.get(`${environment.apiUrl}/api/auth/transfer_budget/download/sample`,
      { headers:headers, responseType: 'blob'})
  }

  fetchAllTransferBudgetEntry():  Observable<BudgetTransferEntryModel[]> {
    return this.http.get<BudgetTransferEntryModel[]>(`${environment.apiUrl}/api/auth/transfer_budget`);
  }

  transferBulkBudget(formData: FormData, assignedUserId: number, remarks: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/transfer_budget/${assignedUserId}/file/${remarks}`,
      formData);
  }

  updateTransferBudgetEntryStatus(status: string, entryId: number): Observable<any>  {
    return this.http.post(`${environment.apiUrl}/api/auth/transfer_budget/transfer_entry/${entryId}/${status}`,
      null);
  }

  fetchAllTransferBudgetDetailByEntryId(entryId: number): Observable<BudgetTransferDetailModel[]> {
    return this.http.get<BudgetTransferDetailModel[]>(`${environment.apiUrl}/api/auth/transfer_budget/transfer_entry/${entryId}`);
  }

  updateTransferBudgetDetailStatus(status: string, detailId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/transfer_budget/transfer_detail/${detailId}/${status}`,
      null);
  }

  updateTransferDetailAmount(budgetTransferDetailId: number, amount: string) : Observable<any>{
    return this.http.post(`${environment.apiUrl}/api/auth/transfer_budget/transfer_detail/${budgetTransferDetailId}/${amount}/update`,
      null);
  }

  addNewBudgetTransferDetail(value: any, entryId: number) : Observable<any>{
    return this.http.post(`${environment.apiUrl}/api/auth/transfer_budget/transfer_entry/${entryId}/transfer_detail/new`,
      value);
  }

  approveBudgetTransfer(entryId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/transfer_budget/approve_transfer/${entryId}`,
      null);
  }

  downloadBranchBudgetExcel(branchId: number,
                            fiscalYearId: number,
                            budgetHeadId: number,
                            accountHeadId: number,
                            selectedQuarter: number) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })

    return this.http.get(`${environment.apiUrl}/api/auth/branch_budget/excel`,
      { headers:headers, responseType: 'blob', params: {
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          budget_head_id: budgetHeadId + '',
          account_head_id: accountHeadId + '',
          quarter_id: selectedQuarter + ''
        }})
  }
}

