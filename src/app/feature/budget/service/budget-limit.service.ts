import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {BudgetLimit} from '../model/budget-limit.model';

@Injectable({
  providedIn: 'root'
})
export class BudgetLimitService {

  constructor(private http: HttpClient) {
  }

  fetchAllBranchBudgetLimit(budgetHeadId: number,
                            accountHeadId: number,
                            userGroupId: number,
                            page: number,
                            size: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/budget_limit`, {
      params:
        {
          budget_head_id: budgetHeadId + '',
          account_head_id: accountHeadId + '',
          user_group_id: userGroupId + '',
          page: page + '',
          size: size + ''
        }
    });

  }


  submitBudgetLimit(value: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/budget_limit`,value);
  }

  findBudgetLimitDetailById(budgetLimitId: string): Observable<BudgetLimit> {
    return this.http.get<BudgetLimit>(`${environment.apiUrl}/api/auth/budget_limit/${budgetLimitId}`);
  }

  updateBudgetLimit(budgetLimitId: string, value: any): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/budget_limit/${budgetLimitId}`, value);
  }
}
