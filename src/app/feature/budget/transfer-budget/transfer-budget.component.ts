import {Component, OnInit} from '@angular/core';
import {PeriodPaymentDetail} from "../../periodic-payment/model/period.payment.detail.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Branch, BranchService} from "../../branch";
import {CommonVariableService} from "../../../shared";
import {PeriodicPaymentService} from "../../periodic-payment/service/periodic.payment.service";
import {ActivatedRoute} from "@angular/router";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {ColumnName} from "../../../shared/constant/column-name";
import * as FileSaver from "file-saver";
import {BehaviorSubject, from} from "rxjs";
import {filter} from "rxjs/operators";
import {BranchBudgetService} from "..";
import {BudgetTransferEntryModel} from "../model/budget-transfer-entry.model";
import {UserService} from "../../users/service/user.service";
import {UserMini} from "../../users/model/user-mini.model";
import {User} from "../../../core/_models/user";
import {ConfirmationService} from 'primeng/api';

@Component({
  selector: 'app-transfer-budget',
  templateUrl: './transfer-budget.component.html',
  styleUrls: ['./transfer-budget.component.sass']
})
export class TransferBudgetComponent implements OnInit {

  columnName: any[];
  data$: BudgetTransferEntryModel[];
  showUploadFilePopUp: boolean = false;
  uploadBudgetTransferForm: FormGroup;
  allUsers: UserMini[];
  submitted: boolean = false;
  currentUserId: number;
  loading: boolean = false;

  constructor(private commonVariableService: CommonVariableService,
              private periodicPaymentService: PeriodicPaymentService,
              private activatedRoute: ActivatedRoute,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder,
              private branchService: BranchService,
              private branchBudgetService: BranchBudgetService,
              private userService: UserService,
              private confirmationService: ConfirmationService) {
    this.uploadBudgetTransferForm = this.fb.group({
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
      remarks: [null, [Validators.required, Validators.maxLength(50)]],
      assignedUserId: [0, Validators.required]
    })
  }

  get f() {
    return this.uploadBudgetTransferForm.controls;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadBudgetTransferForm.patchValue({
        fileSource: file
      });
    }
  }

  ngOnInit(): void {
    this.columnName = ColumnName.TRANSFER_BUDGET_ENTRY;
    this.reloadData();
    this.fetchAllUsers();
    this.currentUserId = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')))
      .getValue().userId;
  }

  reloadData() {
    this.branchBudgetService.fetchAllTransferBudgetEntry()
      .subscribe((data) => {
        this.data$ = data;
      }, error => {
      })
  }

  downloadSampleDocument() {
    this.branchBudgetService.getTransferBudgetSampleDoc()
      .subscribe((data) => {
        FileSaver.saveAs(data, "transfer_budget.xlsx");
      }, error1 => {
        console.log(error1);
      });
  }

  uploadFile() {
    this.submitted = false;
    this.showUploadFilePopUp = true;
  }

  fetchAllUsers() {
    this.userService.findAllBudgetTranferUsers().subscribe(data => {
      this.allUsers = data;
    })
  }

  cancelSubmit() {
    this.showUploadFilePopUp = false;
    this.uploadBudgetTransferForm.reset();
  }

  submitUploadForm() {
    this.submitted = true;
    if (this.uploadBudgetTransferForm.invalid) {
      return;
    }
    this.loading = true;
    const formData = new FormData();
    formData.append('files', this.uploadBudgetTransferForm.get('fileSource').value);
    this.branchBudgetService.transferBulkBudget(formData,
      this.uploadBudgetTransferForm.value.assignedUserId,
      this.uploadBudgetTransferForm.value.remarks).subscribe((data) => {
      this.loading = false;
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        false);
      //  this.reloadData(this.page, this.itemsPerPage);
      this.loading = false;
      this.uploadBudgetTransferForm.reset();
      this.showUploadFilePopUp = false;
      this.reloadData();
    }, error => {
      this.customMessageService.sendSuccessErrorMessage(error,
        'error', false);
      this.loading = false;
      this.uploadBudgetTransferForm.reset();
      this.showUploadFilePopUp = false;
    })
  }

  handleChange($event: any, entryId: number) {
    this.branchBudgetService.updateTransferBudgetEntryStatus(
      $event.checked ? 'ACTIVE' : 'INACTIVE',
      entryId).subscribe(() => {
      this.reloadData();
    }, error1 => {
      console.log(error1);
    });
  }


  confirm(entryId: number) {
    this.confirmationService.confirm({
      message: 'Are Sure you want to Approve ?',
      accept: () => {
        this.branchBudgetService.approveBudgetTransfer(entryId)
          .subscribe((data) => {
            this.customMessageService.sendSuccessErrorMessage(data.success_message,
              'success', true);
            this.reloadData();
          }, error => {
            this.customMessageService.sendSuccessErrorMessage(error, 'error',
              false);
          })
      }
    });
  }
}
