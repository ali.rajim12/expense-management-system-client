import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ColumnName} from '../../../shared/constant/column-name';
import {BranchBudgetService} from '../service/branch-budget.service';
import {BranchBudgetResource} from '../model/branch-budget-resource.model';
import {PreApprovalService} from '../../pre-approval/service/pre-approval.service';
import {FiscalYear} from '../../pre-approval/models/fiscal-year.model';
import {Branch, BranchService} from '../../branch';
import {BudgetHead, BudgetHeadService} from '../../budget-head';
import {AccountHead, AccountHeadService} from '../../account-head';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Month} from '../../pre-approval/models/month.model';
import {CommonVariableService} from '../../../shared';
import {CustomMessageService} from '../../../shared/service/custom-message.service';
import {Router} from "@angular/router";
import {User} from "../../../core/_models/user";
import * as FileSaver from "file-saver";
import {SelectItem} from 'primeng/api';
import {FilterService} from "../../shared";

@Component({
  selector: 'app-list-branch-budget',
  templateUrl: './list-branch-budget.component.html',
  styleUrls: ['./list-branch-budget.component.sass']
})
export class ListBranchBudgetComponent implements OnInit {
  data$: BranchBudgetResource[];
  columnName: any[];
  page = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  selectedItems: BranchBudgetResource;
  branchId: number = 0;
  fiscalYearId: number = 0;
  budgetHeadId: number = 0;
  accountHeadId: number = 0;
  fiscalYears: FiscalYear[];
  branches: Branch[];
  budgetHeads: BudgetHead[];
  accountHeads: AccountHead[];
  months: Month[];
  display: boolean = false;
  title: string;
  addEditBranchBudgetForm: FormGroup;
  budgetTransferForm: FormGroup;
  loading: boolean = false;
  displayBudgetTransferForm: boolean = false;
  submitted: boolean = false;
  canTransferBudget: boolean = false;
  selectedQuarter: number = 0;
  allQuarters = [
    {key: '1st Quarter', value: '1'},
    {key: '2nd Quarter', value: '2'},
    {key: '3rd Quarter', value: '3'},
    {key: '4th Quarter', value: '4'}];

  constructor(private branchBudgetService: BranchBudgetService,
              private preApprovalRequestService: PreApprovalService,
              private branchService: BranchService,
              private budgetHeadService: BudgetHeadService,
              private accountHeadService: AccountHeadService,
              private fb: FormBuilder,
              private commonVariableService: CommonVariableService,
              private customMessageService: CustomMessageService,
              private router: Router) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.addEditBranchBudgetForm = fb.group({
      branchId: null,
      fiscalYearId: null,
      month: null,
      accountHeadId: null,
      budget: null
    });
    this.budgetTransferForm = fb.group({
      fromAccountHeadId: {disabled: true},
      fromBranchId: {disabled: true},
      fromFiscalYearId: {disabled: true},
      fromMonth: {disabled: true},
      toAccountHeadId: [null, Validators.required],
      toBranchId: [null, Validators.required],
      toFiscalYearId: [null, Validators.required],
      toMonth: [null, Validators.required],
      amount: [null, Validators.required],
    })
  }

  ngOnInit(): void {
    this.columnName = ColumnName.BRANCH_BUDGET_DETAIL_TABLE;
    this.reloadData(this.page, 25);
    this.fetchAllFiscalYears();
    this.getAllBranch();
    this.getBudgetHead();
    this.fetchAllMonths();
    this.fetchAllAccountHeads();
    this.canTransferBudget = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')))
      .getValue().canTransferBudget;
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonths()
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }

  getAllBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.branches = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getBudgetHead() {
    this.budgetHeadService.getAllBudgetHeads().subscribe((data) => {
      this.budgetHeads = data;
    }, error1 => {
      console.log(error1);
    });
  }

  reloadData(page: number, size: number) {
    this.branchBudgetService.fetchAllBranchBudgets(this.branchId,
      this.fiscalYearId,
      this.budgetHeadId,
      this.accountHeadId,
      this.selectedQuarter,
      page,
      size).subscribe((response) => {
      this.data$ = response.results;
      this.totalRecords = response.totalResult;
    }, error1 => {
      console.log(error1);
    });
  }

  paginate(event) {
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  fetchAllAccountHeads() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  addEditBranchBudget(option: string, budgetDetail: any) {
    this.title = option;
    this.display = true;
    if (option === 'Edit') {
      this.selectedItems = budgetDetail;
      this.addEditBranchBudgetForm.controls.fiscalYearId.patchValue(budgetDetail.fiscalYearId);
      this.addEditBranchBudgetForm.controls.accountHeadId.patchValue(budgetDetail.accountHeadId);
      this.addEditBranchBudgetForm.controls.budget.patchValue(budgetDetail.budgetAmount);
      this.addEditBranchBudgetForm.controls.branchId.patchValue(budgetDetail.branchMiniResource.branchId);
      this.addEditBranchBudgetForm.controls.month.patchValue(budgetDetail.month);
    }
  }

  submit() {
    this.loading = true;
    this.branchBudgetService.submitBranchBudget(this.addEditBranchBudgetForm.value, this.selectedItems)
      .subscribe((data) => {
        this.loading = false;
        this.display = false;
        setTimeout(() => {
          this.customMessageService.sendSuccessErrorMessage(
            this.selectedItems === undefined ? 'Budget Added Successfully.' : "Budget Edited Successfully.",
            'success', true);
        }, 2000);
        this.reloadData(this.page, this.itemsPerPage);
      }, error1 => {
        this.loading = false;
        this.display = false;
        this.customMessageService.sendSuccessErrorMessage(error1, 'error', true);
      });
  }

  cancel() {
    this.display = false;
    this.addEditBranchBudgetForm.reset();
  }

  search() {
    this.reloadData(0, this.itemsPerPage);
  }

  reset() {
    this.branchId = 0;
    this.fiscalYearId = 0;
    this.budgetHeadId = 0;
    this.accountHeadId = 0;
    this.selectedQuarter = 0;
  }

  transferBudget(budgetDetail: any) {
    this.fetchAllAccountHeads();
    this.displayBudgetTransferForm = true;
    this.budgetTransferForm.controls.fromFiscalYearId.patchValue(budgetDetail.fiscalYearId);
    this.budgetTransferForm.controls.fromAccountHeadId.patchValue(budgetDetail.accountHeadId);
    // this.budgetTransferForm.controls.budget.patchValue(budgetDetail.budgetAmount);
    this.budgetTransferForm.controls.fromBranchId.patchValue(budgetDetail.branchMiniResource.branchId);
    this.budgetTransferForm.controls.fromMonth.patchValue(budgetDetail.month);

  }

  get f() {
    return this.budgetTransferForm.controls;
  }

  cancelTransfer() {
    this.submitted = false;
    this.displayBudgetTransferForm = false;
    this.budgetTransferForm.reset();
  }

  submitTransfer() {
    this.submitted = true;
    if (this.budgetTransferForm.invalid) {
      return;
    }
    this.loading = true;
    this.branchBudgetService.transferBranchBudget(this.budgetTransferForm.value)
      .subscribe((data) => {
        this.loading = false;
        this.displayBudgetTransferForm = false;
        setTimeout(() => {
          this.customMessageService.sendSuccessErrorMessage(
            "Branch Budget transferred successfully.",
            'success', true);
        }, 2000);
        this.reloadData(this.page, this.itemsPerPage);
        this.budgetTransferForm.reset();
      }, error1 => {
        this.loading = false;
        this.displayBudgetTransferForm = false;
        this.customMessageService.sendSuccessErrorMessage(error1, 'error', true);
      });
  }

  gotoBulkTransfer() {
    this.router.navigate(['/branch-budget/transfer'])
  }

  downloadExcel() {
    this.loading = true;
    if (this.selectedQuarter === 0) {
      this.customMessageService.sendSuccessErrorMessage('Please, select quarter',
        'error', false);
      this.loading = false;
      return;
    }
    this.branchBudgetService.downloadBranchBudgetExcel(this.branchId,
      this.fiscalYearId,
      this.budgetHeadId,
      this.accountHeadId,
      this.selectedQuarter)
      .subscribe((data) => {
        this.loading = false;
        FileSaver.saveAs(data, "branch_budget.xlsx");
      }, error1 => {
        this.loading = false;
        console.log(error1);
      });
  }


}
