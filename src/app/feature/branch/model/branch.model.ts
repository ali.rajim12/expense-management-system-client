export class Branch {
  branchId: number;
  name: string;
  address: string;
  code: string;
  status: string;
  createdDate: string;
}
