export class BranchMiniResource {
  name: string;
  code: string;
  branchId: number;
}
