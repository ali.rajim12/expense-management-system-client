import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vendor} from '../../vendors/model/vendor.model';
import {environment} from '../../../../environments/environment';
import {retry} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Branch} from '..';

@Injectable({
  providedIn: 'root'
})

export class BranchService {

  constructor(private http: HttpClient) {
  }

  getAllBranch(): Observable<Branch[]> {
    return this.http.get<Branch[]>(`${environment.apiUrl}/api/auth/branch`);
  }

  fetchSearchedVendor(vendorName: string): Observable<Vendor[]> {
    let params = new HttpParams();
    params = params.append('q', vendorName);
    return this.http.get<Vendor[]>(`${environment.apiUrl}/api/auth/vendors/search`, {params: params});
  }

  fetchBranchListForReports() {
    return this.http.get<Branch[]>(`${environment.apiUrl}/api/auth/branch/reports`);
  }
}
