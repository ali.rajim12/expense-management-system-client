import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {VendorsListComponent} from '../vendors/vendors-list/vendors-list.component';
import {NgModule} from '@angular/core';
import {ListBranchComponent} from './list-branch/list-branch.component';

const routes: Routes = [
  {
    path : 'branch',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: ListBranchComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BranchRouting {

}
