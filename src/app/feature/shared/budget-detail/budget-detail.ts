import {Component, OnInit} from '@angular/core';
import {BranchBudgetService} from '../../budget';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import {BudgetDetailResource} from '../../budget/model/budget-detail-resource';
import {BudgetExpenseDetail} from '../../budget/model/budget-expense-detail';

@Component({
  selector: 'budget-detail-popup',
  templateUrl: './budget-detail.html'
})
export class BudgetDetail implements OnInit {

  detail: BudgetDetailResource;

  constructor(private branchBudgetService: BranchBudgetService,
              public config: DynamicDialogConfig) {
  }

  ngOnInit() {
    if(this.config.data.expenditureDetailId) {
      this.fetchBillBudget();
    } else {
      this.fetchBudget();
    }
  }
  fetchBillBudget() {
    this.branchBudgetService.getBranchBillBudgetDetail(
      this.config.data.expenditureDetailId,
      this.config.data.expenditureTxnDetailId
    ).subscribe((data) => {
      this.detail = data;
    });
  }

  fetchBudget() {
    this.branchBudgetService.getBranchBudgetDetail(
      this.config.data.fiscalYearId,
      this.config.data.month,
      this.config.data.branchId,
      this.config.data.accountHeadId,
    ).subscribe((data) => {
      this.detail = data;
    });
  }

  getStatus(ytdBudgetBranch: BudgetExpenseDetail) {
    if(ytdBudgetBranch.totalExpense >= ytdBudgetBranch.totalBudget) {
      return 'NO';
    } else {
      return 'YES';
    }
  }

  getGrowth(detail: BudgetDetailResource) {
    if(detail.thisMonthBudget.totalExpense === 0) {
      return 0;
    }
    return ((detail.thisMonthBudget.totalExpense - detail.previousMonthBudget.totalExpense)/(detail.thisMonthBudget.totalExpense))*100;
  }

}
