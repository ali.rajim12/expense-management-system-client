import {Component, Input, OnInit} from '@angular/core';
import {ExpenditureTxnDetailTequest} from '../../pre-approval';
import {ExpenseDetail} from '../../pre-approval/models/Expense-detail.model';
import {BillPaymentService} from "../../bill-payment";
import {FetchBudgetParams} from "../../bill-payment/model/fetch.budget.params";
import {BudgetHead} from "../../budget-head";
import {UserGroup} from "../../users/model/user-group.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ColumnName} from "../../../shared/constant/column-name";
import {Branch, BranchService} from "../../branch";
import {AccountHead, AccountHeadService} from "../../account-head";
import {ExpenditureService} from "../../expenditure/expenditure.service";
import {CustomMessages} from "../../../shared/constant/custom-messages";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {takeUntil} from "rxjs/operators";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {JSON2SheetOpts} from "xlsx";
import {Observable} from "rxjs";
import {ExpenditureModel} from "../../pre-approval/models/expenditure.model";
import {ReportsService} from "../../reports/service/reports.service";

@Component({
  selector: 'app-expense-detail',
  templateUrl: './expense-detail.component.html',
  styleUrls: ['./expense-detail.component.css']
})
export class ExpenseDetailComponent implements OnInit {
  @Input() expenseDetail: ExpenseDetail[];
  @Input() isApprovedBillPayment: boolean;
  @Input() expenditureDetailId;
  @Input() showEdit: boolean;
  @Input() approvalType: string;
  @Input() billAmount: number;

  data$: ExpenseDetail[];
  columnName: any[];
  totalRecords: number;
  page: number;
  display: boolean;
  showConfirmation: boolean = false;
  updateTxnDetailForm: FormGroup;
  allBranch: Branch[];
  accountHeads: AccountHead[];
  submitted: boolean = false;
  loading: boolean = false;
  selectedValues: ExpenseDetail;
  expenditureTxnDetailId:  number;
  showAmountMissMatchMessage: boolean = false;
  exportColumns: any[];
  exportData: any[];
  selectedBranchId: number;
  selectedAccountHeadId: number;

  showReport: boolean = false;

  dataMemos$: Observable<ExpenditureModel[]>;
  totalMemosRecords: number;

  // columnName: any[];
  pageMemos: number = 0;
  itemsPerPageMemos: number = 10;
  rowsPerPageOptionsMemos: Array<any>;
  columnNameMemos: any[];


  constructor(private fb: FormBuilder,
              private accountHeadService: AccountHeadService,
              private expenditureService: ExpenditureService,
              private customMessageService: CustomMessageService,
              private preApprovalService: PreApprovalService,
              private branchService: BranchService,
              private reportService: ReportsService) {
    this.updateTxnDetailForm = fb.group({
      debitBranchId: [0, Validators.required],
      accountHeadId: [0, Validators.required],
      creditBranchId: [0, Validators.required],
      creditAccountNumber: ['', Validators.required],
      amount: [0, Validators.required],
      tdsAmount: [0, Validators.required],
      description: ['', Validators.required],
      remarks: ['', Validators.required]
    })
  }

  get f() {
    return this.updateTxnDetailForm.controls;
  }

  ngOnInit(): void {
    this.data$ = this.expenseDetail;
    this.columnName = ColumnName.EXPENSE_DETAIL;
    this.columnNameMemos = ColumnName.ALL_MEMO_REPORTS;
    this.fetchAllAccountHeads();
    this.fetchAllBranch();
    this.totalRecords = this.expenseDetail.length;
    this.exportColumns = this.columnName.map(col => ({title: col.name, dataKey: col.value}));
  }

  updateDetail(detail: ExpenseDetail,
               expenditureTxnDetailId: number) {
    this.expenditureTxnDetailId = expenditureTxnDetailId;
    this.showConfirmation = true;
    this.updateTxnDetailForm.controls.debitBranchId.patchValue(
      this.getDebitBranch(detail.debitBranch.branchId));
    this.updateTxnDetailForm.controls.accountHeadId.patchValue(
      this.getAccountHead(detail.accountHeadId));
    this.updateTxnDetailForm.controls.creditBranchId.patchValue(
      this.getDebitBranch(detail.creditBranch.branchId));
    this.updateTxnDetailForm.controls.creditAccountNumber.patchValue(detail.creditAccount);
    this.updateTxnDetailForm.controls.amount.patchValue(detail.amount);
    this.updateTxnDetailForm.controls.tdsAmount.patchValue(detail.tdsAmount);
    this.updateTxnDetailForm.controls.description.patchValue(detail.expenseDescription);
    this.selectedValues = detail;
  }

  getPreApprovalDetail(id: number) {
    this.preApprovalService.fetchPreApprovalDetail(id)
      .subscribe((response) => {
        this.data$  = response.expenditureTxnDetailResources;
        this.expenseDetail  = response.expenditureTxnDetailResources;
      }, error1 => console.log(error1));
  }

  fetchAllBranch() {
    this.branchService.getAllBranch()
      .subscribe((data) => {
        this.allBranch = data;
      }, error1 => {
        console.log(error1);
      });
  }

  fetchAllAccountHeads() {
    this.accountHeadService.fetchAllAccountHead()
      .subscribe((response) => {
        this.accountHeads = response;
      }, error1 => console.log(error1));
  }

  submitDetail() {
    this.submitted = true;
    if (this.updateTxnDetailForm.invalid) {
      return;
    }
    if(Number.parseInt(String(this.billAmount)) < Number.parseInt(String(
        this.calculateTotalAmount(this.updateTxnDetailForm.value.amount)))) {
      this.showAmountMissMatchMessage = true;
      return;
    }

    this.loading = true;
    this.expenditureService.updateTransactionDetail(this.updateTxnDetailForm.value,
      this.expenditureTxnDetailId, this.expenditureDetailId)
      .subscribe((data) => {
        this.loading = false;
        this.showAmountMissMatchMessage = false;
        this.showConfirmation = false;
        this.getPreApprovalDetail(this.expenditureDetailId);
        this.customMessageService.sendSuccessErrorMessage(
          data.success_message,
          'success', false);
      }, error => {
        this.loading = false;
        this.showConfirmation = false;
        this.submitted = false;
        this.customMessageService.sendSuccessErrorMessage(error,
          'error', false);
      })
    this.submitted = false;
    this.updateTxnDetailForm.reset();
  }

  private calculateTotalAmount(updatedAmount: number) {
    let totalAmount: number = 0;
    this.expenseDetail.forEach((value) =>  {
      totalAmount = totalAmount + value.amount;
    })
    totalAmount =  totalAmount - this.selectedValues.amount + updatedAmount;
    return Math.round((totalAmount + Number.EPSILON) * 100) / 100;
  }

  private getDebitBranch(branchId: number) {
    let requiredBranch: number = null;
    this.allBranch.forEach(value => {
      if (value.branchId === branchId) {
        requiredBranch = value.branchId;
      }
    });
    return requiredBranch;
  }

  private getAccountHead(accountHeadId: number) {
    if (this.accountHeads === undefined) {
      this.fetchAllAccountHeads();
    }
    let requiredAccountHead: number = null;
    this.accountHeads.forEach(value => {
      if (value.accountHeadId === accountHeadId) {
        requiredAccountHead = value.accountHeadId;
      }
    });
    return requiredAccountHead;
  }
  cancel() {
    this.showConfirmation = false;
    this.showAmountMissMatchMessage = false;
    this.updateTxnDetailForm.reset();
    this.submitted = false;
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      this.exportData = this.data$.map(col => ({
        'Debit Branch': col.debitBranchName,
        'Account Head': col.accountHead,
        'Credit Branch': col.creditBranchName,
        'Credit Ac': col.creditAccount,
        'Payable Amt': col.amount,
        'TDS Amt': col.tdsAmount,
        'Narration': col.expenseDescription}))
      const worksheet = xlsx.utils.json_to_sheet(this.exportData);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "expenses");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  showAllMemos(branchId: number, accountHeadId: number) {
    this.selectedBranchId = branchId;
    this.selectedAccountHeadId = accountHeadId;
    this.showReport = true;
    this.reloadMemos(this.pageMemos, this.itemsPerPageMemos);
  }
  reloadMemos(page: number, size: number) {
    this.reportService.fetchAllMemosAccountHeadWise(this.selectedBranchId, this.selectedAccountHeadId,
      page, size)
      .subscribe((response) => {
        this.dataMemos$ = response.results;
        this.totalMemosRecords = response.totalResult;
      }, error1 => console.log(error1))
  }

  paginate(event) {
    this.pageMemos = event.pageMemos;
    const pageIndex = event.first / event.rows;
    this.reloadMemos(pageIndex, event.rows);
  }

  goTo() {
    // [routerLink]="['/pre-approval', list.id, 'BILL_PAYMENT']"
    window.open('/pre-approval/' + this.expenditureTxnDetailId+'/BILL_PAYMENT', "_blank");
  }
}
