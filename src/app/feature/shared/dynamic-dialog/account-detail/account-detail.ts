import {BudgetDetailResource} from "../../../budget/model/budget-detail-resource";
import {BranchBudgetService} from "../../../budget";
import {DynamicDialogConfig} from "primeng/dynamicdialog";
import {BudgetExpenseDetail} from "../../../budget/model/budget-expense-detail";
import {Component, OnInit} from "@angular/core";
import {PreApprovalService} from "../../../pre-approval/service/pre-approval.service";
import {AccountDetailModel} from "../model/account-detail.model";

@Component({
  selector: 'account-detail-popup',
  templateUrl: './account-detail.html'
})
export class AccountDetail implements OnInit {
  detail: any;

  constructor(private preApprovalService: PreApprovalService,
              public config: DynamicDialogConfig) {
  }

  ngOnInit() {
    this.fetchAccountDetail();
  }

  fetchAccountDetail() {
    this.preApprovalService.getAccountDetail(
      this.config.data.accountNumber,
    ).subscribe((data) => {
      this.detail = data;
    });
  }

}
