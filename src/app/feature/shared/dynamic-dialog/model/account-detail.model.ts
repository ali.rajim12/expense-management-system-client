export class AccountDetailModel {
  AccountNumber: string;
  Mobile: string;
  CifId: string;
  Email: string;
  AccountName: string;
  DOB: string;
}
