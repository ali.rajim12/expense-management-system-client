import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {BudgetDetail} from '../budget-detail/budget-detail';
import {AccountDetail} from "./account-detail/account-detail";

@Component({
  selector: 'app-dynamic-dialog',
  templateUrl: './dynamic-dialog.component.html',
  styleUrls: ['./dynamic-dialog.component.sass'],
  providers: [DialogService]
})
export class DynamicDialogComponent implements OnInit, OnDestroy {
  @Input() fiscalYearId: number;
  @Input() month: string;
  @Input() branchId: number;
  @Input() accountHeadId;
  @Input() popupType;
  @Input() accountNumber;
  @Input() expenditureDetailId;
  @Input() expenditureTxnDetailId;


  constructor(public dialogService: DialogService) {}

  ngOnInit(): void {
  }

  showBillBudget() {
    const ref = this.dialogService.open(BudgetDetail, {
      data: {
        expenditureDetailId: this.expenditureDetailId,
        expenditureTxnDetailId: this.expenditureTxnDetailId
      },
      header: 'Budget Detail',
      width: '60%',
      height: '60%'
    });
  }

  show() {
    const ref = this.dialogService.open(BudgetDetail, {
      data: {
        fiscalYearId: this.fiscalYearId,
        month: this.month,
        branchId: this.branchId,
        accountHeadId:  this.accountHeadId,
      },
      header: 'Budget Detail',
      width: '60%'
    });
  }

  showAccountDetail() {
    const ref = this.dialogService.open(AccountDetail, {
      data: {
        accountNumber: this.accountNumber,
      },
      header: 'Account Detail',
      width: '40%'
    });
  }
  ngOnDestroy() {

  }

}
