import {Component, Input, OnInit} from '@angular/core';
import {PreApprovalDetail} from '../../pre-approval';
import {AttachmentDetail} from '../../pre-approval/models/attachment-detail.model';
import {SupportedApprovedDetail} from '../../pre-approval/models/supported-approved-detail';
import {PreApprovalService} from '../../pre-approval/service/pre-approval.service';
import * as FileSaver from 'file-saver';


@Component({
  selector: 'app-view-doc',
  templateUrl: './view-doc.component.html',
  styleUrls: ['./view-doc.component.sass']
})
export class ViewDocComponent implements OnInit {

  @Input() public type: string;
  @Input() public expenditureDetailId: number;
  preApprovalAttachments: AttachmentDetail[];
  billPaymentAttachments: AttachmentDetail[];

  constructor(private preApprovalService: PreApprovalService) { }

  ngOnInit(): void {
    // this.fetchPreApprovalAttachments('PRE_APPROVAL');
    // this.fetchBillPaymentAttachments('BILL_PAYMENT');
  }

  fetchPreApprovalAttachments(type: string) {
    if(this.preApprovalAttachments === undefined || this.preApprovalAttachments.length === 0) {
      this.preApprovalService.fetchExpenditureAttachments(this.expenditureDetailId, type)
        .subscribe((data)=> {
          this.preApprovalAttachments = data;
        })
    }
  }

  fetchBillPaymentAttachments(type: string) {
    if(this.billPaymentAttachments === undefined || this.billPaymentAttachments.length === 0) {
      this.preApprovalService.fetchExpenditureAttachments(this.expenditureDetailId, type)
        .subscribe((data)=> {
          this.billPaymentAttachments = data;
        })
    }
  }

  download(attachmentId: number, fileName: string) {
    this.preApprovalService.downloadFile(attachmentId).subscribe(
        (data)=> {
          FileSaver.saveAs(data, fileName);
        }, error => {
          console.log(error)
        }
    )
  }
}
