import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExpenditureService} from '../../expenditure/expenditure.service';
import {BillDetail} from '../../expenditure/model/bill-detail.model';
import {PreApprovalService} from '../../pre-approval/service/pre-approval.service';

@Component({
  selector: 'app-view-bill-detail',
  templateUrl: './view-bill-detail.component.html',
  styleUrls: ['./view-bill-detail.component.sass']
})
export class ViewBillDetailComponent implements OnInit {
  billDetail: BillDetail;
  @Input() expDetailId: number;
  @Output() billNumber= new EventEmitter();

  constructor(private expenditureService: ExpenditureService, private preApprovalService: PreApprovalService) { }

  ngOnInit(): void {
    this.getBillDetail()
  }

  getBillDetail() {
    this.preApprovalService.getExpenditureBillDetail(this.expDetailId)
      .subscribe((data)=> {
      this.billDetail = data;
      if(this.billDetail !== undefined) {
        this.emitBillNumber(this.billDetail.billNumber);
      }
    }, error1 => {
    })
  }

  emitBillNumber(billNumber: string) {
    this.billNumber.emit({ billNumber: billNumber });
  }

}
