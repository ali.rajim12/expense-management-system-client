import {Component, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-recent-status',
  templateUrl: './recent-status.component.html'
})
export class RecentStatusComponent implements OnInit {

  recentStatus = ['ALL',
    'REQUESTED',
    'SUPPORTED',
    'APPROVED',
    'RETURNED',
    'REJECTED',
    'PAYMENT_REQUESTED',
    'PAID'
  ];
  @Output() selectedRecentStatus = 'ALL';

  constructor() { }

  ngOnInit(): void {
  }

}
