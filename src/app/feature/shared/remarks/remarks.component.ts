import {Component, Input, OnInit} from '@angular/core';
import {ExpenditureRemarks} from '../../pre-approval/models/expenditure-remarks.model';
import {PreApprovalService} from '../../pre-approval/service/pre-approval.service';
import {takeUntil} from 'rxjs/operators';
import {ColumnName} from '../../../shared/constant/column-name';

@Component({
  selector: 'app-remarks',
  templateUrl: './remarks.component.html',
  styleUrls: ['./remarks.component.sass']
})
export class RemarksComponent implements OnInit {
  @Input() expenditureDetailId: number;
  preApprovalRemarks: ExpenditureRemarks[];
  billPaymentRemarks: ExpenditureRemarks[];
  columnName: any[];
  @Input() type: string;

  constructor(private preApprovalService: PreApprovalService) { }

  ngOnInit(): void {
    this.columnName = ColumnName.COMMENT_COLUMN_LIST;
    this.getPreApprovalRemarks('PRE_APPROVAL');
    this.getBillPaymentRemarks('BILL_PAYMENT');
  }

  getPreApprovalRemarks(type: string) {
    this.preApprovalService.fetchComments(this.expenditureDetailId, type)
      .subscribe((response) => {
        this.preApprovalRemarks = response;
      }, error1 => console.log(error1));
  }

  getBillPaymentRemarks(type: string) {
    this.preApprovalService.fetchComments(this.expenditureDetailId, type)
      .subscribe((response) => {
        this.billPaymentRemarks = response;
      }, error1 => console.log(error1));
  }

}
