import {Component, Input, OnInit} from '@angular/core';
import {PreApprovalDetail} from '../../pre-approval';
import {SupportedApprovedDetail} from '../../pre-approval/models/supported-approved-detail';
import {BehaviorSubject} from 'rxjs';
import {User} from '../../../core/_models/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PreApprovalService} from '../../pre-approval/service/pre-approval.service';
import {CustomMessages} from '../../../shared/constant/custom-messages';
import {CustomMessageService} from '../../../shared/service/custom-message.service';
import {Router} from '@angular/router';
import {UserService} from "../../users/service/user.service";
import {UserMini} from "../../users/model/user-mini.model";

@Component({
  selector: 'app-support-approve-detail',
  templateUrl: './support-approve-detail.component.html',
  styleUrls: ['./support-approve-detail.component.css']
})
export class SupportApproveDetailComponent implements OnInit {

  @Input() preApprovalDetail: PreApprovalDetail;
  display: boolean = false;
  performedAction: string;
  approveSupportConfirmForm: FormGroup;
  @Input() type: string;
  @Input() id: number;
  preApprovalRequests: SupportedApprovedDetail[];
  billPaymentRequests: SupportedApprovedDetail[];
  loading: boolean = false;
  submitted: boolean = false;
  approver: UserMini[];

  constructor(private fb: FormBuilder,
              private preApprovalService: PreApprovalService,
              private router: Router,
              private customMessageService: CustomMessageService,
              private userService: UserService) {
    this.approveSupportConfirmForm = fb.group({
      comments: [null, Validators.required],
      approvers: null
    });
  }

  ngOnInit(): void {
    this.getPreApprovalList(this.preApprovalDetail.approvalDetail, 'PRE_APPROVAL');
    this.getBillPaymentApprovalList(this.preApprovalDetail.approvalDetail, 'BILL_PAYMENT');
    // this.orderDetail();
  }


  getPreApprovalList(approvalDetail: SupportedApprovedDetail[], type: string) {
    let requiredList: SupportedApprovedDetail[] = [];
    approvalDetail.forEach(value => {
      if (value.type === type) {
        requiredList.push(value);
      }
    });
    this.preApprovalRequests = requiredList;
  }

  getBillPaymentApprovalList(approvalDetail: SupportedApprovedDetail[], type: string) {
    let requiredList: SupportedApprovedDetail[] = [];
    approvalDetail.forEach(value => {
      if (value.type === type) {
        requiredList.push(value);
      }
    });
    this.billPaymentRequests = requiredList;
  }


  takeAction(action: string) {
    this.display = true;
    this.performedAction = action;

  }

  searchApprover(event: any) {
    this.userService.getAllApprovers(event.query).subscribe(data => {
      this.approver = data;
    });
  }


  isValidUser(id: number) {
    let userId = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user'))).getValue().userId;
    if (userId === id) {
      return true;
    }
    return false;
  }

  get f() {
    return this.approveSupportConfirmForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.approveSupportConfirmForm.invalid) {
      return;
    }
    this.loading = true;
      let submitRequest = {
        comments: this.approveSupportConfirmForm.value.comments,
        status: this.performedAction === 'APPROVE' ? this.performedAction + 'D' : this.performedAction + 'ED',
        type: this.type,
        userId: this.performedAction === 'FORWARD'? this.approveSupportConfirmForm.value.approvers[0].userId : 0,
      };
      this.preApprovalService.addCommentAndUpdateStatus(submitRequest, this.id)
        .subscribe(response => {
          this.display = false;
          this.loading = false;
          setTimeout(() => {
            this.customMessageService.sendSuccessErrorMessage(response.success_message, 'success', true);
          }, 1000);
          if (this.performedAction === 'APPROVE') {
            this.router.navigate([this.type === 'PRE_APPROVAL' ? '/pre-approval/approve' : '/bill-payment/approve-request']);
          } else {
            this.router.navigate([this.type === 'PRE_APPROVAL' ? '/pre-approval/support' : '/bill-payment/support-request']);
          }
        }, error => {
          this.display = false;
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(error, 'error', false);
        });
  }
}
