import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor() {
  }
  filterData: Array<any>;

  filterAnyData(event, searchList: Array<any>) {
    let filtered: any[] = [];
    if(event === null) {
      return searchList;
    }
    let query = event.query;
    for (let i = 0; i < searchList.length; i++) {
      let country = searchList[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }
    return filtered;
  }
}

