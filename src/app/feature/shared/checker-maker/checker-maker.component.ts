import {Component, Input, OnInit} from '@angular/core';
import {MakePaymentService} from "../../payments/service/make.payment.service";
import {CheckerMaker} from "../../payments/model/checker.maker.model";

@Component({
  selector: 'app-checker-maker',
  templateUrl: './checker-maker.component.html',
  styleUrls: ['./checker-maker.component.sass']
})
export class CheckerMakerComponent implements OnInit {

  @Input() expenditureDetailId: number;
  checkerMakerDetail: CheckerMaker;

  constructor(private makePaymentService: MakePaymentService) {
  }

  ngOnInit(): void {
    this.fetchCheckerMakerDetail();
  }

  fetchCheckerMakerDetail() {
    this.makePaymentService.fetchCheckerMakerDetail(this.expenditureDetailId)
      .subscribe((data) => {
        this.checkerMakerDetail = data;
      }, error => {
        console.log(error);
      })
  }
}
