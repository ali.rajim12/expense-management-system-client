import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {NgModule} from '@angular/core';
import {UsersComponent} from './users.component';
import {UserGroupComponent} from './user-group/user-group.component';
import {GroupTypeComponent} from './group-type/group-type.component';
import {DepartmentComponent} from './department/department.component';
import {DesignationsComponent} from './designations/designations.component';
import {AddNewUserComponent} from './add-new-user/add-new-user.component';
import {ProfileComponent} from "./profile/profile.component";

const routes: Routes = [
  {
    path : 'users',
    component: MainLayoutComponent,
    children: [
      {
        path: 'all',
        component: UsersComponent,
      },
      {
        path: 'create/:id',
        component:AddNewUserComponent
      },
      {
        path: 'group',
        component: UserGroupComponent
      },
      {
        path: 'group-type',
        component: GroupTypeComponent
      },
      {
        path: 'departments',
        component: DepartmentComponent
      },
      {
        path: 'designation',
        component: DesignationsComponent
      },
      {
       path: 'profile',
       component: ProfileComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRouting {

}
