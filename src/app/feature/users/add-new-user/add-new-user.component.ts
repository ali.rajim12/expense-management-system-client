import {Component, OnInit} from '@angular/core';
import {Designation} from '../model/designation.model';
import {UserGroup} from '../model/user-group.model';
import {Department} from '../model/department.model';
import {UserGroupType} from '../model/user-group-type.model';
import {Branch, BranchService} from '../../branch';
import {Users} from '../model/users.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DepartmentService} from '../service/department.service';
import {UserGroupService} from '../service/user-group.service';
import {UserGroupTypeService} from '../service/user-group-type.service';
import {DesignationService} from '../service/designation.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {UserMini} from '../model/user-mini.model';
import {CreateUser} from '../model/create-user.model';
import {User} from '../../../core/_models/user';
import {of} from 'rxjs';
import {every, map, timeout} from 'rxjs/operators';
import {FiscalYear} from '../../pre-approval/models/fiscal-year.model';
import {CustomMessages} from "../../../shared/constant/custom-messages";
import {CustomMessageService} from "../../../shared/service/custom-message.service";

@Component({
  selector: 'app-add-new-user',
  templateUrl: './add-new-user.component.html',
  styleUrls: ['./add-new-user.component.css']
})
export class AddNewUserComponent implements OnInit {
  designations: Designation[];
  userGroups: UserGroup[];
  departments: Department[];
  groupTypes: UserGroupType[];
  branchs: Branch[];
  parents: UserMini[];
  showConfirmation: boolean = false;
  addNewUserForm: FormGroup;
  userId: string;
  addEdit: string;
  user: Users;
  submitted: boolean = false;
  loading: boolean = false;


  constructor(private fb: FormBuilder,
              private branchService: BranchService,
              private departmentService: DepartmentService,
              private userGroupService: UserGroupService,
              private groupTypeService: UserGroupTypeService,
              private designationService: DesignationService,
              private userService: UserService,
              private router: Router,
              private customMessageService: CustomMessageService,
              private activeRoute: ActivatedRoute) {
    this.userId = this.activeRoute.snapshot.params.id;
    this.addEdit = this.userId === 'new' ? 'Add' : 'Edit';
    this.addNewUserForm = this.fb.group({
      fullName: [null, Validators.required],
      mobileNumber: [null, Validators.required],
      username: [null, Validators.required],
      email: [null, Validators.required],
      employeeId: [null, Validators.required],
      designation: [null, Validators.required],
      branch: [null, Validators.required],
      groupType: [null],
      department: [null, Validators.required],
      parent: null,
      userGroup: [null, Validators.required],
      canApprove: [false],
      canSupport: [false],
      canMakePayment: [false],
      canVerify: [false]
    });
  }

  get f() {
    return this.addNewUserForm.controls;
  }

  ngOnInit(): void {
    this.getAllActiveBranch();
    this.getAllDepartments();
    this.getAllUserGroups();
    this.getAllGroupTypes();
    this.getDesignation();
    this.getAllMiniUsersList();
    if (this.userId !== 'new') {
      this.getUsrDetail();
    }
  }

  getAllMiniUsersList() {
    this.userService.getAllMiniUsers().subscribe((data) => {
      this.parents = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAllActiveBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.branchs = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAllDepartments() {
    this.departmentService.getAllDepartments().subscribe((data) => {
      this.departments = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAllUserGroups() {
    this.userGroupService.getAllUserGroups().subscribe((data) => {
      this.userGroups = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAllGroupTypes() {
    this.groupTypeService.getAllUserGroupTypes().subscribe((data) => {
      this.groupTypes = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getDesignation() {
    this.designationService.getAllDesignations().subscribe((data) => {
      this.designations = data;
    }, error1 => {
      console.log(error1);
    });
  }

  cancel() {
    this.addNewUserForm.reset();
    this.router.navigate(['/users/all']);
  }

  Confirm() {
    this.submitted = true;
    if (this.addNewUserForm.invalid) {
      return;
    }
    this.showConfirmation = true;
  }

  onSubmit() {
    this.loading = true;
    let detail = {
      fullName: this.addNewUserForm.value.fullName,
      mobileNumber: this.addNewUserForm.value.mobileNumber,
      userName: this.addNewUserForm.value.username,
      email: this.addNewUserForm.value.email,
      employeeId: this.addNewUserForm.value.employeeId,
      designationId: this.addNewUserForm.value.designation.designationId,
      branchId: this.addNewUserForm.value.branch.branchId,
      groupTypeId: this.addNewUserForm.value.groupType === null ? 0 : this.addNewUserForm.value.groupType.userGroupTypeId,
      departmentId: this.addNewUserForm.value.department.departmentId,
      parentId: this.addNewUserForm.value.parent === null ? 0 : this.addNewUserForm.value.parent.userId,
      userGroupId: this.addNewUserForm.value.userGroup.userGroupId,
      canApprove: this.addNewUserForm.value.canApprove,
      canSupport: this.addNewUserForm.value.canSupport,
      canMakePayment: this.addNewUserForm.value.canMakePayment,
      canVerify: this.addNewUserForm.value.canVerify
    };
    if (this.userId !== 'new') {
      this.userService.updateUser(this.userId, detail)
        .subscribe((data) => {
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(
            data.success_message,
            'success', true);
          this.router.navigate(['/users/all']);
        }, error1 => {
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(
            error1,
            'error', false);
        });
    } else {
      this.userService.createNewUser(detail)
        .subscribe((data) => {
          this.customMessageService.sendSuccessErrorMessage(
            data.success_message,
            'success', true);
          this.router.navigate(['/users/all']);
        }, error1 => {
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(
            error1,
            'error', false);
        });
    }
  }

  private getUsrDetail() {
    this.userService.getUserDetailById(this.userId)
      .subscribe((data) => {
        of(this.designations)
          .pipe(map(results => results
            .filter(r => r.value === data.designation)))
          .subscribe(value => {
            this.addNewUserForm.controls.designation.patchValue(value[0]);
          });
        of(this.branchs)
          .pipe(map(results => results
            .filter(r => r.name === data.branchName)))
          .subscribe(value => {
            this.addNewUserForm.controls.branch.patchValue(value[0]);
          });

        this.addNewUserForm.controls.fullName.patchValue(data.fullName);
        this.addNewUserForm.controls.mobileNumber.patchValue(data.mobileNumber);
        this.addNewUserForm.controls.username.patchValue(data.username);
        this.addNewUserForm.controls.email.patchValue(data.email);
        this.addNewUserForm.controls.employeeId.patchValue(data.employeeId);
        this.addNewUserForm.controls.groupType.patchValue(this.setGroupType(data.groupType));
        this.addNewUserForm.controls.department.patchValue(this.setDepartments(data.department));
        if (data.parent !== null) {
          this.addNewUserForm.controls.parent.patchValue(this.setParent(data.parent.userId));
        }
        this.addNewUserForm.controls.userGroup.patchValue(this.setUserGroup(data.userGroup));
        this.addNewUserForm.controls.canApprove.patchValue(data.canApprove);
        this.addNewUserForm.controls.canSupport.patchValue(data.canSupport);
        this.addNewUserForm.controls.canMakePayment.patchValue(data.canMakePayment);
        this.addNewUserForm.controls.canVerify.patchValue(data.canVerify);
      }, error1 => {
        console.log(error1);
      });
  }

  setUserGroup(name: string) {
    let requiredValue: UserGroup = null;
    this.userGroups.forEach(value => {
      if (value.description === name) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }

  setParent(userId: number) {
    let requiredValue: UserMini = null;
    this.parents.forEach(value => {
      if (value.userId === userId) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }


  setGroupType(data: string) {
      let requiredValue: UserGroupType = null;
      this.groupTypes.forEach(value => {
        if (value.name === data) {
          requiredValue = value;
        }
      });
      return requiredValue;
  }

  private setDepartments(department: string) {
    let requiredValue: Department = null;
    this.departments.forEach(value => {
      if (value.name === department) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }
}
