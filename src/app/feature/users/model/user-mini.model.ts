export class UserMini {
  fullName: string;
  userName: string;
  userId: number;
  employeeId?: string;
  designation: string;
  branchName: string
}
