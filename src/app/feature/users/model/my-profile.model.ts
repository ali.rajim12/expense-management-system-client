export class MyProfileModel {
   branch: string;
   branchId: number;
   fullName: string;
   username: string;
   department: string;
   departmentId: number;
   userGroup: string;
   userGroupId: number;
   mobileNumber: string;
   email: string;
   status: string;
   employeeId: string;
   designations: string;
   designationId: number;
   canApprove: boolean;
   canSupport: boolean;
   canMakePayment: boolean;
   canVerify: boolean;
   canReject: boolean;
   groupType: string;
   groupTypeId: number;
   reportAccess: string;
   canTransferBudget: boolean;

}
