export class CreateUser {
  fullName: string;
  mobileNumber: string;
  userName: string;
  email: string;
  employeeId: string;
  designationId: number;
  branchId: number;
  groupTypeId: number;
  departmentId: number;
  parentId: number;
  userGroupId: number;
  canApprove: boolean;
  canSupport: boolean;
  canMakePayment: boolean;
}
