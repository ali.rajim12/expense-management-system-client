export class Department {
  name: string;
  code: string;
  status: string;
  createdDate: string;
  departmentId: number;
}
