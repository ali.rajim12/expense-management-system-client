export class Designation {
  name: string;
  value: string;
  designationId: number;
  createdDate: string;
}
