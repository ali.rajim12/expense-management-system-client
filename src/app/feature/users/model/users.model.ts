import {UserMini} from './user-mini.model';

export class Users {
  branchName: string;
  fullName: string;
  username: string;
  department: string;
  userGroup: string;
  mobileNumber: string;
  email: string;
  status: string;
  parent: UserMini;
  employeeId: string;
  designation: string;
  canApprove: boolean;
  canSupport: boolean;
  canVerify: boolean;
  canMakePayment: boolean;
  groupType: string;
}
