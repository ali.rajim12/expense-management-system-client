export class UserGroup {
  code: string;
  description: string;
  status: string;
  createdDate: string;
  userGroupId: number;
}
