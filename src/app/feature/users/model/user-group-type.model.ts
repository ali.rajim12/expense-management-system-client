export class UserGroupType {
  name: string;
  maximumLimit: number;
  status : string;
  createdDate: string;
  userGroupTypeId: number;
}
