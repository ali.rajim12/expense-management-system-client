import {Component, OnInit} from '@angular/core';
import {UserService} from "../service/user.service";
import {MyProfileModel} from "../model/my-profile.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Department} from "../model/department.model";
import {Designation} from "../model/designation.model";
import {Branch, BranchService} from "../../branch";
import {DepartmentService} from "../service/department.service";
import {DesignationService} from "../service/designation.service";
import {UserGroupType} from "../model/user-group-type.model";
import {UserGroupTypeService} from "../service/user-group-type.service";
import {CustomMessageService} from "../../../shared/service/custom-message.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  myProfile: MyProfileModel;
  display: boolean = false;
  editUserProfileForm: FormGroup;
  submitted: boolean = false;
  departments: Department[];
  designations: Designation[];
  branches: Branch[];
  groupTypes: UserGroupType[];
  loading: boolean = false;


  constructor(private userService: UserService,
              private fb: FormBuilder,
              private branchService: BranchService,
              private departmentService: DepartmentService,
              private designationService: DesignationService,
              private groupTypeService: UserGroupTypeService,
              private customMessageService: CustomMessageService
  ) {
    this.editUserProfileForm = fb.group({
      fullName: [null, Validators.required],
      email: [null, Validators.required],
      mobileNumber: [null, Validators.required],
      branchId: [null, Validators.required],
      departmentId: [null, Validators.required],
      employeeId: [null, Validators.required],
      designationId: [null, Validators.required],
      groupTypeId: [null, Validators.required],


    });
  }

  ngOnInit(): void {
    this.getMyProfile();
    this.getAllActiveBranch();
    this.getAllDepartments();
    this.getDesignation();
    this.getAllGroupTypes();
  }

  get f() {
    return this.editUserProfileForm.controls;
  }

  getMyProfile() {
    this.userService.getMyProfile().subscribe((data) => {
        this.myProfile = data;
      }, error => {
        console.log(error);
      }
    )
  }

  editUserProfile() {
    this.display = true;
    this.editUserProfileForm.controls.fullName.patchValue(this.myProfile.fullName);
    this.editUserProfileForm.controls.mobileNumber.patchValue(this.myProfile.mobileNumber);
    this.editUserProfileForm.controls.branchId.patchValue(this.myProfile.branchId);
    this.editUserProfileForm.controls.departmentId.patchValue(this.myProfile.departmentId);
    this.editUserProfileForm.controls.email.patchValue(this.myProfile.email);
    this.editUserProfileForm.controls.employeeId.patchValue(this.myProfile.employeeId);
    this.editUserProfileForm.controls.designationId.patchValue(this.myProfile.designationId);
    this.editUserProfileForm.controls.groupTypeId.patchValue(this.myProfile.groupTypeId);
  }


  getAllActiveBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.branches = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAllDepartments() {
    this.departmentService.getAllDepartments().subscribe((data) => {
      this.departments = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getDesignation() {
    this.designationService.getAllDesignations().subscribe((data) => {
      this.designations = data;
    }, error1 => {
      console.log(error1);
    });
  }

  getAllGroupTypes() {
    this.groupTypeService.getAllUserGroupTypes().subscribe((data) => {
      this.groupTypes = data;
    }, error1 => {
      console.log(error1);
    });
  }

  submit() {
    this.submitted = true;
    if (this.editUserProfileForm.invalid) {
      return;
    }
    this.loading = true;
    this.userService.updateMyProfile(this.editUserProfileForm.value)
      .subscribe((data) => {
        this.myProfile = data
        this.display = false;
        this.loading = false;
        this.submitted = false;
      }, error => {
        this.display = false;
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(error, 'error', true);
        this.submitted = false;
      })
  }

  cancel() {
    this.submitted = false;
    this.display = false;
    this.loading = false;
  }


}
