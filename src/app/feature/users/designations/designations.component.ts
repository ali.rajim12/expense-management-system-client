import { Component, OnInit } from '@angular/core';
import {ColumnName} from '../../../shared/constant/column-name';
import {Designation} from '../model/designation.model';
import {DesignationService} from '../service/designation.service';

@Component({
  selector: 'app-designations',
  templateUrl: './designations.component.html',
  styleUrls: ['./designations.component.sass']
})
export class DesignationsComponent implements OnInit {

  data$: Designation[];
  columnName: any[];
  totalRecords: number;
  constructor(private designationService: DesignationService) {
  }

  ngOnInit(): void {
    this.columnName = ColumnName.DESIGNATION_TABLE;
    this.reloadData();
  }

  reloadData() {
    this.designationService.getAllDesignations()
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = this.data$.length;
      }, error1 => console.log(error1));
  }

}
