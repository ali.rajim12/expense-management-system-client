import { Component, OnInit } from '@angular/core';
import {ConfirmationService} from 'primeng/api';
import {ColumnName} from '../../../shared/constant/column-name';
import {UserGroupType} from '../model/user-group-type.model';
import {UserGroupTypeService} from '../service/user-group-type.service';

@Component({
  selector: 'app-group-type',
  templateUrl: './group-type.component.html',
  styleUrls: ['./group-type.component.sass']
})
export class GroupTypeComponent implements OnInit {
  data$: UserGroupType[];
  columnName: any[];
  totalRecords: number;
  status: string = '';
  name: string = '';

  constructor(private userGroupTypeService: UserGroupTypeService,
              private confirmationService: ConfirmationService) {
  }

  ngOnInit(): void {
    this.columnName = ColumnName.USER_GROUP_TYPE_TABLE;
    this.reloadData();
  }

  reloadData() {
    this.userGroupTypeService.getAllUserGroupTypes()
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = this.data$.length;
      }, error1 => console.log(error1));
  }

  handleChange($event: any, userGroupTypeId: string) {
    this.userGroupTypeService.updateUserGroupTypeStatus($event.checked ? 'ACTIVE': 'INACTIVE',
      userGroupTypeId).subscribe(()=>{
        this.reloadData();
    }, error1 => {console.log(error1)});
  }

  updateUserGroupType(userGroupTypeId: number) {

  }
}
