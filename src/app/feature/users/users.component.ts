import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {CommonVariableService} from '../../shared';
import {ColumnName} from '../../shared/constant/column-name';
import {Users} from './model/users.model';
import {UserService} from './service/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  data$: Observable<Users[]>;
  columnName: any[];
  page: number = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<number>;
  totalRecords: number;
  selectedItems: Users;
  userName: string = '';

  constructor(private userService: UserService,
              private commonVariableService: CommonVariableService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.USER_TABLE;
    this.reloadData(this.page, this.itemsPerPage);
  }

  reloadData(page: number, size: number) {
    this.userService.findAllUsers(this.userName, page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
        // this.totalRecords = this.data.length;
      }, error1 => console.log(error1));
  }

  searchUsers(user: string) {
    this.userName = user;
    this.reloadData(0, this.itemsPerPage);
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }


}
