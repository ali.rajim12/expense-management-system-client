import { Component, OnInit } from '@angular/core';
import {ConfirmationService} from 'primeng/api';
import {ColumnName} from '../../../shared/constant/column-name';
import {Department} from '../model/department.model';
import {DepartmentService} from '../service/department.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.sass']
})
export class DepartmentComponent implements OnInit {

  data$: Department[];
  columnName: any[];
  totalRecords: number;

  constructor(private departmentService: DepartmentService,
              private confirmationService: ConfirmationService) {
  }

  ngOnInit(): void {
    this.columnName = ColumnName.USER_DEPARTMENT_TABLE;
    this.reloadData();
  }

  reloadData() {
    this.departmentService.getAllDepartments()
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = this.data$.length;
      }, error1 => console.log(error1));
  }

  handleChange($event: any) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to change status ?',
      accept: () => {
        let isChecked = $event.checked;

        //Actual logic to perform a confirmation
      },
      reject: () => {

      }
    });
  }

  updateUserGroup(userGroupId: number) {


  }

}
