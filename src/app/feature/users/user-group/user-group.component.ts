import { Component, OnInit } from '@angular/core';
import {ColumnName} from '../../../shared/constant/column-name';
import {UserGroup} from '../model/user-group.model';
import {UserGroupService} from '../service/user-group.service';
import {ConfirmationService} from 'primeng/api';

@Component({
  selector: 'app-user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.sass']
})
export class UserGroupComponent implements OnInit {

  data$: UserGroup[];
  columnName: any[];
  totalRecords: number;
  status: string = '';
  val: boolean = false;

  constructor(private userGroupService: UserGroupService,
              private confirmationService: ConfirmationService) {
  }

  ngOnInit(): void {
    this.columnName = ColumnName.USER_GROUP_TABLE;
    this.reloadData();
  }

  reloadData() {
    this.userGroupService.getAllUserGroups()
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = this.data$.length;
      }, error1 => console.log(error1));
  }

  handleChange($event: any, userGroupId: string) {
    console.log($event);
    this.userGroupService.updateStatus(userGroupId, $event.checked ? 'ACTIVE': 'INACTIVE')
      .subscribe(()=> {
        this.reloadData();
      }, error1 => {console.log(error1)}
    )
  }

  updateUserGroup(userGroupId: number) {


  }
}
