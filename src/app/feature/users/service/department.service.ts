import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Injectable} from '@angular/core';
import {Department} from '../model/department.model';

@Injectable({providedIn: 'root'})
export class DepartmentService {

  constructor(private http: HttpClient) { }

  getAllDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(`${environment.apiUrl}/api/auth/department`)
  }
}
