import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserGroupType} from '../model/user-group-type.model';
import {environment} from '../../../../environments/environment';


@Injectable({providedIn: 'root'})
export class UserGroupTypeService {

  constructor(private http: HttpClient) { }

  getAllUserGroupTypes(): Observable<UserGroupType[]> {
    return this.http.get<UserGroupType[]>(`${environment.apiUrl}/api/auth/groupTypes`);
  }

  updateUserGroupTypeStatus(status: any, userGroupTypeId: string) {
    return this.http.post(`${environment.apiUrl}/api/auth/groupTypes/${userGroupTypeId}/${status}`, null);
  }
}
