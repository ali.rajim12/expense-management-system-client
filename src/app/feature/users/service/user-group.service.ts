import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserGroup} from '../model/user-group.model';
import {environment} from '../../../../environments/environment';

@Injectable({providedIn: 'root'})
export class UserGroupService {

  constructor(private http: HttpClient) {
  }

  getAllUserGroups(): Observable<UserGroup[]> {
    return this.http.get<UserGroup[]>(`${environment.apiUrl}/api/auth/userGroups`);
  }

  updateStatus(userGroupId: string, status: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/userGroups/${userGroupId}/${status}`, null);
  }
}
