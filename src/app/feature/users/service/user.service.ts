import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {User} from '../../../core/_models/user';
import {UserMini} from '../model/user-mini.model';
import {Observable} from 'rxjs';
import {Users} from '../model/users.model';
import {MyProfileModel} from "../model/my-profile.model";

@Injectable({providedIn: 'root'})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/users`);
  }

  getById(id: number) {
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
  }

  getAllApprovers(username: string) {
    let params = new HttpParams();
    params = params.append('q', username);
    return this.http.get<UserMini[]>(`${environment.apiUrl}/api/auth/users/approvers`,
      {params: params});
  }

  getAllSupporter(username: string) {
    let params = new HttpParams();
    params = params.append('q', username);
    return this.http.get<UserMini[]>(`${environment.apiUrl}/api/auth/users/supporters`,
      {params: params});
  }


  findAllUsers(userName: string, page: number, size: number): Observable<any> {
    let params = new HttpParams();
    params = params.append('username', userName);
    params = params.append('page', page+'');
    params = params.append('size', size+'');
    return this.http.get<any>(`${environment.apiUrl}/api/auth/users`,
      {params: params});
  }
  getAllMiniUsers(): Observable<UserMini[]> {
    return this.http.get<UserMini[]>(`${environment.apiUrl}/api/auth/users/mini`);
  }

  createNewUser(detail: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/users`, detail);
  }

  getUserDetailById(userId: string): Observable<Users> {
    return this.http.get<Users>(`${environment.apiUrl}/api/auth/users/${userId}`,);
  }

  updateUser(userId: string, detail: any): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/users/${userId}`, detail);
  }

  findAllReportUsers() {
    return this.http.get<UserMini[]>(`${environment.apiUrl}/api/auth/users/reports`);
  }

  findAllBudgetTranferUsers() {
    return this.http.get<UserMini[]>(`${environment.apiUrl}/api/auth/users/transfer_budget`);
  }

  findAllVerifierUsers(): Observable<UserMini[]> {
    return this.http.get<UserMini[]>(`${environment.apiUrl}/api/auth/users/can_verify`);
  }

  findAllMakePaymentUsers(): Observable<UserMini[]> {
    return this.http.get<UserMini[]>(`${environment.apiUrl}/api/auth/users/can_make_payment`);
  }

  getMyProfile(): Observable<MyProfileModel> {
    return this.http.get<MyProfileModel>(`${environment.apiUrl}/api/auth/users/my_profile`);
  }

  updateMyProfile(updateMyDetail): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/users/my_profile`, updateMyDetail)
  }
}
