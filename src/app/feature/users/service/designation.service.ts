import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Department} from '../model/department.model';
import {environment} from '../../../../environments/environment';
import {Designation} from '../model/designation.model';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class DesignationService {
  constructor(private http: HttpClient) { }

  getAllDesignations(): Observable<Designation[]> {
    return this.http.get<Designation[]>(`${environment.apiUrl}/api/auth/designations`)
  }
}
