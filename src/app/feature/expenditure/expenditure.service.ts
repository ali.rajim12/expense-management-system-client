import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {BillDetail} from './model/bill-detail.model';

@Injectable({
  providedIn: 'root'
})
export class ExpenditureService {

  constructor(private http: HttpClient) {
  }

  getExpenditureBillDetail(expenditureDetailId: number): Observable<BillDetail> {
    return this.http.get<BillDetail>(`${environment.apiUrl}/api/auth/expenditure/bill_detail/${expenditureDetailId}`);
  }


  updateTransactionDetail(request: any, expenditureTxnDetailId: number, expenditureDetailId: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/expenditure/${expenditureDetailId}/txn_detail/${expenditureTxnDetailId}/update`,
      request);
  }
}
