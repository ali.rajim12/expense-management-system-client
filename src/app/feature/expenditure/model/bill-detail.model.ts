import {Vendor} from '../../vendors/model/vendor.model';
import {VendorMiniResource} from '../../vendors/model/vendor-mini-resource.model';

export class BillDetail {
  vendorMiniResource: VendorMiniResource;
  billingDate: string;
  billNumber: string;
  billAmount: number;
  advanceAmount: number;
  registrationNumber: string;
  registrationType: string;
  tdsAmount: number;
  vendorAccountNumber: string;
  rebateAmount: number;
  penaltyAmount: number;
  panVatNumber: string;
}
