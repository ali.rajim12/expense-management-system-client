import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {ApprovedMemoModel} from "../model/approved-memo.model";
import {ApprovedExpenseDetailModel} from "../model/approved.expense.detail.model";
import {Branch, BranchService} from "../../branch";
import {AccountHead, AccountHeadService} from "../../account-head";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {Month} from "../../pre-approval/models/month.model";
import {ReportsService} from "../service/reports.service";
import {CommonVariableService} from "../../../shared";
import {UserService} from "../../users/service/user.service";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {ColumnName} from "../../../shared/constant/column-name";
import * as FileSaver from "file-saver";
import {AggregatedBudget} from "../model/aggregated-budget.model";

@Component({
  selector: 'app-aggregated-budget-expense',
  templateUrl: './aggregated-budget-expense.component.html',
  styleUrls: ['./aggregated-budget-expense.component.sass']
})
export class AggregatedBudgetExpenseComponent implements OnInit {

  data$: AggregatedBudget[];
  columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  allBranch: Branch[];
  accountHeads: AccountHead[];
  fiscalYears: FiscalYear[];
  months: Month[];
  selectedAccountHeadId: number = 0;
  fiscalYearId: number = 0;
  month: string = '';
  branchId: number = 0;

  constructor(private reportService: ReportsService,
              private commonVariableService: CommonVariableService,
              private branchService: BranchService,
              private userService: UserService,
              private accountHeadService: AccountHeadService,
              private preApprovalRequestService: PreApprovalService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.EMS_AGGREGATED_REPORTS;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllBranchForReports();
    this.getAccountHead();
    this.fetchAllFiscalYears();
    this.fetchAllMonths();
  }

  restValues() {
    this.selectedAccountHeadId = 0;
    this.fiscalYearId = 0;
    this.month = '';
    this.branchId = 0;
  }


  getAccountHead() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage)
  }

  reloadData(page: number, size: number) {
    this.reportService.fetchAggregatedReport(
      this.selectedAccountHeadId,
      this.branchId,
      this.fiscalYearId,
      this.month,
      page, size)
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = 1000000;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }
  loading = false;
  downloadExcel() {
    this.loading = true;
    this.reportService.DownloadAggregatedExcel(this.selectedAccountHeadId,
      this.branchId,
      this.fiscalYearId,
      this.month,)
      .subscribe((data) => {
        this.loading = false;
        FileSaver.saveAs(data, "aggregated_report.xlsx");
      }, error1 => {
        this.loading = false;
        console.log(error1);
      });
  }

  fetchAllBranchForReports() {
    this.branchService.fetchBranchListForReports().subscribe(branchList => {
      this.allBranch = branchList;
    })
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonths()
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }

}
