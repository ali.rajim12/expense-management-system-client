import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {ApprovedMemoModel} from "../model/approved-memo.model";
import {ApprovedExpenseDetailModel} from "../model/approved.expense.detail.model";
import {Branch, BranchService} from "../../branch";
import {AccountHead, AccountHeadService} from "../../account-head";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {Month} from "../../pre-approval/models/month.model";
import {ReportsService} from "../service/reports.service";
import {CommonVariableService} from "../../../shared";
import {UserService} from "../../users/service/user.service";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {ColumnName} from "../../../shared/constant/column-name";
import FileSaver from "file-saver";

@Component({
  selector: 'app-all-approved-transactions',
  templateUrl: './all-approved-transactions.component.html',
  styleUrls: ['./all-approved-transactions.component.sass']
})
export class AllApprovedTransactionsComponent implements OnInit {

  data$: Observable<ApprovedExpenseDetailModel[]>
  selectedItems: ApprovedExpenseDetailModel;
  columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  minFromDate: Date;
  minToDate: Date;
  fromDate = this.getFromDate();
  toDate = new Date();
  loading: boolean = false;
  allBranch: Branch[];
  accountHeads: AccountHead[];
  fiscalYears: FiscalYear[];
  months: Month[];
  selectedAccountHeadId: number = 0;
  fiscalYearId: number = 0;
  month: string = '';
  branchId: number = 0;
  showTransactionDetailForm = false;
  amount: number = 0;

  constructor(private reportService: ReportsService,
              private commonVariableService: CommonVariableService,
              private branchService: BranchService,
              private userService: UserService,
              private accountHeadService: AccountHeadService,
              private preApprovalRequestService: PreApprovalService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.ALL_EMS_APPROVED_MEMO_DETAIL;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllBranchForReports();
    this.getAccountHead();
    this.fetchAllFiscalYears();
    this.fetchAllMonths();
  }

  restValues() {
    this.selectedAccountHeadId = 0;
    this.fromDate = this.getFromDate();
    this.toDate = new Date();
    this.fiscalYearId = 0;
    this.month = '';
    this.branchId = 0;
    this.amount = 0;
  }


  getAccountHead() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  getFromDate() {
    var date = new Date();
    date.setDate(date.getDate() - 10);
    return date;
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage)
  }

  reloadData(page: number, size: number) {
    this.reportService.fetchAllApprovedTransactionsEntries(this.fromDate,
      this.toDate,
      this.selectedAccountHeadId,
      this.branchId,
      this.fiscalYearId,
      this.month,
      this.amount,
      page, size)
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = 1000000;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  downloadExcel() {
    this.loading = true;
    this.reportService.downloadAllApprovedTransactionsEntriesExcel(this.fromDate,
      this.toDate,
      this.selectedAccountHeadId,
      this.branchId,
      this.fiscalYearId,
      this.month,
      this.amount,)
      .subscribe((data) => {
        this.loading = false;
        FileSaver.saveAs(data, "approved_transaction_entries.xlsx");
      }, error1 => {
        this.loading = false;
        console.log(error1);
      });
  }

  fetchAllBranchForReports() {
    this.branchService.fetchBranchListForReports().subscribe(branchList => {
      this.allBranch = branchList;
    })
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonths()
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }
}
