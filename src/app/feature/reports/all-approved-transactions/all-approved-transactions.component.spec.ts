import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllApprovedTransactionsComponent } from './all-approved-transactions.component';

describe('AllApprovedTransactionsComponent', () => {
  let component: AllApprovedTransactionsComponent;
  let fixture: ComponentFixture<AllApprovedTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllApprovedTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllApprovedTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
