import { Component, OnInit } from '@angular/core';
import {BudgetDetailResource} from "../../budget/model/budget-detail-resource";
import {MemoDetail} from "../model/memo.detail.model";
import {BudgetExpenseDetail} from "../../budget/model/budget-expense-detail";
import {PreApprovalDetail} from "../../pre-approval";
import {ExpenditureRemarks} from "../../pre-approval/models/expenditure-remarks.model";
import {takeUntil} from "rxjs/operators";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {ActivatedRoute} from "@angular/router";
import {BranchBudgetService} from "../../budget";
import {BillDetail} from "../../expenditure/model/bill-detail.model";
import {PaymentJournals} from "../../payments/model/PaymentJournals";
import {MakePaymentService} from "../../payments/service/make.payment.service";

@Component({
  selector: 'app-memo-pdf',
  templateUrl: './memo-pdf.component.html',
  styleUrls: ['./memo-pdf.component.css']
})
export class MemoPdfComponent implements OnInit {
  preApprovalDetail: PreApprovalDetail;
  comments: ExpenditureRemarks[];
  memoDetail = new MemoDetail();
  detail: BudgetDetailResource;
  expenditureDetailId: number;
  budgetDetail: BudgetDetailResource[];
  billDetail: BillDetail;
  paymentJournals: PaymentJournals[];
  totalPayableAmount: number;
  preApprovalRemarks: ExpenditureRemarks[];
  BillPaymentRemarks: ExpenditureRemarks[];


  constructor(private preApprovalService: PreApprovalService,
              private branchBudgetService: BranchBudgetService,
              private makePaymentService: MakePaymentService,
              private activatedRoute: ActivatedRoute) {
    this.expenditureDetailId = this.activatedRoute.snapshot.params.expenditureDetailId;
  }
  ngOnInit(): void {
    this.getPreApprovalDetail(this.expenditureDetailId);
    this.getBranchBudgetDetail(this.expenditureDetailId);
    this.getBillDetail();
    this.fetchAllPaymentJournals();
    this.getPreApprovalRemarks('PRE_APPROVAL');
    this.getBillPaymentRemarks('BILL_PAYMENT');

  }

  getBillDetail() {
    this.preApprovalService.getExpenditureBillDetail(this.expenditureDetailId)
      .subscribe((data)=> {
        this.billDetail = data;
      }, error1 => {
        console.log(error1)
      })
  }

  getStatus(ytdBudgetBranch: BudgetExpenseDetail) {
    if(ytdBudgetBranch.totalExpense >= ytdBudgetBranch.totalBudget) {
      return 'NO';
    } else {
      return 'YES';
    }
  }

  getGrowth(detail: BudgetDetailResource) {
    if(detail.thisMonthBudget.totalExpense === 0) {
      return 0;
    }
    return ((detail.thisMonthBudget.totalExpense - detail.previousMonthBudget.totalExpense)/(detail.thisMonthBudget.totalExpense))*100;
  }

  getPreApprovalDetail(id: number) {
    this.preApprovalService.fetchPreApprovalDetail(id)
      .subscribe((response) => {
        this.preApprovalDetail = response;
        this.totalPayableAmount = 0;
        this.preApprovalDetail.expenditureTxnDetailResources.forEach((value => {
          this.totalPayableAmount = this.totalPayableAmount + (value.amount- value.tdsAmount);
        }))
      }, error1 => console.log(error1));
  }
  private getBranchBudgetDetail(expenditureDetailId: number) {
    this.branchBudgetService.fetchBranchBudgetDetailByExpDetailId(expenditureDetailId)
      .subscribe(data => {
        this.budgetDetail = data;
      })
  }

  fetchAllPaymentJournals() {
    this.makePaymentService.getPaymentJournals(this.expenditureDetailId)
      .subscribe((data) => {
        this.paymentJournals = data;
      }, error => {
        console.log(error)
      });
  }

  getPreApprovalRemarks(type: string) {
    this.preApprovalService.fetchComments(this.expenditureDetailId, type)
      .subscribe((response) => {
        this.preApprovalRemarks = response;
      }, error1 => console.log(error1));
  }


  getBillPaymentRemarks(type: string) {
    this.preApprovalService.fetchComments(this.expenditureDetailId, type)
      .subscribe((response) => {
        this.BillPaymentRemarks = response;
      }, error1 => console.log(error1));
  }
}
