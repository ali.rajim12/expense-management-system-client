import {RouterModule, Routes} from "@angular/router";
import {LoginOnlyLayoutComponent, MainLayoutComponent} from "../../layout";
import {VendorsListComponent} from "../vendors/vendors-list/vendors-list.component";
import {CategoriesComponent} from "../vendors/categories/categories.component";
import {AddVendorComponent} from "../vendors/vendors-list/add-vendor/add-vendor.component";
import {NgModule} from "@angular/core";
import {SearchedRequestDetailComponent} from "./searched-request-detail/searched-request-detail.component";
import {MemoPdfComponent} from "./memo-pdf/memo-pdf.component";
import {AllMemosComponent} from "./all-memos/all-memos.component";
import {TransactionEntriesComponent} from './transaction-entries/transaction-entries.component';
import {ApprovedExpenseComponent} from "./approved-expense/approved-expense.component";
import {AllApprovedTransactionsComponent} from "./all-approved-transactions/all-approved-transactions.component";
import {AggregatedBudgetExpenseComponent} from "./aggregated-budget-expense/aggregated-budget-expense.component";
import {MyReportsComponent} from "./my-reports/my-reports.component";

const routes: Routes = [
  {
    path: 'reports',
    component: MainLayoutComponent,
    children: [
      {
        path: 'all',
        component: AllMemosComponent
      },
      {
        path: 'journals',
        component: TransactionEntriesComponent
      },
      {
        path: 'approved-expenses',
        component: ApprovedExpenseComponent
      },
      {
        path: 'approved-entries',
        component: AllApprovedTransactionsComponent
      },
      {
        path: 'aggregated-budget',
        component: AggregatedBudgetExpenseComponent
      },
      {
        path: 'my-reports/:reportType',
        component: MyReportsComponent
      }
    ]
  },
  {
    path: 'doc',
    component: LoginOnlyLayoutComponent,
    children: [
      {
        path: 'pdf/:expenditureDetailId',
        component: MemoPdfComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRouting {
}
