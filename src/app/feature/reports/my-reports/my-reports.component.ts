import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {PreApprovalResponse} from "../../pre-approval";
import {Branch, BranchService} from "../../branch";
import {UserMini} from "../../users/model/user-mini.model";
import {BillPaymentService} from "../../bill-payment";
import {UserService} from "../../users/service/user.service";
import {CommonVariableService} from "../../../shared";
import {ColumnName} from "../../../shared/constant/column-name";
import {MyRequestModel} from "../model/my-request.model";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {ActivatedRoute} from "@angular/router";
import {ReportsService} from "../service/reports.service";

@Component({
  selector: 'app-my-reports',
  templateUrl: './my-reports.component.html',
  styleUrls: ['./my-reports.component.css']
})
export class MyReportsComponent implements OnInit {

  myRequestData$: MyRequestModel[];
  mySupportedApprovedData$: MyRequestModel[];

  columnName: any[];
  columnNameApproveApprove: any[];
  page = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<any>;
  refCode: string = '';
  totalRecords: number;

  minFromDate: Date;
  minToDate: Date;
  fromDate = this.getFromDate();
  toDate = new Date();

  memoCode: string = '';
  fiscalYears: FiscalYear[];
  selectedFiscalYear: number = 0;

  recentStatus = ['ALL',
    'REQUESTED',
    'SUPPORTED',
    'APPROVED',
    'RETURNED',
    'REJECTED',
    'PAYMENT_REQUESTED',
    'PAID'
  ];
  selectedRecentStatus = 'ALL';
  requestTypes = [
    {key: 'Pre Approval', value: 'PRE_APPROVAL'},
    {key: 'Bill Payment', value: 'BILL_PAYMENT'}
  ];
  requestType: string = 'BILL_PAYMENT';

  requestedForTypes = ['ALL', 'SUPPORT', 'APPROVE'];
  selectedRequestedFor: string = 'ALL';

  reportType: string;

  constructor(private billPaymentService: BillPaymentService,
              private commonVariableService: CommonVariableService,
              private preApprovalRequestService: PreApprovalService,
              private activatedRoute: ActivatedRoute,
              private reportService: ReportsService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.reportType = this.activatedRoute.snapshot.params.reportType;
    if (this.reportType === 'APPROVED') {
      this.selectedRequestedFor = 'APPROVE';
    } else if (this.reportType === 'SUPPORTED') {
      this.selectedRequestedFor = 'SUPPORT';
    } else {
      this.selectedRequestedFor = 'ALL'
    }
  }

  ngOnInit(): void {
    this.columnName = ColumnName.MY_REPORTS;
    this.columnNameApproveApprove = ColumnName.MY_REPORTS_APPROVE_SUPPORT;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllFiscalYears();
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }


  getFromDate() {
    var date = new Date();
    date.setDate(date.getDate() - 10);
    return date;
  }

  reloadData(page: number, size: number) {
    if (this.reportType === 'MY_REQUEST') {
      this.reportService.fetchAllMyReports(this.fromDate, this.toDate, this.selectedFiscalYear,
        this.selectedRecentStatus, this.memoCode, this.requestType, page, size).subscribe((response) => {
        this.myRequestData$ = response;
        this.totalRecords = 10000000;
      }, error1 => console.log(error1));
    } else {
      this.reportService.fetchAllSupportedApprovedReports(this.fromDate,
        this.toDate,
        this.selectedFiscalYear,
        this.selectedRecentStatus,
        this.memoCode,
        this.requestType,
        page,
        size, this.selectedRequestedFor).subscribe((response) => {
        this.mySupportedApprovedData$ = response;
        this.totalRecords = 10000000;
      }, error1 => console.log(error1));
    }
  }

  paginate(event) {
    const pageIndex = event.first / event.rows;
    this.page = pageIndex;
    this.reloadData(pageIndex, event.rows);
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage);
  }

  reset() {
    this.selectedRecentStatus = 'ALL';
    this.selectedFiscalYear = 0;
    this.selectedRecentStatus = 'BILL_PAYMENT';
  }
}
