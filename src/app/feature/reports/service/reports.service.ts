import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {PaymentJournalModel} from "../model/payment.journal.model";
import {AttachmentDetail} from "../../pre-approval/models/attachment-detail.model";
import {AggregatedBudget} from "../model/aggregated-budget.model";
import {MyRequestModel} from "../model/my-request.model";

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private http: HttpClient) {
  }

  getExpenditureDetail(refNumber: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/reports/search`, {
      params:
        {ref_number: refNumber}
    });
  }

  fetchAllReports(fromDate: Date,
                  toDate: Date,
                  type: string,
                  memoCode: string,
                  selectedRecentStatus: string,
                  userId,
                  branchId,
                  page: number,
                  size: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reports/all`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          type: type,
          memo_code: memoCode,
          recent_status: selectedRecentStatus,
          userId: userId + '',
          branchId: branchId + '',
          page: page + '',
          size: size + ''
        },
    });
  }

  fetchDataForExcel(fromDate: Date,
                    toDate: Date,
                    type: string,
                    memoCode: string) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })
    return this.http.get(`${environment.apiUrl}/api/auth/reports/all/excel`,
      {
        headers: headers, responseType: 'blob', params: {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          type: type,
          memo_code: memoCode
        }
      });
  }

  fetchAllTransactionEntries(selectedPaymentStatus: string,
                             fromDate: Date,
                             toDate: Date,
                             selectedAccountHeadId: number,
                             debitBranchCode: string,
                             creditBranchCode: string,
                             memoCode: string,
                             txnId: string,
                             toAccountNumber: string,
                             page: number,
                             size: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reports/finance/debit_credit`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          memo_code: memoCode + '',
          account_head_id: selectedAccountHeadId + '',
          payment_status: selectedPaymentStatus,
          txn_id: txnId + '',
          to_account_number: toAccountNumber + '',
          from_sol_id: debitBranchCode + '',
          to_sol_id: creditBranchCode + '',
          page: page + '',
          size: size + ''
        },
    });

  }

  fetchDebitCreditDataForExcel(selectedPaymentStatus: string,
                               fromDate: Date,
                               toDate: Date,
                               selectedAccountHeadId: number,
                               debitBranchCode: string,
                               creditBranchCode: string,
                               memoCode: string,
                               txnId: string,
                               toAccountNumber: string) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })
    return this.http.get(`${environment.apiUrl}/api/auth/reports/finance/debit_credit/excel`,
      {
        headers: headers, responseType: 'blob', params: {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          memo_code: memoCode + '',
          account_head_id: selectedAccountHeadId + '',
          payment_status: selectedPaymentStatus,
          txn_id: txnId + '',
          to_account_number: toAccountNumber + '',
          from_sol_id: debitBranchCode + '',
          to_sol_id: creditBranchCode + '',
        }
      });

  }

  fetchAllApprovedEntries(fromDate: Date,
                          toDate: Date,
                          selectedAccountHeadId: number,
                          branchId: number,
                          fiscalYearId: number,
                          month: string,
                          type: string,
                          page: number,
                          size: number) {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reports/approved_reports`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          account_head_id: selectedAccountHeadId + '',
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          month: month + '',
          request_type: type + '',
          page: page + '',
          size: size + ''
        },
    });

  }

  fetchAllApprovedEntriesDetail(fromDate: Date,
                                toDate: Date,
                                selectedAccountHeadId: any,
                                branchId: any,
                                fiscalYearId: any,
                                month: any) {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reports/approved_reports/detail`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          account_head_id: selectedAccountHeadId + '',
          branch_id: branchId + '',
          fiscal_year: fiscalYearId + '',
          month: month + ''
        },
    });
  }

  fetchAllApprovedTransactionsEntries(fromDate: Date,
                                      toDate: Date,
                                      selectedAccountHeadId: number,
                                      branchId: number,
                                      fiscalYearId: number,
                                      month: string,
                                      amount: number,
                                      page: number,
                                      size: number) {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reports/approved_reports/detail/all`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          account_head_id: selectedAccountHeadId + '',
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          month: month + '',
          amount: amount + '',
          page: page + '',
          size: size + ''
        },
    });
  }

  downloadAllApprovedTransactionsEntriesExcel(fromDate: Date,
                                              toDate: Date,
                                              selectedAccountHeadId: number,
                                              branchId: number,
                                              fiscalYearId: number,
                                              month: string,
                                              amount: number) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })
    return this.http.get(`${environment.apiUrl}/api/auth/reports/approved_reports/detail/all/excel`,
      {
        headers: headers, responseType: 'blob', params: {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          account_head_id: selectedAccountHeadId + '',
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          month: month + '',
          amount: amount + ''
        }
      });
  }

  fetchAllApprovedEntriesForExcel(fromDate: Date,
                                  toDate: Date,
                                  selectedAccountHeadId: number,
                                  branchId: number,
                                  fiscalYearId: number,
                                  month: string,
                                  type: string
  ) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })
    return this.http.get(`${environment.apiUrl}/api/auth/reports/approved_reports/excel`,
      {
        headers: headers, responseType: 'blob', params: {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          account_head_id: selectedAccountHeadId + '',
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          month: month + '',
          request_type: type + ''
        }
      });
  }

  rejectMemoDetail(formData: FormData, selectedExpenditureDetailId: number, remarks: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/expenditure/rejection/${selectedExpenditureDetailId}`,
      formData);
  }

  fetchAdminRejectionDetail(id: number): Observable<AttachmentDetail> {
    return this.http.get<AttachmentDetail>(`${environment.apiUrl}/api/auth/expenditure/rejection/${id}/reason`);
  }

  fetchAggregatedReport(selectedAccountHeadId: number,
                        branchId: number,
                        fiscalYearId: number,
                        month: string,
                        page: number,
                        size: number): Observable<AggregatedBudget[]> {
    return this.http.get<AggregatedBudget[]>(`${environment.apiUrl}/api/auth/reports/budget_ytd_report`, {
      params:
        {
          account_head_id: selectedAccountHeadId + '',
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          month: month + '',
          page: page + '',
          size: size + ''
        },
    });

  }

  DownloadAggregatedExcel(selectedAccountHeadId: number, branchId: number, fiscalYearId: number, month: string) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })
    return this.http.get(`${environment.apiUrl}/api/auth/reports/budget_ytd_report/excel`,
      {
        headers: headers, responseType: 'blob', params: {
          account_head_id: selectedAccountHeadId + '',
          branch_id: branchId + '',
          fiscal_year_id: fiscalYearId + '',
          month: month + ''
        }
      });
  }

  fetchAllMyReports(fromDate: Date,
                    toDate: Date,
                    selectedFiscalYear: number,
                    recentStatus: string,
                    memoCode: string,
                    requestType: string,
                    page: number,
                    size: number): Observable<MyRequestModel[]> {
    return this.http.get<MyRequestModel[]>(`${environment.apiUrl}/api/auth/my_reports/bil_payments`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          fiscal_year_id: selectedFiscalYear+'',
          recent_status: recentStatus,
          memo_code: memoCode,
          request_type: requestType,
          page: page + '',
          size: size + ''
        },
    });
  }

  fetchAllSupportedApprovedReports(fromDate: Date,
                                   toDate: Date,
                                   selectedFiscalYear: number,
                                   selectedRecentStatus: string,
                                   memoCode: string,
                                   requestType: string,
                                   page: number,
                                   size: number,
                                   requestedFor: string): Observable<MyRequestModel[]> {
    return this.http.get<MyRequestModel[]>(`${environment.apiUrl}/api/auth/my_reports/approved_supported`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          fiscal_year_id: selectedFiscalYear+'',
          recent_status: selectedRecentStatus,
          memo_code: memoCode,
          request_type: requestType,
          page: page + '',
          size: size + '',
          requestedFor: requestedFor
        },
    });
  }

  fetchAllVendorDetailByExpDetailId(expenditureId: number, page: number, size: number) {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reports/vendor_wise_report`, {
      params:
        {
          expenditure_detail_id: expenditureId+'',
          page: page + '',
          size: size + ''
        },
    });
  }

  fetchAllMemosAccountHeadWise(selectedBranchId: number,
                               selectedAccountHeadId: number,
                               page: number,
                               size: number) {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/reports/account_head_wise_report`, {
      params:
        {
          account_head_id: selectedAccountHeadId+'',
          branch_id: selectedBranchId+'',
          page: page + '',
          size: size + ''
        },
    });
  }
}
