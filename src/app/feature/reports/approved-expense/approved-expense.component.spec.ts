import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovedExpenseComponent } from './approved-expense.component';

describe('ApprovedExpenseComponent', () => {
  let component: ApprovedExpenseComponent;
  let fixture: ComponentFixture<ApprovedExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovedExpenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovedExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
