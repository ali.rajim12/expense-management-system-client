import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ExpenditureModel} from '../../pre-approval/models/expenditure.model';
import {Branch, BranchService} from '../../branch';
import {UserMini} from '../../users/model/user-mini.model';
import {ReportsService} from '../service/reports.service';
import {CommonVariableService} from '../../../shared';
import {UserService} from '../../users/service/user.service';
import {Router} from '@angular/router';
import {ColumnName} from '../../../shared/constant/column-name';
import * as FileSaver from 'file-saver';
import {AccountHead, AccountHeadService} from "../../account-head";
import {PaymentJournalModel} from "../model/payment.journal.model";

@Component({
  selector: 'app-transaction-entries',
  templateUrl: './transaction-entries.component.html',
  styleUrls: ['./transaction-entries.component.sass']
})
export class TransactionEntriesComponent implements OnInit {

  data$: Observable<PaymentJournalModel[]>;
  selectedItems: PaymentJournalModel;
  columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  minFromDate: Date;
  minToDate: Date;
  fromDate = this.getFromDate();
  toDate = new Date();
  paymentStatus = ['ALL', 'UNPAID', 'PAID'];
  selectedPaymentStatus = 'ALL';
  memoCode: string = '';
  loading: boolean = false;
  allBranch: Branch[];
  debitBranchCode: string = '';
  creditBranchCode: string = '';
  accountHeads: AccountHead[];
  selectedAccountHeadId: number = 0;
  toAccountNumber: string = '';
  txnId: string = '';

  constructor(private reportService: ReportsService,
              private commonVariableService: CommonVariableService,
              private branchService: BranchService,
              private userService: UserService,
              private accountHeadService: AccountHeadService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.CBS_PAYMENT_ENTRIES;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllBranchForReports();
    this.getAccountHead();
  }

  restValues() {
    this.memoCode = '';
    this.txnId = '';
    this.toAccountNumber = '';
    this.selectedAccountHeadId = 0;
    this.debitBranchCode = '';
    this.creditBranchCode = '';
    this.selectedPaymentStatus = 'ALL';
    this.fromDate = this.getFromDate();
    this.toDate = new Date();
  }


  getAccountHead() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  getFromDate() {
    var date = new Date();
    date.setDate(date.getDate() - 10);
    return date;
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage)
  }

  reloadData(page: number, size: number) {
    this.reportService.fetchAllTransactionEntries(this.selectedPaymentStatus,
      this.fromDate,
      this.toDate,
      this.selectedAccountHeadId,
      this.debitBranchCode,
      this.creditBranchCode,
      this.memoCode,
      this.txnId,
      this.toAccountNumber,
      page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  downloadExcel() {
    this.loading = true;
    this.reportService.fetchDebitCreditDataForExcel(this.selectedPaymentStatus,
      this.fromDate,
      this.toDate,
      this.selectedAccountHeadId,
      this.debitBranchCode,
      this.creditBranchCode,
      this.memoCode,
      this.txnId,
      this.toAccountNumber,)
      .subscribe((data) => {
        this.loading = false;
        FileSaver.saveAs(data, "transaction_detail.xlsx");
      }, error1 => {
        this.loading = false;
        console.log(error1);
      });
  }

  // changeType(event: string) {
  //   this.type = event;
  // }

  // changeStatus(event: string) {
  //   this.selectedRecentStatus = event;
  // }

  fetchAllBranchForReports() {
    this.branchService.fetchBranchListForReports().subscribe(branchList => {
      this.allBranch = branchList;
    })
  }

  viewDetail(detail: any) {

  }

  // fetchAllUsers() {
  //   this.userService.findAllReportUsers().subscribe(data => {
  //     this.allUsers = data;
  //   })
  // }

}
