export class PaymentJournalModel {
  txnDate: string;
  txnId: string;
  debitSolId: string;
  debitAccountNumber: string;
  debitHead: string;
  debitNarration: string;
  creditSolId: string;
  creditAccountNumber: string;
  creditNarration: string;
  amount: number;
  paymentMadeBy: number;
  memoCode: number;
  verifyBy: number;
  journalId: number;
  paymentStatus: string;
  expenditureDetailId: number;
}
