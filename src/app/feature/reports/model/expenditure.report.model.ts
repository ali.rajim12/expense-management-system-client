export class ExpenditureReport {
   id: number;
   memoCode: string;
   subject: string;
   description: string;
   memoDate: string;
   fiscalYear: string;
   billAmount: string;
   currentPreApprovalStatus: string;
   currentBillPaymentStatus: string;
   hasPreApproval: boolean;
}
