export class AggregatedBudget {
   fiscalYear: string;
   solId: string;
   branchName: string;
   accountHead: string;
   accountNumber: string;
   month: string;
   monthOrdinal: number;
   budget: number;
   expense: number;
   preApprovedAmount: number;
   ytdBudget: number;
   ytdExpense: number;
   ytdPreApproved: number;
}
