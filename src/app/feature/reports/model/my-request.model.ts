export class MyRequestModel {
  expenditureDetailId: number;
  billPaymentRequestDate: string;
  hasPreApproval: string;
  billPaymentMemoCode: string;
  subject: string;
  fiscalYear: string;
  amount: string;
  approvedDate: string;
  supportedDate: string;
  cbsPaidDate: string;
  recentStatus: string;
  requestedForType: string;
  recentMyStatus: string;
}
