export class ApprovedMemoModel {
  solId: string;
  branchName: string;
  totalExpense: string;
  description: string;
  accountNumber: string;
  month: string;
  debitBranchId: string;
  accountHeadId: string;
  fiscalYear: string;
}
