export class ApprovedExpenseDetailModel {
  solId: string;
  branchName: string;
  expenseAmount: string;
  description: string;
  accountNumber: string;
  month: string;
  billPaymentMemoCode: string;
  billApprovedDate: string;
  requestedDate: string;
  supportedDate: string;
  requestType: string;
  expenditureDetailId: number;
}
