export class MemoDetail {
  title: string;
  to: string;
  from: string;
  memoDate: string;
  subject: string;
  detail: string;
  requestedByOfficeName: string;
  requestedByOfficeCode: string;

}
