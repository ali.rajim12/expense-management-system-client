import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {PreApprovalResponse} from "../../pre-approval";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {CommonVariableService} from "../../../shared";
import {Router} from "@angular/router";
import {ColumnName} from "../../../shared/constant/column-name";
import {ReportsService} from "../service/reports.service";
import {ExpenditureModel} from "../../pre-approval/models/expenditure.model";
import * as FileSaver from 'file-saver';
import {Branch, BranchService} from "../../branch";
import {UserMini} from "../../users/model/user-mini.model";
import {UserService} from "../../users/service/user.service";
import {User} from "../../../core/_models/user";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomMessageService} from "../../../shared/service/custom-message.service";


@Component({
  selector: 'app-all-memos',
  templateUrl: './all-memos.component.html',
  styleUrls: ['./all-memos.component.sass']
})
export class AllMemosComponent implements OnInit {

  data$: Observable<ExpenditureModel[]>;
  selectedItems: ExpenditureModel;
  columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  minFromDate: Date;
  minToDate: Date;
  fromDate = this.getFromDate();
  toDate = new Date();
  recentStatus = ['ALL',
    'REQUESTED',
    'SUPPORTED',
    'APPROVED',
    'RETURNED',
    'REJECTED',
    'PAYMENT_REQUESTED',
    'PAID'
  ];
  selectedRecentStatus = 'ALL';
  requestTypes = [
    {key: 'Pre Approval', value: 'PRE_APPROVAL'},
    {key: 'Bill Payment', value: 'BILL_PAYMENT'}
  ];
  type: string = 'BILL_PAYMENT';
  memoCode: string = '';
  loading: boolean = false;
  allBranch: Branch[];
  allUsers: UserMini[];
  requestedUserId: number = 0;
  debitBranchId: number = 0;
  canReject: boolean;

  uploadMemoRejectionForm: FormGroup;
  showUploadFilePopUp: boolean = false;
  submitted: boolean = false;
  selectedExpenditureDetailId: number;
  loadingFileUpload: boolean = false;

  constructor(private reportService: ReportsService,
              private commonVariableService: CommonVariableService,
              private branchService: BranchService,
              private userService: UserService,
              private router: Router,
              private fb: FormBuilder,
              private customMessageService: CustomMessageService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.canReject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')))
      .getValue().canReject;

    this.uploadMemoRejectionForm = this.fb.group({
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
      remarks: [null, [Validators.required, Validators.maxLength(50)]],
    })

  }

  ngOnInit(): void {
    this.columnName = ColumnName.ALL_MEMO_REPORTS;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllBranchForReports();
    this.fetchAllUsers();
  }

  get f() {
    return this.uploadMemoRejectionForm.controls;
  }

  getFromDate() {
    var date = new Date();
    date.setDate(date.getDate() - 10);
    return date;
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage)
  }

  reloadData(page: number, size: number) {
    this.reportService.fetchAllReports(this.fromDate,
      this.toDate,
      this.type,
      this.memoCode,
      this.selectedRecentStatus,
      this.requestedUserId,
      this.debitBranchId,
      page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  downloadExcel() {
    this.loading = true;
    this.reportService.fetchDataForExcel(this.fromDate,
      this.toDate,
      this.type,
      this.memoCode)
      .subscribe((data) => {
        this.loading = false;
        FileSaver.saveAs(data, "detail.xlsx");
      }, error1 => {
        this.loading = false;
        console.log(error1);
      });
  }

  changeType(event: string) {
    this.type = event;
  }

  changeStatus(event: string) {
    this.selectedRecentStatus = event;
  }

  fetchAllBranchForReports() {
    this.branchService.fetchBranchListForReports().subscribe(branchList => {
      this.allBranch = branchList;
    })
  }

  fetchAllUsers() {
    this.userService.findAllReportUsers().subscribe(data => {
      this.allUsers = data;
    })
  }

  rejectMemo(expenditureDetailId: number) {
    this.showUploadFilePopUp = true;
    this.selectedExpenditureDetailId = expenditureDetailId;

  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadMemoRejectionForm.patchValue({
        fileSource: file
      });
    }
  }


  cancelSubmit() {
    this.showUploadFilePopUp = false;
    this.uploadMemoRejectionForm.reset();
    this.submitted = false;
    this.loadingFileUpload = false;
  }

  submitUploadForm() {
    this.submitted = true;
    if (this.uploadMemoRejectionForm.invalid) {
      return;
    }
    this.loadingFileUpload = true;
    const formData = new FormData();
    formData.append('files', this.uploadMemoRejectionForm.get('fileSource').value);
    formData.append('detail', this.uploadMemoRejectionForm.value.remarks);
    this.reportService.rejectMemoDetail(formData,
      this.selectedExpenditureDetailId,
      this.uploadMemoRejectionForm.value.remarks).subscribe((data) => {
      this.loading = false;
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        false);
      //  this.reloadData(this.page, this.itemsPerPage);
      this.loadingFileUpload = false;
      this.uploadMemoRejectionForm.reset();
      this.showUploadFilePopUp = false;
      this.reloadData(this.page, this.itemsPerPage);
    }, error => {
      this.customMessageService.sendSuccessErrorMessage(error,
        'error', false);
      this.loadingFileUpload = false;
      this.uploadMemoRejectionForm.reset();
      this.showUploadFilePopUp = false;
    })
  }

}
