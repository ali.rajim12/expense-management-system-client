import {UserMini} from "../../users/model/user-mini.model";

export class CheckerMaker {
  verifiedBy: UserMini;
  pushedBy: UserMini;
}
