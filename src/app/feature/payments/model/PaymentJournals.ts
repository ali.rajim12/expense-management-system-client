import {BranchMiniResource} from "../../branch";

export class PaymentJournals {
  paymentJournalId: number;
  transactionDate: string;
  fromAccountId: string;
  toAccountId: string;
  amount: number;
  status: string;
  attempts: number;
  remarks: string;
  fromBranch: BranchMiniResource;
  toBranch: BranchMiniResource;
  responseMessage: string
  cbsTxnId: string;
  creditRemarks: string;
  debitRemarks: string;
  entryType: string;
}
