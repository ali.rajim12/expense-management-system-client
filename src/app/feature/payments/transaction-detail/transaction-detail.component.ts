import {Component, Input, OnInit} from '@angular/core';
import {PaymentJournals} from "../model/PaymentJournals";
import {MakePaymentService} from "../service/make.payment.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ColumnName} from "../../../shared/constant/column-name";
import {VerifyPaymentDetail} from "../../bill-payment";
import {ConfirmationService} from "primeng/api";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {PreApprovalDetail} from "../../pre-approval";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SupportedApprovedDetail} from "../../pre-approval/models/supported-approved-detail";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {BehaviorSubject} from "rxjs";
import {User} from "../../../core/_models/user";

@Component({
  selector: 'app-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.css']
})
export class TransactionDetailComponent implements OnInit {
  paymentJournals: PaymentJournals[];
  @Input() expenditureDetailId: number;
  columnName: any[];
  display: boolean = false;
  detail: PaymentJournals;
  displayUpdateStatus: boolean = false;
  updateStatusForm: FormGroup;
  viewUpdateRemarksForm: FormGroup;
  status: string[] = ['PAID', 'AMBIGIOUS', 'UNPAID'];
  submitted: boolean = false;
  loading: boolean = false;
  paymentJournalId: number;
  selectedPaymentJournalId: number;
  displayViewUpdateNarrationForm: boolean = false;
  exportData: any[];

  constructor(private makePaymentService: MakePaymentService,
              private confirmationService: ConfirmationService,
              private customMessageService: CustomMessageService,
              private router: Router,
              private fb: FormBuilder) {
    this.updateStatusForm = fb.group({
      status: [null, Validators.required],
      comments: [null, Validators.required],
    });

    this.viewUpdateRemarksForm = fb.group({
      remarks: [null, Validators.required],
      debitRemarks: [null, Validators.required],
      creditRemarks: [null, Validators.required]
    });
  }

  get f() {
    return this.updateStatusForm.controls;
  }
  get ff() {
    return this.viewUpdateRemarksForm.controls;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.TRANSACTION_JOURNALS;
    this.fetchAllPaymentJournals();
  }

  fetchAllPaymentJournals() {
    this.makePaymentService.getPaymentJournals(this.expenditureDetailId)
      .subscribe((data) => {
        this.paymentJournals = data;
      }, error => {
        console.log(error)
      });
  }

  showDetail(detail: any) {
    this.display = true;
    this.detail = detail;
  }

  Reprocess(detail: any) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to Reprocess this transaction ?',
      accept: () => {
        this.makePaymentService.reProcessTransaction(detail.paymentJournalId)
          .subscribe((data) => {
            this.fetchAllPaymentJournals();
            this.customMessageService.sendSuccessErrorMessage(data.success_message,
              'success', true);
            if(data.success_message.includes("PAID")) {
              this.router.navigate(['/approved-bills/make-payment']);
            }
          }, error => {
            this.fetchAllPaymentJournals();
            this.customMessageService.sendSuccessErrorMessage(error, 'error',
              true);
          });
      }
    });
  }

  updateStatus(detail: any) {
    this.displayUpdateStatus = true;
    this.updateStatusForm.controls.status.patchValue(detail.status);
    this.paymentJournalId = detail.paymentJournalId;
  }

  onSubmit() {
    this.submitted = true;
    if (this.updateStatusForm.invalid) {
      return;
    }
    this.loading = true;
    this.makePaymentService.updatePaymentStatus(this.updateStatusForm.value,
      this.paymentJournalId)
      .subscribe(response => {
        this.fetchAllPaymentJournals();
        this.displayUpdateStatus = false;
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(response.success_message,
          'success', false);
        if(response.success_message.includes("PAID")) {
          this.router.navigate(['/approved-bills/make-payment']);
        }
      }, error => {
        this.fetchAllPaymentJournals();
        this.displayUpdateStatus = false;
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(error,
          'error', false);
      });
  }

  onUpdateRemarksSubmit() {
    this.submitted = true;
    if (this.viewUpdateRemarksForm.invalid) {
      return;
    }
    this.loading = true;
    this.makePaymentService.updatePaymentJournalRemarks(this.viewUpdateRemarksForm.value,
      this.selectedPaymentJournalId)
      .subscribe(response => {
        this.fetchAllPaymentJournals();
        this.displayViewUpdateNarrationForm = false;
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(response.success_message,
          'success', false);
      }, error => {
        this.fetchAllPaymentJournals();
        this.displayViewUpdateNarrationForm = false;
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(error,
          'error', false);
      });
  }

  updateRemarks(list: any) {
    this.selectedPaymentJournalId = list.paymentJournalId;
    this.displayViewUpdateNarrationForm = true;
    this.viewUpdateRemarksForm.controls.remarks.patchValue(list.remarks);
    this.viewUpdateRemarksForm.controls.debitRemarks.patchValue(list.debitRemarks);
    this.viewUpdateRemarksForm.controls.creditRemarks.patchValue(list.creditRemarks);
  }


  exportExcel() {
    import("xlsx").then(xlsx => {
      this.exportData = this.paymentJournals.map(col => ({
        'Txn Date': col.transactionDate,
        'Txn Id':col.cbsTxnId,
        'From A/C':col.fromAccountId,
        'To A/C':col.toAccountId,
        'Amount':col.amount,
        'From Branch':col.fromBranch.name +' ( '+col.fromBranch.code+' )',
        'To Branch': col.toBranch.name + '( '+col.toBranch.code+' )',
        'Attempts': col.attempts,
        'Status': col.status,
        'Debit Narration': col.debitRemarks,
        'Credit Narration': col.creditRemarks,
        'Remarks': col.remarks,
        'Response Message': col.responseMessage,
        'Type': col.entryType
      }))
      const worksheet = xlsx.utils.json_to_sheet(this.exportData);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "memo_entries");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().toDateString() + EXCEL_EXTENSION);
    });
  }

}
