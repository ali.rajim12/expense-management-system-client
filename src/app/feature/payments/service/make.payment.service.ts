import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {RequestReport} from "../../dashborad/model/request-report.model";
import {environment} from "../../../../environments/environment";
import {PaymentJournals} from "../model/PaymentJournals";
import {CheckerMaker} from "../model/checker.maker.model";

@Injectable({providedIn: 'root'})
export class MakePaymentService {

  constructor(private http: HttpClient) {
  }

  getPaymentJournals(expenditureDetailId: number): Observable<PaymentJournals[]> {
    return this.http.get<PaymentJournals[]>(`${environment.apiUrl}/api/auth/payments/journals/${expenditureDetailId}`);
  }

  reProcessTransaction(paymentJournalId: number): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/payments/journals/${paymentJournalId}/reprocess`, null);
  }
  updatePaymentStatus(detail: any,
                      paymentJournalId: number): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/payments/journals/${paymentJournalId}`, detail);
  }

  makePaymentToCBS( expenditureDetailId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/payments/make_payment/${expenditureDetailId}`, null);
  }

  fetchCheckerMakerDetail(expenditureDetailId: number): Observable<CheckerMaker> {
    return this.http.get<CheckerMaker>(`${environment.apiUrl}/api/auth/payments/journals/${expenditureDetailId}/checker_maker`)
  }

  updatePaymentJournalRemarks(value: any, paymentJournalId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/payments/journals/${paymentJournalId}/remarks`,
      value);
  }
}
