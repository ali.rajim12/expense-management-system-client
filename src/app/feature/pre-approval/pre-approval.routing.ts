import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequestListComponent} from './request-list/request-list.component';
import {MainLayoutComponent} from '../../layout';
import {RequestDetailComponent} from './request-detail/request-detail.component';
import {CreateRequestComponent} from './create-request/create-request.component';
import {SublayoutComponent} from './sublayout/sublayout.component';
import {SupportListComponent} from './support-list/support-list.component';
import {ApproveListComponent} from './approve-list/approve-list.component';
import {ReturnedRequestComponent} from "./returned-request/returned-request.component";
import {RejectedRequestComponent} from "./rejected-request/rejected-request.component";

const routes: Routes = [
  {
    path: 'pre-approval',
    component: MainLayoutComponent,
    // canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: SublayoutComponent,
        children: [
          {
            path: 'my-request',
            component: RequestListComponent
          },
          {
            path: 'support',
            component: SupportListComponent
          },
          {
            path: 'approve',
            component: ApproveListComponent
          },
          {
            path: 'returned',
            component: ReturnedRequestComponent
          },
          {
            path: 'rejected',
            component: RejectedRequestComponent
          },
          {
            path: 'create/:requestId',
            component: CreateRequestComponent
          },
          {
            path: ':id/:type',
            component: RequestDetailComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreApprovalRouting {

}
