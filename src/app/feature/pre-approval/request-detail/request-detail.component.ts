import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BehaviorSubject, from, Observable, of, Subject, zip} from 'rxjs';
import {PreApprovalService} from '../service/pre-approval.service';
import {PreApprovalDetail} from '../models/Pre-approval-detail.model';
import {groupBy, map, mergeMap, reduce, takeUntil, toArray} from 'rxjs/operators';
import {ExpenditureRemarks} from '../models/expenditure-remarks.model';
import {SupportedApprovedDetail} from '../models/supported-approved-detail';
import {User} from '../../../core/_models/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ReportsService} from "../../reports/service/reports.service";
import {AttachmentDetail} from "../models/attachment-detail.model";
import * as FileSaver from "file-saver";
import {ExpenditureModel} from "../models/expenditure.model";
import {CommonVariableService} from "../../../shared";
import {ColumnName} from "../../../shared/constant/column-name";

@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.css']
})
export class RequestDetailComponent implements OnInit, OnDestroy {
  preApprovalDetail: PreApprovalDetail;
  comments: ExpenditureRemarks[];
  destroy$: Subject<boolean> = new Subject<boolean>();
  id: number;
  type: string;
  display: boolean = false;
  performedAction: string;
  columnName: any[];
  preApprovalDetailForm: FormGroup;
  links: string[] = [];

  canViewRejectReason: boolean;
  showRejectionReasonForm: boolean = false;
  adminRejectionDetail: AttachmentDetail;
  showVendorReport: boolean = false;
  data$: Observable<ExpenditureModel[]>;
  totalRecords: number;

  // columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;

  constructor(private route: ActivatedRoute,
              private preApprovalService: PreApprovalService,
              private fb: FormBuilder,
              private reportService: ReportsService,
              private commonVariableService: CommonVariableService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.id = this.route.snapshot.params.id;
    this.type = this.route.snapshot.params.type;
    this.preApprovalDetailForm = fb.group({
      justification: null,
      detail: null
    })
    this.canViewRejectReason = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')))
      .getValue().canReject;

  }

  ngOnInit(): void {
    this.columnName = ColumnName.ALL_MEMO_REPORTS;
    this.getPreApprovalDetail(this.id);
    // this.fetchCommentDetail(this.id);
    this.setForm();
  }

  getPreApprovalDetail(id: number) {
    this.preApprovalService.fetchPreApprovalDetail(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((response) => {
        this.preApprovalDetail = response;
        if(this.preApprovalDetail.link !== null) {
          this.preApprovalDetail.link.split(" ").forEach((value => {
            this.links.push(value);
          }))
        }
      }, error1 => console.log(error1));
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

  private setForm() {
    if(this.preApprovalDetail) {
      this.preApprovalDetailForm.controls.justification.patchValue(this.preApprovalDetail.justification);
      this.preApprovalDetailForm.controls.detail.patchValue(this.preApprovalDetail.memoDetail);
    }
  }

  goToLink(link: string) {
    window.open('/doc/pdf/'+this.id, "_blank");
  }
  goToRefLink(link: string) {
    window.open(link, "_blank");
  }

  viewRejectionReason() {
    this.showRejectionReasonForm = true;
    this.reportService.fetchAdminRejectionDetail(this.id)
      .subscribe((response) => {
      this.adminRejectionDetail = response;
    }, error1 => console.log(error1));
  }

  download(attachmentId: number, fileName: string) {
    this.preApprovalService.downloadFile(attachmentId).subscribe(
      (data)=> {
        FileSaver.saveAs(data, fileName);
      }, error => {
        console.log(error)
      }
    )
  }

  viewVendorDetail() {
    this.showVendorReport = true;
    this.reloadVendorTable(this.page, this.itemsPerPage);
  }

  reloadVendorTable(page: number, size: number) {
    this.reportService.fetchAllVendorDetailByExpDetailId(this.id, page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1))
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadVendorTable(pageIndex, event.rows);
  }

  goTo() {
   // [routerLink]="['/pre-approval', list.id, 'BILL_PAYMENT']"
    window.open('/pre-approval/' + this.id+'/BILL_PAYMENT', "_blank");
  }


}
