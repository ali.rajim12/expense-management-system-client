import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-sublayout',
  templateUrl: './sublayout.component.html',
  styleUrls: ['./sublayout.component.sass']
})
export class SublayoutComponent implements OnInit {

  items: MenuItem[];
  constructor() { }

  ngOnInit(): void {
    this.items = [
      {
        label: 'My Requests',
        routerLink: 'dashboard'
      },
      {
        label: 'Approval Request',
        routerLink: '',
      },
      {
        label: 'Support Request',
        routerLink: ''
      }
    ];
  }

}
