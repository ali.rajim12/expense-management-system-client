import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {PreApprovalDetail} from '../models/Pre-approval-detail.model';
import {catchError, retry} from 'rxjs/operators';
import {ExpenditureRemarks} from '../models/expenditure-remarks.model';
import {AttachmentDetail} from '../models/attachment-detail.model';
import {BillDetail} from '../../expenditure/model/bill-detail.model';
import {AccountDetailModel} from "../../shared/dynamic-dialog/model/account-detail.model";

@Injectable({
  providedIn: 'root'
})
export class PreApprovalService {

  constructor(private http: HttpClient) {
  }

  // TODO make common code
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  fetchAllRequests(refCode: string, page: number, size: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/pre_approval`, {
      params:
        {ref_code: refCode, page: page + '', size: size + '', sort: 'created'}
    });
  }

  getAllMonths(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/months`);
  }

  getAllMonthsForAddEdit(isEdit: boolean): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/months/all`,
      { params: { isEdit: isEdit+'',
        }
      });
  }
  getAllFiscalYears(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/fiscal_years`);
  }

  getAllExpenditureTypes(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/expenditure_type`);
  }

  submitRequest(formData: FormData): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/expenditure/pre_approval`, formData);
  }

  fetchPreApprovalDetail(id: number): Observable<PreApprovalDetail> {
    return this.http.get<PreApprovalDetail>(`${environment.apiUrl}/api/auth/expenditure/${id}`);
  }
  fetchComments(id: number, type: string): Observable<ExpenditureRemarks[]> {
    return this.http.get<ExpenditureRemarks[]>(`${environment.apiUrl}/api/auth/expenditure/remarks/${id}/${type}`);
  }
  fetchAllSupportOrApproveRequests(requestedFor: string, page: number, size: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/pre_approval/${requestedFor}`, {
      params:
        { ref_code: '', page: page + '', size: size + ''}
    });
  }
  addCommentAndUpdateStatus( submitRequest: any, id: number): Observable<any> {
    if(submitRequest.status === 'FORWARDED') {
      return this.http.post(`${environment.apiUrl}/api/auth/expenditure/${id}/forward`, submitRequest);
    } else {
      return this.http.post(`${environment.apiUrl}/api/auth/expenditure/${id}/remarks`, submitRequest);
    }
  }

  fetchExpenditureAttachments(expenditureDetailId: number, type: string): Observable<AttachmentDetail[]> {
    return this.http.get<AttachmentDetail[]>(`${environment.apiUrl}/api/auth/expenditure/${expenditureDetailId}/documents/${type}`);
  }

  getExpenditureBillDetail(id: number): Observable<BillDetail> {
    return this.http.get<BillDetail>(`${environment.apiUrl}/api/auth/expenditure/bill_detail/${id}`);
  }

  getAccountDetail(accountNumber: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/expenditure/${accountNumber}/validate`);
  }

  downloadFile(attachmentId: number)  {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/download/${attachmentId}`,
      { responseType: 'blob' });
  }

  fetchAllReturnOrRejectedRequests(actionType: string, requestType: string,  page: number, size: number) : Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/requests/${actionType}`, {
      params:
        { ref_code: '',
          request_type: requestType,
          page: page + '',
          size: size + ''
        }
    });
  }

  updateExpenditureDetailRequest(formData: FormData, preApprovalDetailId: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/expenditure/pre_approval/${preApprovalDetailId}/update`, formData);
  }

  generatePaymentDetail(fiscalYearId: any, remarks: any) : Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/rent/payment/${fiscalYearId}/${remarks}`,
      null);
  }
}
