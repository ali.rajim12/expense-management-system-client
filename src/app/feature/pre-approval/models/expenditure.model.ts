export class ExpenditureModel {
  id: number;
  memoCode: string;
  subject: string;
  description: string;
  memoDate: string;
  fiscalYear: string;
  billAmount: number;
  currentStatus: number;
}
