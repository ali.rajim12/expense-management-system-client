import {UserMini} from '../../users/model/user-mini.model';

export class SupportedApprovedDetail {
  supportApproveId: number;
  userMiniResources: UserMini;
  status: string;
  requestedFor: string;
  type: string;
}
