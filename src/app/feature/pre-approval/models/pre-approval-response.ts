
export class PreApprovalResponse {
  memo_code: string;
  subject: string;
  description: string;
  memo_date: any;
  fiscal_year: string;
  bill_amount: number

}
