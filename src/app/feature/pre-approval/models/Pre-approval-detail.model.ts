import {BranchMiniResource} from '../../branch';
import {ExpenseDetail} from './Expense-detail.model';
import {AttachmentDetail} from './attachment-detail.model';
import {SupportedApprovedDetail} from './supported-approved-detail';

export  class PreApprovalDetail {
  memoDatePreApproval: string;
  memoDateBillPayment: string;
  preApprovalRequestedByBranch: BranchMiniResource;
  billPaymentRequestedByBranch: BranchMiniResource;
  referenceNumberPreApproval: string;
  referenceNumberBillPayment: string;
  memoSubject: string;
  proposedAmount: number;
  memoDetail: string;
  justification: string;
  expenditureTxnDetailResources: ExpenseDetail[];
  approvalDetail: SupportedApprovedDetail[];
  expenditureType: string;
  month: string;
  fiscalYear: string
  advance: number;
  paymentMode: string;
  link: string;
  vendorId: number;
  recentBillPaymentStatus: string;
  recentPreApprovalStatus: string;
  billNumber: string;
  penalty: number;
  rebateAmount: number;
}
