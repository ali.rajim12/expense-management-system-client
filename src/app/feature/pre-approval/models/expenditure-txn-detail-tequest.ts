export class ExpenditureTxnDetailTequest {
  accountHeadId: number;
  amount: number;
  expenseDescription: string;
  debitBranchId: number;
  creditBranchId: number;
  creditAccount: string;
  expenditureTxnDetailId: number;
  tdsAmount: number;

  constructor(accountHeadId: number,
              amount: number,
              expenseDescription: string,
              debitBranchId: number,
              creditBranchId: number,
              creditAccount: string,
              expenditureTxnDetailId: number,
              tdsAmount: number
  ) {
    this.accountHeadId = accountHeadId;
    this.amount = amount;
    this.expenseDescription = expenseDescription;
    this.debitBranchId = debitBranchId;
    this.creditBranchId = creditBranchId;
    this.creditAccount = creditAccount;
    this.expenditureTxnDetailId = expenditureTxnDetailId;
    this.tdsAmount = tdsAmount;
  }
}
