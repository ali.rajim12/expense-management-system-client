export class AttachmentDetail {
  fileName: string;
  type: string;
  expenditureAttachmentId: number;
  attachmentType: string;
  remarks: string;
}
