import {BranchMiniResource} from '../../branch';

export class ExpenseDetail {
  creditBranch: BranchMiniResource;
  creditBranchName: string;
  debitBranch: BranchMiniResource;
  debitBranchName: string;
  accountHead: string;
  accountHeadNumber: string;
  accountHeadId: number;
  amount: number;
  creditAccount: string;
  expenseDescription: string;
  expenditureTxnDetailId:  number;
  tdsAmount:  number;
}
