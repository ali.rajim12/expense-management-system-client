import {UserMini} from '../../users/model/user-mini.model';

export class ExpenditureRemarks {
  remarks: string;
  user: UserMini;
  commentedDate: string;
  commentedBy: string;
  status: string;
  expenditureType: string;
}
