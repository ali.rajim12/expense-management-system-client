import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {Month} from '../models/month.model';
import {FiscalYear} from '../models/fiscal-year.model';
import {ExpenditureType} from '../models/expenditure-type.model';
import {PreApprovalService} from '../service/pre-approval.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../users/service/user.service';
import {UserMini} from '../../users/model/user-mini.model';
import {AccountHead, AccountHeadService} from '../../account-head';
import {ExpenditureTxnDetailTequest} from '../models/expenditure-txn-detail-tequest';
import {CustomMessageService} from '../../../shared/service/custom-message.service';
import {CustomMessages} from '../../../shared/constant/custom-messages';
import {Branch, BranchService} from '../../branch';
import {MessageService} from 'primeng/api';
import {ExpenseDetail} from '../models/Expense-detail.model';
import {takeUntil} from "rxjs/operators";
import {PreApprovalDetail} from "..";
import {User} from "../../../core/_models/user";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';



@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.css']
})
export class CreateRequestComponent implements OnInit {

  public Editor = ClassicEditor;

  expenditureRequestForm: FormGroup;
  minDate: Date;
  months: Month[];
  fiscalYears: FiscalYear[];
  expenditureTypes: ExpenditureType[];
  requestDate = new Date();
  showPreApprovaldDetail = false;
  files: string [] = [];
  approver: UserMini[];
  supporter: UserMini[];
  accountHeads: AccountHead[];
  loading = false;
  submitted = false;
  allBranch: Branch[];
  showAmountMismatchMessage: boolean = false;
  showFileValidationMessage: boolean = false;
  preApprovalDetailId: string;
  preApprovalDetail: PreApprovalDetail;
  currentUserBranchId: number;


  constructor(private fb: FormBuilder,
              private preApprovalRequestService: PreApprovalService,
              private userService: UserService,
              private accountHeadService: AccountHeadService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private customMessageService: CustomMessageService,
              private branchService: BranchService,
              private preApprovalService: PreApprovalService,
              private messageService: MessageService) {
    this.preApprovalDetailId = this.activatedRoute.snapshot.params.requestId;
    this.expenditureRequestForm = this.fb.group({
      requestDate: [this.requestDate],
      expenditureType: [null, Validators.required],
      fiscalYearId: [null, Validators.required],
      month: [null, Validators.required],
      expenseDetail: [null, [Validators.required, Validators.minLength(10)]],
      subject: [null, [Validators.required, Validators.minLength(10),
        Validators.maxLength(200)]],
      amount: [null, Validators.required],
      advanceAmount: [null],
      justification: [null, Validators.required],
      file: [null],
      link: [null],
      proposedExpenseRequest: this.fb.array([this.newProposedExpenses()]),
      approvers: [null, Validators.required],
      supporters: [null],
    });
    this.currentUserBranchId = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user'))).getValue().branchId;
  }

  get f() {
    return this.expenditureRequestForm.controls;
  }

  proposedExpenseRequest(): FormArray {
    return this.expenditureRequestForm.get('proposedExpenseRequest') as FormArray;
  }

  newProposedExpenses(): FormGroup {
    return this.fb.group({
      accountHeadId: ['', Validators.required],
      amount: ['', Validators.required],
      expenseDescription: ['', [Validators.required, Validators.minLength(1),
        Validators.maxLength(45)]],
      debitBranchId: ['', Validators.required],
      expenditureTxnDetailId: 0
    });
  }

  addProposedExpenses() {
    this.proposedExpenseRequest().push(this.newProposedExpenses());
  }

  removeProposedExpenses(index: number) {
    this.proposedExpenseRequest().removeAt(index);
  }

  confirmCreateRequest() {
    this.submitted = true;
    if (this.expenditureRequestForm.value.amount !== undefined) {
      if (Number.parseFloat(this.expenditureRequestForm.value.amount) < this.getTotalAmount()) {
        this.showAmountMismatchMessage = true;
        return;
      }
    }
    if (this.expenditureRequestForm.invalid) {
      return;
    }
    this.showPreApprovaldDetail = true;
    this.showAmountMismatchMessage = false;
  }

  getTotalAmount() {
    let total: number = 0;
    this.expenditureRequestForm.value.proposedExpenseRequest.forEach(function(value) {
      total = total + value.amount;
    });
    return total;
  }
  validateFile(file: any) {
    var ext = file.name.substring(file.name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() === 'js' ||
      ext.toLowerCase() === 'php' ||
      ext.toLowerCase() === 'py' ) {
      return false;
    }
    else {
      return true;
    }
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    if (this.expenditureRequestForm.invalid) {
      return;
    }
    this.loading = true;
    const formData = new FormData();

    for (var i = 0; i < this.files.length; i++) {
      if(!this.validateFile(this.files[i])) {
        this.loading = false;
        this.showPreApprovaldDetail = false;
        this.showFileValidationMessage = true;
        return;
      }
      formData.append('files', this.files[i]);
    }
    let detail = {
      requestDate: this.expenditureRequestForm.value.requestDate,
      expenditureType: this.expenditureRequestForm.value.expenditureType.type,
      subject: this.expenditureRequestForm.value.subject,
      month: this.expenditureRequestForm.value.month,
      amount: this.expenditureRequestForm.value.amount,
      advanceAmount: this.expenditureRequestForm.value.advanceAmount,
      fiscalYearId: this.expenditureRequestForm.value.fiscalYearId.id,
      expenseDetail: this.expenditureRequestForm.value.expenseDetail,
      justification: this.expenditureRequestForm.value.justification,
      link: this.expenditureRequestForm.value.link,
      expenditureTxnDetailRequest: this.getExpenditureTxnDetailRequest(),
      supportedByUserIds: this.getSupportedBYUserIds(),
      approvedByUserIds: this.getApprovedByUsersIds()
    };
    formData.append('detail', JSON.stringify(detail));
    if(this.preApprovalDetailId != 'new') {
      this.preApprovalRequestService.updateExpenditureDetailRequest(formData,
        this.preApprovalDetailId)
        .subscribe(
          data => {
            this.showPreApprovaldDetail = false;
            this.router.navigate(['/pre-approval/my-request']);
            this.customMessageService.sendSuccessErrorMessage(CustomMessages.PRE_APPROVAL_REQUEST_CREATED_SUCCESSFULLY, 'success', true);
          },
          error => {
            this.showPreApprovaldDetail = false;
            this.loading = false;
            this.customMessageService.sendSuccessErrorMessage(error, 'error', true);
            // this.messageService.add({key: 'tc', severity: 'error', summary: 'error', detail: error});
          });
    } else {
      this.preApprovalRequestService.submitRequest(formData)
        .subscribe(
          data => {
            this.showPreApprovaldDetail = false;
            this.router.navigate(['/pre-approval/my-request']);
            this.customMessageService.sendSuccessErrorMessage(CustomMessages.PRE_APPROVAL_REQUEST_CREATED_SUCCESSFULLY, 'success', true);
          },
          error => {
            this.showPreApprovaldDetail = false;
            this.loading = false;
            this.customMessageService.sendSuccessErrorMessage(error, 'error', true);
            // this.messageService.add({key: 'tc', severity: 'error', summary: 'error', detail: error});
          });
    }
  }

  getExpenditureTxnDetailRequest() {
    let expentitureTxnDetail: ExpenditureTxnDetailTequest[] = [];
    this.expenditureRequestForm.value.proposedExpenseRequest.forEach(function(value) {
      expentitureTxnDetail.push(new ExpenditureTxnDetailTequest(
        value.accountHeadId,
        value.amount,
        value.expenseDescription,
        value.debitBranchId,
        0,
        '',
        value.expenditureTxnDetailId,
        0
      ));
    });
    return expentitureTxnDetail;
  }

  ngOnInit(): void {
    this.fetchAllExpenditureTypes();
    this.fetchAllMonths();
    this.fetchAllFiscalYears();
    this.fetchAllAccountHeads();
    this.fetchAllBranch();
    if(this.preApprovalDetailId != 'new') {
      this.fetchPreApprovalDetail()
    }
  }


  fetchAllBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.allBranch = data;
    }, error1 => {
      console.log(error1);
    });
  }

  fetchAllAccountHeads() {
    this.accountHeadService.fetchAllAccountHead().subscribe((response) => {
      this.accountHeads = response;
    }, error1 => console.log(error1));
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonthsForAddEdit(this.preApprovalDetailId === 'new' ? false: true)
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  fetchAllExpenditureTypes() {
    this.preApprovalRequestService.getAllExpenditureTypes()
      .subscribe((response) => {
        this.expenditureTypes = response;
      }, error1 => console.log(error1));
  }

  onFileChange(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.files.push(event.target.files[i]);
    }

  }

  searchApprover(event: any) {
    this.userService.getAllApprovers(event.query).subscribe(data => {
      this.approver = data;
    });
  }

  searchSupporter(event: any) {
    this.userService.getAllSupporter(event.query).subscribe(data => {
      this.supporter = data;
    });
  }

  private getSupportedBYUserIds() {
    let userIds: number[] = [];
    if(this.expenditureRequestForm.value.supporters !== null) {
      this.expenditureRequestForm.value.supporters.forEach(function(value) {
        userIds.push(value.userId);
      });
      return userIds;
    }
    return userIds;
  }

  private getApprovedByUsersIds() {
    let userIds: number[] = [];
    this.expenditureRequestForm.value.approvers.forEach(function(value) {
      userIds.push(value.userId);
    });
    return userIds;
  }

  cancel() {
    this.router.navigate(['/pre-approval/my-request']);
  }

  renderProposedExpenses(event: any) {
    this.expenditureRequestForm.controls.proposedExpenseRequest.patchValue([{
      accountHeadId: '',
      amount: event,
      expenseDescription: '',
      debitBranchId: ''
    }]);
  }

  fetchPreApprovalDetail() {
    this.preApprovalService.fetchPreApprovalDetail(Number.parseInt(this.preApprovalDetailId))
      .subscribe((response) => {
        this.expenditureRequestForm.controls.expenditureType.patchValue(
          this.getExpenditureType(response.expenditureType));
        this.expenditureRequestForm.controls.fiscalYearId.patchValue(
          this.getFiscalYear(response.fiscalYear)
        );
        this.expenditureRequestForm.controls.month.patchValue(
          this.getMonth(response.month)
        );
        this.expenditureRequestForm.controls.expenseDetail.patchValue(response.memoDetail);
        this.expenditureRequestForm.controls.subject.patchValue(response.memoSubject);
        this.expenditureRequestForm.controls.amount.patchValue(response.proposedAmount);
        this.expenditureRequestForm.controls.advanceAmount.patchValue(response.advance);
        this.expenditureRequestForm.controls.justification.patchValue(response.justification);
        this.expenditureRequestForm.controls.link.patchValue(response.link);
        const txnDetail = this.expenditureRequestForm.get('proposedExpenseRequest') as FormArray;
        while (txnDetail.length) {
          txnDetail.removeAt(0);
        }
        response.expenditureTxnDetailResources.forEach(txn => txnDetail.push(this.fb.group({
          accountHeadId: this.getAccountHead(txn.accountHeadId),
          amount: txn.amount,
          expenseDescription: txn.expenseDescription,
          debitBranchId: this.getDebitBranch(txn.debitBranch?.branchId),
          expenditureTxnDetailId: this.preApprovalDetailId === 'new' ? 0 : txn.expenditureTxnDetailId,
        })));
      }, error1 => console.log(error1));
  }

  private getMonth(month: string) {
    if (this.months === undefined) {
      this.fetchAllMonths();
    }
    let requiredValue: String = null;
    this.months.forEach(value => {
      if (value.months === month) {
        requiredValue = value.months;
      }
    });
    return requiredValue;
  }

  private getExpenditureType(expenditureType: string) {
    let requiredValue: ExpenditureType = null;
    this.expenditureTypes.forEach((value) => {
      if (value.type === expenditureType) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }

  private getFiscalYear(fiscalYear: string) {
    let requiredValue: FiscalYear = null;
    this.fiscalYears.forEach(value => {
      if (value.year === fiscalYear) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }



  private getDebitBranch(branchId: number) {
    let requiredBranch: number = null;
    this.allBranch.forEach(value => {
      if (value.branchId === branchId) {
        requiredBranch = value.branchId;
      }
    });
    return requiredBranch;
  }
  private getAccountHead(accountHeadId: number) {
    if (this.accountHeads === undefined) {
      this.fetchAllAccountHeads();
    }
    let requiredAccountHead: number = null;
    this.accountHeads.forEach(value => {
      if (value.accountHeadId === accountHeadId) {
        requiredAccountHead = value.accountHeadId;
      }
    });
    return requiredAccountHead;
  }

}
