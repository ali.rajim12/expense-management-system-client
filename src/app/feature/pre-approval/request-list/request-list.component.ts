import {Component, OnInit} from '@angular/core';
import {PreApprovalService} from '../service/pre-approval.service';
import {Observable} from 'rxjs';
import {PreApprovalResponse} from '../models/pre-approval-response';
import {CommonVariableService} from '../../../shared/service/common-variable.service';
import {ColumnName} from '../../../shared/constant/column-name';
import {Router} from '@angular/router';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.sass']
})
export class RequestListComponent implements OnInit {
  data$: Observable<PreApprovalResponse[]>;
  errors$: Observable<string>;
  selectedItems: PreApprovalResponse;
  columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  refCode: string = '';
  totalRecords: number;

  constructor(private preApprovalService: PreApprovalService,
              private commonVariableService: CommonVariableService,
              private router: Router) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.PRE_APPROVAL_REQUST_COLUMN_LIST;
    this.reloadData(this.page, this.itemsPerPage);
  }

  reloadData(page: number, size: number) {
    this.preApprovalService.fetchAllRequests(this.refCode, page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  goToCreateNew() {
    this.router.navigate(['/pre-approval/create', 'new']);
  }

}
