import {RouterModule, Routes} from "@angular/router";
import {MainLayoutComponent} from "../../layout";
import {UsersComponent} from "../users";
import {AddNewUserComponent} from "../users/add-new-user/add-new-user.component";
import {UserGroupComponent} from "../users/user-group/user-group.component";
import {GroupTypeComponent} from "../users/group-type/group-type.component";
import {DepartmentComponent} from "../users/department/department.component";
import {DesignationsComponent} from "../users/designations/designations.component";
import {NgModule} from "@angular/core";
import {ExpenseTypeComponent} from "./expense-type/expense-type.component";
import {PaymentListComponent} from "./payment-list/payment-list.component";
import {CreatePaymentComponent} from "./create-payment/create-payment.component";

const routes: Routes = [
  {
    path: 'periodic',
    component: MainLayoutComponent,
    children: [
      {
        path: 'expense-types',
        component: ExpenseTypeComponent,
      },
      {
        path: 'payments/:periodicPaymentTypeId',
        component: PaymentListComponent
      },
      {
        path: 'create-payment/:periodicPaymentTypeId',
        component: CreatePaymentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeriodicPaymentRouting {

}
