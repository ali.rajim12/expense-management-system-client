import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, Subscribable} from "rxjs";
import {Department} from "../../users/model/department.model";
import {environment} from "../../../../environments/environment";
import {PeriodicExpense} from "../model/periodic.expense.model";

@Injectable({providedIn: 'root'})
export class PeriodicPaymentService {

  constructor(private http: HttpClient) { }

  getAllPeriodicPaymentTypes(periodicPaymentType: string): Observable<PeriodicExpense[]> {
    return this.http.get<PeriodicExpense[]>(`${environment.apiUrl}/api/auth/periodic_expenses`, {
      params: {
        expense_type_name: periodicPaymentType
      }
    })
  }

  updatePeriodicPaymentTypeStatus(status: string, periodicPaymentTypeId: string): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/periodic_expenses/${periodicPaymentTypeId}/${status}`,  null)
  }

  addNewPeriodicPaymentType(value: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/periodic_expenses`, value)
  }

  updatePeriodicPaymentType(value: any, selectedPeriodicPaymentTypeId: number) {
    return this.http.put(`${environment.apiUrl}/api/auth/periodic_expenses/${selectedPeriodicPaymentTypeId}`,
      value)
  }

  fetchAllPeriodicPayments(periodicPaymentTypeId: number,
                           amount: number,
                           debitBranchId: number,
                           creditBranchId: number,
                           accountNumber: string,
                           page: number, size: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/periodic_payment/${periodicPaymentTypeId}/all`, {
      params: {
        amount: amount+'',
        debit_branch_id:debitBranchId+'',
        credit_branch_id: creditBranchId+'',
        account_number: accountNumber,
        page: page+'',
        size: size+''
      }
    })
  }

  updatePeriodicPaymentStatus(string: string, periodicPaymentId: string): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/auth/periodic_payment/${periodicPaymentId}/${string}`,
      null)
  }

  clearPeriodicPaymentDetail(periodicPaymentTypeId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/periodic_payment/${periodicPaymentTypeId}/clear`,
      null)
  }

  downloadSampleDoc() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })

    return this.http.get(`${environment.apiUrl}/api/auth/periodic_payment/download/sample`,
      { headers:headers, responseType: 'blob'})
  }


  uploadExcelFile(formData: FormData, periodicPaymentTypeId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/periodic_payment/${periodicPaymentTypeId}/file`,
      formData);
  }

  addEditPeriodicPayment(value: any, eventType: string, periodicPaymentTypeId: number,
                         periodicPaymentId: number): Observable<any> {
    if(eventType === 'Add') {
      return this.http.post(`${environment.apiUrl}/api/auth/periodic_payment/${periodicPaymentTypeId}`,
        value);
    } else {
      return this.http.put(`${environment.apiUrl}/api/auth/periodic_payment/${periodicPaymentId}`,
        value);
    }
  }

  submitPeriodicPaymentRequest(formData: FormData, periodicPaymentTypeId: number) : Observable<any>{
    return this.http.post(`${environment.apiUrl}/api/auth/periodic_payment/${periodicPaymentTypeId}/create_payment_request`,
      formData);
  }

  downloadSampleData() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })

    return this.http.get(`${environment.apiUrl}/api/auth/periodic_payment/download/sample_data`,
      { headers:headers, responseType: 'blob'})
  }
}
