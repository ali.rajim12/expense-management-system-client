export class PeriodicExpense {
  expenseName: string;
  period: string;
  status: string;
  periodicExpenseId: number;
  createdDate: any;
}
