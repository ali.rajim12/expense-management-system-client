import {BranchMiniResource} from "../../branch";

export class PeriodPaymentDetail {
  debitBranch: BranchMiniResource;
  creditBranch: BranchMiniResource;
  debitAccountHead: string;
  debitAccountHeadId: number;
  creditAccountNumber: string;
  amount: number;
  status: string;
  periodicPaymentId: number;
  tdsAmount: number;
  narration: string;
}
