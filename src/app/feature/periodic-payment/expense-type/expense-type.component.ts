import {Component, OnInit} from '@angular/core';
import {ColumnName} from "../../../shared/constant/column-name";
import {PeriodicExpense} from "../model/periodic.expense.model";
import {PeriodicPaymentService} from "../service/periodic.payment.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AccountHead, AccountHeadService} from "../../account-head";
import {CustomMessages} from "../../../shared/constant/custom-messages";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {from} from "rxjs";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-expense-type',
  templateUrl: './expense-type.component.html',
  styleUrls: ['./expense-type.component.sass']
})
export class ExpenseTypeComponent implements OnInit {
  periodicPaymentType: string = '';
  columnName: any[];
  data$: PeriodicExpense[];
  totalRecords: number;
  display: boolean = false;
  addEditType: string;
  addEditPeriodicPaymentTypeForm: FormGroup;
  accountHeads: AccountHead[];
  submitted: boolean = false;
  periods: string[] = ['DAILY', 'MONTHLY', 'QUARTERLY', 'HALF_YEARLY', 'YEARLY'];
  loading: boolean = false;
  selectedAccountHeadId: number;
  selectedPeriodicPaymentTypeId: number;

  constructor(private periodicPaymentService: PeriodicPaymentService,
              private accountHeadService: AccountHeadService,
              private fb: FormBuilder,
              private customMessageService: CustomMessageService) {
    this.addEditPeriodicPaymentTypeForm = fb.group({
      // accountHeadId: [null, Validators.required],
      name: [null, Validators.required],
      period: [null, Validators.required]
    });
  }

  get f() {
    return this.addEditPeriodicPaymentTypeForm.controls;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.PERIODIC_PAYMENT_EXPENSE_TYPE;
    this.reloadData()
    // this.fetchAllAccountHeads();
  }

  // fetchAllAccountHeads() {
  //   this.accountHeadService.fetchAllAccountHead().subscribe((response) => {
  //     this.accountHeads = response;
  //   }, error1 => console.log(error1));
  // }

  reloadData() {
    this.periodicPaymentService.getAllPeriodicPaymentTypes(this.periodicPaymentType)
      .subscribe((data) => {
        this.data$ = data;
        this.totalRecords = data.length;
      }, error1 => {
        console.log(error1);
      });
  }

  searchPeriodicPayment(periodicPaymentType: string) {
    this.periodicPaymentType = periodicPaymentType;
    this.reloadData();
  }

  handleChange($event: any, periodicPaymentTypeId: string) {
    this.periodicPaymentService.updatePeriodicPaymentTypeStatus($event.checked ? 'ACTIVE' : 'INACTIVE',
      periodicPaymentTypeId).subscribe(() => {
      this.reloadData();
    }, error1 => {
      console.log(error1);
    });
  }

  addEditPeriodicPaymentType(type: string, detail: any) {
    this.addEditType = type;
    this.display = true;
    if (type === 'Edit') {
      this.selectedPeriodicPaymentTypeId = detail.periodicExpenseId;
      // if (this.accountHeads === null || this.accountHeads.length === 0) {
      //   this.fetchAllAccountHeads();
      // }
      // this.setAccountHeadId(detail.accountHeadId);
      // this.addEditPeriodicPaymentTypeForm.controls.accountHeadId.patchValue(
      //   this.selectedAccountHeadId
      // );
      this.addEditPeriodicPaymentTypeForm.controls.name.patchValue(detail.expenseName);
      this.addEditPeriodicPaymentTypeForm.controls.period.patchValue(detail.period);
    }
  }

  // setAccountHeadId(accountHeadId: number) {
  //   from(this.accountHeads)
  //     .pipe(filter(value => value.accountHeadId === accountHeadId))
  //     .subscribe(value => {
  //       this.selectedAccountHeadId = value.accountHeadId;
  //     })
  // }

  submit() {
    this.submitted = true;
    if (this.addEditPeriodicPaymentTypeForm.invalid) {
      return;
    }
    this.loading = true;
    if (this.addEditType === 'Add') {
      this.periodicPaymentService.addNewPeriodicPaymentType(this.addEditPeriodicPaymentTypeForm.value)
        .subscribe((data) => {
          this.display = false;
          this.loading = false;
          this.reloadData();
          this.customMessageService.sendSuccessErrorMessage(CustomMessages.PERIODIC_PAYMENT_TYPE_CREATED_SUCCESSFULLY,
            'success', false);
        }, error => {
          this.addEditPeriodicPaymentTypeForm.reset();
          this.display = false;
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(error, 'error', false);
        })
    } else {
      this.periodicPaymentService.updatePeriodicPaymentType(this.addEditPeriodicPaymentTypeForm.value,
        this.selectedPeriodicPaymentTypeId)
        .subscribe((data) => {
          this.display = false;
          this.loading = false;
          this.reloadData();
          this.customMessageService.sendSuccessErrorMessage(CustomMessages.PERIODIC_PAYMENT_TYPE_UPDATED_SUCCESSFULLY,
            'success', false);
        }, error => {
          this.addEditPeriodicPaymentTypeForm.reset();
          this.display = false;
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(error, 'error', false);
        })
    }

  }

  cancel() {
    this.addEditPeriodicPaymentTypeForm.reset();
    this.display = false;
  }

}
