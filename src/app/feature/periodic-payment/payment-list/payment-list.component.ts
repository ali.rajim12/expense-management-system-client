import {Component, OnInit} from '@angular/core';
import {ColumnName} from "../../../shared/constant/column-name";
import {PeriodPaymentDetail} from "../model/period.payment.detail.model";
import {CommonVariableService} from "../../../shared";
import {PeriodicPaymentService} from "../service/periodic.payment.service";
import {ActivatedRoute} from "@angular/router";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import * as FileSaver from 'file-saver';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomMessages} from "../../../shared/constant/custom-messages";
import {Branch, BranchService} from "../../branch";
import {AccountHead, AccountHeadService} from "../../account-head";
import {from} from "rxjs";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.sass']
})
export class PaymentListComponent implements OnInit {

  columnName: any[];
  data$: PeriodPaymentDetail[];
  periodicPaymentTypeId: number;
  amount: number = 0;
  debitBranchId: number = 0;
  creditBranchId: number = 0;
  accountNumber: string = '';
  page: number = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<number>;
  totalRecords: number;
  selectedItems: PeriodPaymentDetail;
  eventType: string;
  files: string[] = [];
  file: any;
  submitted: boolean = false;
  loading: boolean = false;
  display: boolean = false;
  myForm: FormGroup;
  addEditPeriodicPaymentForm: FormGroup;
  allBranch: Branch[];
  accountHeads: AccountHead[];
  selectedPaymentId: number;

  selectedDebitBranchId: number;
  selectedCreditBranchId: number;
  clearDataLoading: boolean = false;


  constructor(private commonVariableService: CommonVariableService,
              private periodicPaymentService: PeriodicPaymentService,
              private activatedRoute: ActivatedRoute,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder,
              private branchService: BranchService,
              private accountHeadService: AccountHeadService) {
    this.myForm = this.fb.group({
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])
    });
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.periodicPaymentTypeId = this.activatedRoute.snapshot.params.periodicPaymentTypeId;
    this.addEditPeriodicPaymentForm = this.fb.group({
      debitBranchId: [null, Validators.required],
      debitAccountHeadId: [null, Validators.required],
      creditBranchId: [null, Validators.required],
      creditAccountNumber: [null, Validators.required],
      amount: [null, Validators.required],
      tdsAmount: [null, Validators.required],
      narration: [null, Validators.required]
    })
  }

  get f() {
    return this.myForm.controls;
  }

  get ff() {
    return this.addEditPeriodicPaymentForm.controls;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.myForm.patchValue({
        fileSource: file
      });
    }
  }

  ngOnInit(): void {
    this.columnName = ColumnName.PERIODIC_PAYMENT_DETAILS;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllBranch();
    this.fetchAllAccountHeads();
  }

  reloadData(page: number, size: number) {
    this.periodicPaymentService.fetchAllPeriodicPayments(this.periodicPaymentTypeId,
      this.amount, this.debitBranchId, this.creditBranchId, this.accountNumber, page, size)
      .subscribe((data) => {
        this.data$ = data.results;
        this.totalRecords = data.totalResult;
      }, error => {
      })
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  handleChange($event: any, periodicPaymentId: string) {
    this.periodicPaymentService.updatePeriodicPaymentStatus(
      $event.checked ? 'ACTIVE' : 'DELETED',
      periodicPaymentId).subscribe(() => {
      this.reloadData(this.page, this.itemsPerPage);
    }, error1 => {
      console.log(error1);
    });
  }


  clearDetail() {
    this.clearDataLoading = true;
    this.periodicPaymentService.clearPeriodicPaymentDetail(
      this.periodicPaymentTypeId).subscribe((data) => {
      this.clearDataLoading = false;
      this.reloadData(0, this.itemsPerPage);
      this.customMessageService.sendSuccessErrorMessage(data.success_message,
        'success', false);
    }, error => {
      this.clearDataLoading = false;
      this.customMessageService.sendSuccessErrorMessage(error, 'error',
        false);

    });
  }

  submit() {
    this.submitted = true;
    if (this.myForm.invalid) {
      return;
    }
    this.loading = true;
    const formData = new FormData();
    formData.append('files', this.myForm.get('fileSource').value);
    this.periodicPaymentService.uploadExcelFile(formData, this.periodicPaymentTypeId)
      .subscribe((data) => {
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(
          data.success_message,
          'success',
          false);
        this.reloadData(this.page, this.itemsPerPage);
      }, error => {
        this.customMessageService.sendSuccessErrorMessage(error,
          'error', false);
        this.loading = false;
      })
  }

  downloadSampleDocument() {
    this.periodicPaymentService.downloadSampleDoc()
      .subscribe((data) => {
        FileSaver.saveAs(data, "sample.xlsx");
      }, error1 => {
        console.log(error1);
      });
  }

  downloadSampleData() {
    this.periodicPaymentService.downloadSampleData()
      .subscribe((data) => {
        FileSaver.saveAs(data, "account_head_list.xlsx");
      }, error1 => {
        console.log(error1);
      });
  }

  addEditNewPeriodicPayment(eventType: string, detail: any) {
    this.display = true;
    this.eventType = eventType;
    if (eventType === 'Edit') {
      this.selectedPaymentId = detail.periodicPaymentId;
      this.addEditPeriodicPaymentForm.controls.amount.patchValue(detail.amount);
      this.addEditPeriodicPaymentForm.controls.creditAccountNumber.patchValue(detail.creditAccountNumber);
      this.setDebitBranchId(detail.debitBranch.branchId);
      this.addEditPeriodicPaymentForm.controls.debitBranchId.patchValue(
        this.selectedDebitBranchId
      );
      this.setCreditBranchId(detail.creditBranch.branchId);
      this.addEditPeriodicPaymentForm.controls.creditBranchId.patchValue(
        this.selectedCreditBranchId
      );
      this.addEditPeriodicPaymentForm.controls.creditAccountNumber.patchValue(detail.creditAccountNumber);
      this.addEditPeriodicPaymentForm.controls.debitAccountHeadId.patchValue(detail.debitAccountHeadId);
      this.addEditPeriodicPaymentForm.controls.amount.patchValue(detail.amount);
      this.addEditPeriodicPaymentForm.controls.tdsAmount.patchValue(detail.tdsAmount);
      this.addEditPeriodicPaymentForm.controls.narration.patchValue(detail.narration);
    }
  }
  setCreditBranchId(creditBranchId: number) {
    if (this.allBranch === null || this.allBranch.length === 0) {
      this.fetchAllBranch();
    }
    from(this.allBranch)
      .pipe(filter(value => value.branchId === creditBranchId))
      .subscribe(value => {
        this.selectedCreditBranchId = value.branchId;
      })
  }

  setDebitBranchId(debitBranchId: number) {
    if (this.allBranch === null || this.allBranch.length === 0) {
      this.fetchAllBranch();
    }
    from(this.allBranch)
      .pipe(filter(value => value.branchId === debitBranchId))
      .subscribe(value => {
        this.selectedDebitBranchId = value.branchId;
      })
  }

  fetchAllBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.allBranch = data;
    }, error1 => {
      console.log(error1);
    });
  }


  fetchAllAccountHeads() {
    this.accountHeadService.fetchAllAccountHead()
      .subscribe((response) => {
      this.accountHeads = response;
    }, error1 => console.log(error1));
  }

  cancel() {
    this.display = false;
    this.addEditPeriodicPaymentForm.reset();
    this.submitted = false;
    // this.addEditPeriodicPaymentForm.reset();
  }

  submitNewPeriodicPayment() {
    this.submitted = true;
    if (this.addEditPeriodicPaymentForm.invalid) {
      return;
    }
    this.loading = true;
    this.periodicPaymentService.addEditPeriodicPayment(this.addEditPeriodicPaymentForm.value,
      this.eventType, this.periodicPaymentTypeId, this.selectedPaymentId).subscribe(data => {
      this.loading = false;
      this.display = false;
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        false);
      this.reloadData(0, this.itemsPerPage);
    }, error => {
      this.customMessageService.sendSuccessErrorMessage(error,
        'error', false);
      this.loading = false;
      this.display = false;
    })
  }

  resetForm() {
    this.submitted = false;
    this.myForm.reset();
  }
}
