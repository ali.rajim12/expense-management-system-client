import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ExpenditureType} from "../../pre-approval/models/expenditure-type.model";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {Month} from "../../pre-approval/models/month.model";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {Vendor} from "../../vendors/model/vendor.model";
import {VendorsService} from "../../vendors/service/vendors.service";
import {UserMini} from "../../users/model/user-mini.model";
import {UserService} from "../../users/service/user.service";
import {CustomMessages} from "../../../shared/constant/custom-messages";
import {PeriodicPaymentService} from "../service/periodic.payment.service";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-create-payment',
  templateUrl: './create-payment.component.html',
  styleUrls: ['./create-payment.component.css']
})
export class CreatePaymentComponent implements OnInit {
  public Editor = ClassicEditor;

  createPaymentRequestForm: FormGroup;
  periodicPaymentTypeId: number;
  requestedDate = new Date();
  billingDate = new Date();
  files: string[] = [];
  submitted = false;
  loading: boolean = false;
  minDate: Date;
  expenditureTypes: ExpenditureType[];
  months: Month[];
  fiscalYears: FiscalYear[];
  vendors: Vendor[];
  registrationTypes: string[] = ['PAN', 'VAT'];
  showConfirmationPage: boolean = false;
  approver: UserMini[];
  supporter: UserMini[];

  constructor(private fb: FormBuilder,
              private activeRoute: ActivatedRoute,
              private preApprovalRequestService: PreApprovalService,
              private vendorService: VendorsService,
              private userService: UserService,
              private periodicPaymentService: PeriodicPaymentService,
              private router: Router,
              private customMessageService: CustomMessageService) {
    this.periodicPaymentTypeId = this.activeRoute.snapshot.params.periodicPaymentTypeId;
    this.createPaymentRequestForm = this.fb.group({
      requestedDate: [this.requestedDate, Validators.required],
      expenditureType: [null, Validators.required],
      month: [null, Validators.required],
      fiscalYear: [null, Validators.required],
      subject: [null, Validators.required],
      expenseDetail: [null, Validators.required],
      link: [null],
      preApprovalRequestId: null,
      vendor: [null, Validators.required],
      billingDate: [this.billingDate, Validators.required],
      billAmount: [null, Validators.required],
      registrationType: [null, Validators.required],
      tdsAmount: 0,
      billNumber: [null, Validators.required],
      advance: 0,
      registrationNumber: null,
      vendorAccountNumber: null,
      file: [null],
      supporters: null,
      approvers: [null, Validators.required],
      penaltyAmount: 0,
      rebateAmount: 0
    });
  }
  get f() {
    return this.createPaymentRequestForm.controls;
  }

  ngOnInit(): void {
    this.fetchAllExpenditureTypes();
    this.fetchAllMonths();
    this.fetchAllFiscalYears();
    this.fetchAllActiveVendors();
  }

  fetchAllActiveVendors() {
    this.vendorService.fetchAllActiveVendors()
      .subscribe(data => {
        this.vendors = data;
      });
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonthsForAddEdit(false)
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  fetchAllExpenditureTypes() {
    this.preApprovalRequestService.getAllExpenditureTypes()
      .subscribe((response) => {
        this.expenditureTypes = response;
      }, error1 => console.log(error1));
  }

  searchSupporter(event: any) {
    this.userService.getAllSupporter(event.query).subscribe(data => {
      this.supporter = data;
    });
  }

  searchApprover(event: any) {
    this.userService.getAllApprovers(event.query).subscribe(data => {
      this.approver = data;
    });
  }
  confirmCreateRequest() {
    this.submitted = true;
    if(this.createPaymentRequestForm.invalid) {
      return;
    }
    this.showConfirmationPage = true;
  }

  onFileChange(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.files.push(event.target.files[i]);
    }
  }

  onSubmit() {
    this.loading = true;
    const formData = new FormData();
    for (var i = 0; i < this.files.length; i++) {
      formData.append('files', this.files[i]);
    }
    let detail = {
      preApprovalRequestId: this.activeRoute.snapshot.params.searchId === 'new' ? 0 : this.activeRoute.snapshot.params.searchId,
      budgetDetailRequest: {
        requestedDate: this.createPaymentRequestForm.value.requestedDate,
        expenditureType: this.createPaymentRequestForm.value.expenditureType.type,
        month: this.createPaymentRequestForm.value.month.months,
        fiscalYearId: this.createPaymentRequestForm.value.fiscalYear.id,
        subject: this.createPaymentRequestForm.value.subject,
        expenseDetail: this.createPaymentRequestForm.value.expenseDetail,
        link: this.createPaymentRequestForm.value.link
      },
      billDetailRequest: {
        vendorId: this.createPaymentRequestForm.value.vendor.vendorId,
        billingDate: this.createPaymentRequestForm.value.billingDate,
        billNumber: this.createPaymentRequestForm.value.billNumber,
        billAmount: this.createPaymentRequestForm.value.billAmount,
        advance: this.createPaymentRequestForm.value.advance,
        registrationType: this.createPaymentRequestForm.value.vendor.registrationType,
        registrationNumber: this.createPaymentRequestForm.value.vendor.registrationNumber,
        tdsAmount: this.createPaymentRequestForm.value.tdsAmount,
        penaltyAmount: this.createPaymentRequestForm.value.vendor.penaltyAmount,
        rebateAmount: this.createPaymentRequestForm.value.vendor.rebateAmount
      },
      expenditureTxnDetailRequest: null,
      supportedByUserIds: this.getSupportedBYUserIds(),
      approvedByUserIds: this.getApprovedByUsersIds()
    };
    formData.append('detail', JSON.stringify(detail));

    this.periodicPaymentService.submitPeriodicPaymentRequest(formData,
      this.periodicPaymentTypeId)
      .subscribe(
        data => {
          this.showConfirmationPage = false;
          this.router.navigate(['/bill-payment/all']);
          this.customMessageService.sendSuccessErrorMessage(
            data.success_message,
            'success', true);
        },
        error => {
          this.showConfirmationPage = false;
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(error, 'error',
            true);
        });

  }

  private getSupportedBYUserIds() {
    let userIds: number[] = [];
    if(this.createPaymentRequestForm.value.supporters !== null) {
      this.createPaymentRequestForm.value.supporters.forEach(function (value) {
        userIds.push(value.userId);
      });
    }
    return userIds;
  }

  private getApprovedByUsersIds() {
    let userIds: number[] = [];
    this.createPaymentRequestForm.value.approvers.forEach(function (value) {
      userIds.push(value.userId);
    });
    return userIds;
  }

  renderForm(event: any) {
    this.createPaymentRequestForm.controls.registrationNumber.patchValue(event.registrationNumber);
    this.createPaymentRequestForm.controls.vendorAccountNumber.patchValue(event.accountNumber);
    this.createPaymentRequestForm.controls.registrationType.patchValue(event.registrationType);

    this.createPaymentRequestForm.controls.registrationNumber.disable();
    this.createPaymentRequestForm.controls.vendorAccountNumber.disable();
    this.createPaymentRequestForm.controls.registrationType.disable();
  }

  goTo() {
    this.createPaymentRequestForm.reset();
    this.router.navigate(['/periodic/payments', this.periodicPaymentTypeId]);
  }
}
