import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ApprovalRequestComponent} from './approval-request/approval-request.component';
import {MainLayoutComponent} from '../../layout';
import {NestedlayoutComponent} from './nestedlayout/nestedlayout.component';
import {BillPaymentListComponent} from './bill-payment-list/bill-payment-list.component';
import {BillPaymentSupportComponent} from './bill-payment-support/bill-payment-support.component';
import {BillPaymentApproveComponent} from './bill-payment-approve/bill-payment-approve.component';
import {ApprovedBillPaymentsComponent} from './approved-bill-payments/approved-bill-payments.component';
import {MakePaymentComponent} from './make-payment/make-payment.component';
import {BillDetailComponent} from './approved-bill-payments/bill-detail/bill-detail.component';
import {PreApprovedComponent} from "./pre-approved/pre-approved.component";
import {ReturnedComponent} from "./returned/returned.component";
import {RejectedComponent} from "./rejected/rejected.component";

const routes: Routes = [
  {
    path: 'bill-payment',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: NestedlayoutComponent,
        children: [
          {
            path: 'all',
            component: BillPaymentListComponent
          },
          {
            path: 'pre-approved',
            component: PreApprovedComponent
          },
          {
            path: 'support-request',
            component: BillPaymentSupportComponent
          },
          {
            path: 'approve-request',
            component: BillPaymentApproveComponent
          },
          {
            path: 'returned-request',
            component: ReturnedComponent
          },
          {
            path: 'rejected-request',
            component: RejectedComponent
          },
          {
            path: 'create/:searchId',
            component: ApprovalRequestComponent
          },
          // {
          //   path: ':id',
          //   component: RequestDetailComponent
          // }
        ]
      }
    ],
  },
  {
    path: 'approved-bills',
    component: MainLayoutComponent,
    children: [
      {
        path: 'all',
        component: ApprovedBillPaymentsComponent
      },
      {
        path: 'make-payment',
        component: MakePaymentComponent
      },
      {
        path:':expDetailId/:type/:paymentStatus',
        component: BillDetailComponent
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillPaymentRouting {

}
