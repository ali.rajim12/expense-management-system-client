import {UserMini} from '../../users/model/user-mini.model';

export class PaymentDetail {
  expenditurePaymentId: number;
  requestedBy: string;
  pushedBy: string;
  paymentStatus: string;
  billPaymentCode: string;
  preApprovalCode: string;
  memoDate: any;
  fiscalYear: string;
  billAmount: string;
  subject: string;
  description: string;
  expenditureDetailId: number;
  billPaymentRequestedBranch: string;
}
