export class VerifyPaymentDetail {
  remarks: string;
  assignedToUserId: number;
  expenditureDetailId: number;
  memoCode: string;
  billNumber: string;

}
