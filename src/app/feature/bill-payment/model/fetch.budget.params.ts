export class FetchBudgetParams {
  fiscalYearId: number;
  month: string;
  branchId: number;
  accountHeadId:  number;
}
