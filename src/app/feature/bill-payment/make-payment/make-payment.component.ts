import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ExpenditureModel} from '../../pre-approval/models/expenditure.model';
import {BillPaymentService} from '../service/bill-payment.service';
import {ColumnName} from '../../../shared/constant/column-name';
import {PaymentDetail} from '../model/payment-detail.model';
import {ActivatedRoute, Router} from '@angular/router';
import {UserMini} from "../../users/model/user-mini.model";
import {UserService} from "../../users/service/user.service";
import * as FileSaver from "file-saver";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomMessageService} from "../../../shared/service/custom-message.service";

@Component({
  selector: 'app-make-payment',
  templateUrl: './make-payment.component.html',
  styleUrls: ['./make-payment.component.sass']
})
export class MakePaymentComponent implements OnInit {

  data$: Observable<PaymentDetail[]>;
  columnName: any[];
  page = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  selectedItems: ExpenditureModel;
  refCode: string = '';
  fragment: string;
  paymentStatus: string = 'UNPAID';
  allPaymentStatus: string[] = ['ALL', 'PAID', 'UNPAID'];
  allUsersVerifier: UserMini[];
  allUsersApprover: UserMini[];
  verifiedBy: number = 0;
  paymentBy: number = 0;

  minFromDate: Date;
  minToDate: Date;
  fromDate = this.getFromDate();
  toDate = new Date();
  loading: boolean = false;
  display: boolean = false;
  addPaymentDetailForm: FormGroup;
  submitted: boolean = false;
  selectedPaymentId: number;

  constructor(private billPaymentService: BillPaymentService,
              private userService: UserService,
              private router: Router,
              private fb: FormBuilder,
              private customMessageService: CustomMessageService) {
    this.addPaymentDetailForm = fb.group({
      txnDateString: [null, Validators.required],
      txnId: [null, Validators.required],
      remarks: [null, Validators.required]
    });
  }

  get f() {
    return this.addPaymentDetailForm.controls;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.MAKE_PAYMENT_TABLE_COLUMN_LIST;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllUsers();
    this.fetchAllApproverUsers();
  }

  reloadData(page: number, size: number) {
    this.billPaymentService.fetchAllPaymentRequests(this.refCode,
      this.paymentStatus,
      this.fromDate,
      this.toDate,
      this.verifiedBy,
      this.paymentBy,
      page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    const pageIndex = event.first / event.rows;
    this.page = pageIndex;
    this.reloadData(pageIndex, event.rows);
  }

  searchDetail() {
    this.reloadData(0, this.itemsPerPage);
  }

  fetchAllUsers() {
    this.userService.findAllVerifierUsers().subscribe(data => {
      this.allUsersVerifier = data;
    })
  }

  fetchAllApproverUsers() {
    this.userService.findAllMakePaymentUsers().subscribe(data => {
      this.allUsersApprover = data;
    })
  }

  getFromDate() {
    var date = new Date();
    date.setDate(date.getDate() - 30);
    return date;
  }

  downloadFile(expenditurePaymentId: number, paymentCode: string) {
    this.loading = true;
    this.billPaymentService.fetchDocument(expenditurePaymentId)
      .subscribe((data) => {
        this.loading = false;
        FileSaver.saveAs(data, paymentCode+"_txn_detail.txt");
      }, error1 => {
        this.loading = false;
        console.log(error1);
      });
  }

  makePayment(paymentId: number) {
    this.selectedPaymentId = paymentId;
    this.display = true;
  }

  cancel() {
    this.display = false;
    this.addPaymentDetailForm.reset();
    this.submitted = false;
  }

  confirm() {
    this.submitted = true;
    if (this.addPaymentDetailForm.invalid) {
      return;
    }
    this.loading = true;
    this.billPaymentService.submitRequest(this.addPaymentDetailForm.value, this.selectedPaymentId)
      .subscribe((response) => {
        this.display = false;
        this.reloadData(this.page, this.itemsPerPage);
        this.customMessageService.sendSuccessErrorMessage(response.success_message,
          'success', true);
        this.loading = false;
      }, error1 => console.log(error1));

    console.log(this.addPaymentDetailForm.value)

  }
}
