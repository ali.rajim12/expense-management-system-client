import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {ExpenditureModel} from "../../pre-approval/models/expenditure.model";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {CommonVariableService} from "../../../shared";
import {ColumnName} from "../../../shared/constant/column-name";

@Component({
  selector: 'app-rejected',
  templateUrl: './rejected.component.html',
  styleUrls: ['./rejected.component.sass']
})
export class RejectedComponent implements OnInit {
  data$: Observable<ExpenditureModel[]>;
  columnName: any[];
  page: number = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<number>;
  totalRecords: number;
  selectedItems: ExpenditureModel;

  constructor(private preApprovalService: PreApprovalService,
              private commonVariableService: CommonVariableService,) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.PRE_APPROVAL_REQUST_COLUMN_LIST;
    this.reloadData(this.page, this.itemsPerPage);
  }

  reloadData(page: number, size: number) {
    this.preApprovalService.fetchAllReturnOrRejectedRequests('REJECTED',
      'BILL_PAYMENT',  page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
        // this.totalRecords = this.data.length;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page= event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

}
