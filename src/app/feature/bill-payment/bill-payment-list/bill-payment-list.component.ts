import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {PreApprovalResponse} from '../../pre-approval';
import {CommonVariableService} from '../../../shared';
import {ColumnName} from '../../../shared/constant/column-name';
import {BillPaymentService} from '../service/bill-payment.service';

@Component({
  selector: 'app-bill-payment-list',
  templateUrl: './bill-payment-list.component.html',
  styleUrls: ['./bill-payment-list.component.sass']
})
export class BillPaymentListComponent implements OnInit {
  data$: Observable<PreApprovalResponse[]>;
  errors$: Observable<string>;
  selectedItems: PreApprovalResponse;
  columnName: any[];
  page = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<any>;
  refCode: string = '';
  totalRecords: number;

  constructor(private billPaymentService: BillPaymentService,
              private commonVariableService: CommonVariableService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.PRE_APPROVAL_REQUST_COLUMN_LIST;
    this.reloadData(this.page, 25);
  }

  reloadData(page: number, size: number) {
    this.billPaymentService.fetchAllBillPaymentRequests(this.refCode, page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
        // this.totalRecords = this.data.length;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }


}
