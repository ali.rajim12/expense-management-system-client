import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Injectable} from '@angular/core';
import {PreApprovalDetail} from '../../pre-approval';
import {VerifyPaymentDetail} from '../model/verify-payment-detail.model';
import {FetchBudgetParams} from "../model/fetch.budget.params";

@Injectable({
  providedIn: 'root'
})
export class BillPaymentService {

  constructor(private http: HttpClient) {
  }

  fetchAllBillPaymentRequests(refCode: string, page: number, size: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/bill_payment`, {
      params:
        {ref_code: refCode, page: page + '', size: size + ''}
    });
  }


  submitBillPaymentRequest(formData: FormData, requestId: string): Observable<any> {
    if (requestId === 'new') {
      return this.http.post(`${environment.apiUrl}/api/auth/expenditure/bill_payment`, formData);
    } else {
      return this.http.post(`${environment.apiUrl}/api/auth/expenditure/bill_payment/update`, formData);
    }
  }

  fetchPreApprovalDetail(number: number): Observable<PreApprovalDetail> {
    return this.http.get<PreApprovalDetail>(`${environment.apiUrl}/api/auth/expenditure/bill_payment/exp_detail/${number}`);
  }

  fetchAllSupportOrApproveRequests(requestedFor: string, page: number, size: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/bill_payment/${requestedFor}`, {
      params:
        {ref_code: '', page: page + '', size: size + '', sort: 'id'}
    });
  }

  fetchAllApprovedBill(refCode: string,
                       page: number,
                       size: number,
                       fromDate: any,
                       toDate: any,
                       selectedBranchId: number,
                       requestedUserId: number
                       ): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/expenditure/approved_bills`, {
      params:
        {
          ref_code: refCode + '',
          page: page + '',
          size: size + '',
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          requested_by: requestedUserId+'',
          requested_by_branch: selectedBranchId+''
        }
    });
  }

  initiateVerifyPaymentRequest(request: VerifyPaymentDetail): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/payments/verify`, request);
  }

  fetchAllPaymentRequests(refCode: string,
                          paymentStatus: string,
                          fromDate: any,
                          toDate: any,
                          verifiedBy: number,
                          paymentBy: number,
                          page: number,
                          size: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/payments`, {
      params:
        {
          from_date: fromDate.toISOString() + '',
          to_date: toDate.toISOString() + '',
          payment_by: paymentBy+'',
          verified_by: verifiedBy+'',
          ref_code: refCode,
          payment_status: paymentStatus,
          page: page + '',
          size: size + ''
        }
    });
  }

  fetchAllPreApprovedRequests(refCode: string, page: number, size: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/expenditure/pre_approved`, {
      params:
        {ref_code: '', page: page + '', size: size + ''}
    });
  }

  fetchBudgetDetail(expTxnDetailId: number): Observable<FetchBudgetParams> {
    return this.http.get<FetchBudgetParams>(`${environment.apiUrl}/api/auth/expenditure/budget_fetch_params/${expTxnDetailId}`);
  }


  fetchDocument(expenditurePaymentId: number)  {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })
    return this.http.get(`${environment.apiUrl}/api/auth/payments/download/${expenditurePaymentId}`, {
      headers: headers, responseType: 'blob'
    });
  }

  submitRequest(value: any, selectedPaymentId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/payments/makePayment/${selectedPaymentId}`, value);

  }
}
