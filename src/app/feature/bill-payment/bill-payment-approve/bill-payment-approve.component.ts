import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ExpenditureModel} from '../../pre-approval/models/expenditure.model';
import {ColumnName} from '../../../shared/constant/column-name';
import {BillPaymentService} from '../service/bill-payment.service';
import {CommonVariableService} from '../../../shared';

@Component({
  selector: 'app-bill-payment-approve',
  templateUrl: './bill-payment-approve.component.html',
  styleUrls: ['./bill-payment-approve.component.sass']
})
export class BillPaymentApproveComponent implements OnInit {

  data$: Observable<ExpenditureModel[]>;
  columnName: any[];
  page = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  selectedItems: ExpenditureModel;


  constructor(private billPaymentService: BillPaymentService,
              private commonVariableService: CommonVariableService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.PRE_APPROVAL_REQUST_COLUMN_LIST;
    this.reloadData(this.page, this.itemsPerPage);
  }

  reloadData(page: number, size: number) {
    this.billPaymentService.fetchAllSupportOrApproveRequests('APPROVE', page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    const pageIndex = event.first / event.rows;
    this.page = event.page;
    this.reloadData(pageIndex, event.rows);
  }

}
