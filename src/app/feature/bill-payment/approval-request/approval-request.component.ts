import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Month} from '../../pre-approval/models/month.model';
import {FiscalYear} from '../../pre-approval/models/fiscal-year.model';
import {ExpenditureType} from '../../pre-approval/models/expenditure-type.model';
import {PreApprovalService} from '../../pre-approval/service/pre-approval.service';
import {ActivatedRoute, Router} from '@angular/router';
import {VendorsService} from '../../vendors/service/vendors.service';
import {Vendor} from '../../vendors/model/vendor.model';
import {AccountHead, AccountHeadService} from '../../account-head';
import {UserService} from '../../users/service/user.service';
import {UserMini} from '../../users/model/user-mini.model';
import {Branch, BranchService} from '../../branch';
import {ExpenditureTxnDetailTequest, PreApprovalDetail} from '../../pre-approval';
import {BillPaymentService} from '../service/bill-payment.service';
import {ExpenseDetail} from '../../pre-approval/models/Expense-detail.model';
import {CustomMessages} from '../../../shared/constant/custom-messages';
import {CustomMessageService} from '../../../shared/service/custom-message.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-approval-request',
  templateUrl: './approval-request.component.html',
  styleUrls: ['./approval-request.component.css']
})
export class ApprovalRequestComponent implements OnInit {
  public Editor = ClassicEditor;

  showConfirmationPage = false;
  approvalRequestForm: FormGroup;
  tdsCalculatorForm: FormGroup;
  showConfirmation: boolean = false;
  minDate: Date;
  requestedDate: Date = new Date();
  billingDate: Date = new Date();
  months: Month[];
  fiscalYears: FiscalYear[];
  expenditureTypes: ExpenditureType[];
  files: string[] = [];
  vendors: Vendor[];
  accountHeads: AccountHead[];
  approver: UserMini[];
  supporter: UserMini[];
  allBranch: Branch[];
  registrationTypes: string[] = ['PAN', 'VAT'];
  expenditureDetailId: string;
  preApprovalDetail: PreApprovalDetail;
  loading: boolean = false;
  expenditureTxnDetailId: number;
  submitted = false;
  showAmountMismatchMessage = false;
  showTdsCalculateForm: boolean = false;
  selectedIndex: number;
  showTDSTotalAmount: boolean = false;
  showFileValidationMessage: boolean = false;


  constructor(private fb: FormBuilder,
              private preApprovalRequestService: PreApprovalService,
              private vendorService: VendorsService,
              private accountHeadService: AccountHeadService,
              private activeRoute: ActivatedRoute,
              private userService: UserService,
              private branchService: BranchService,
              private billPaymentService: BillPaymentService,
              private router: Router,
              private customMessageService: CustomMessageService) {
    this.expenditureDetailId = activeRoute.snapshot.params.searchId;
    this.approvalRequestForm = this.fb.group({
      requestedDate: [this.requestedDate, Validators.required],
      expenditureType: [null, Validators.required],
      month: [null, Validators.required],
      fiscalYear: [null, Validators.required],
      subject: [null, Validators.required],
      expenseDetail: [null, Validators.required],
      link: [null],
      preApprovalRequestId: null,
      vendor: [null, Validators.required],
      billingDate: [this.billingDate, Validators.required],
      billAmount: [null, Validators.required],
      registrationType: [null, Validators.required],
      tdsAmount: 0,
      billNumber: [null, Validators.required],
      advance: null,
      registrationNumber: null,
      vendorAccountNumber: null,
      file: [null],
      transactionDetailRequest: this.fb.array([this.newTransactionDetail()]),
      supporters: [null],
      approvers: [null, Validators.required],
      penaltyAmount: 0,
      rebateAmount: 0
    });
    this.tdsCalculatorForm = this.fb.group({
      registrationType: ['PAN', [Validators.required]],
      tdsPercentage: [0, [Validators.required]]
    });
  }

  get f() {
    return this.approvalRequestForm.controls;
  }

  transactionDetailRequest(): FormArray {
    return this.approvalRequestForm.get('transactionDetailRequest') as FormArray;
  }

  addTransactionDetail() {
    this.transactionDetailRequest().push(this.newTransactionDetail());
  }

  removeTransactionDetail(index: number) {
    this.transactionDetailRequest().removeAt(index);
  }

  newTransactionDetail(): FormGroup {
    return this.fb.group({
      accountHeadId: ['', Validators.required],
      amount: [0, Validators.required],
      creditAccount: ['', [Validators.required, Validators.maxLength(16)]],
      transactionDetail: ['', [Validators.required, Validators.minLength(1),
        Validators.maxLength(45)]],
      debitBranchId: ['', Validators.required],
      creditBranchId: ['', Validators.required],
      expenditureTxnDetailId: 0,
      tdsAmount: [0, Validators.required],
      billAmt: [0, Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadAllDetail();
  }

  loadAllDetail() {
    this.fetchAllExpenditureTypes();
    this.fetchAllFiscalYears();
    this.fetchAllMonths();
    this.fetchAllAccountHeads();
    this.fetchAllBranch();
    this.fetchAllActiveVendors();
    if (this.expenditureDetailId !== 'new') {
      this.fetchPreApprovalRequestDetail();
    }
  }

  fetchAllActiveVendors() {
    this.vendorService.fetchAllActiveVendors()
      .subscribe(data => {
        this.vendors = data;
      });
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonthsForAddEdit(this.expenditureDetailId === 'new'? false: true)
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  fetchAllExpenditureTypes() {
    this.preApprovalRequestService.getAllExpenditureTypes()
      .subscribe((response) => {
        this.expenditureTypes = response;
      }, error1 => console.log(error1));
  }

  onFileChange(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.files.push(event.target.files[i]);
    }
  }

  fetchAllAccountHeads() {
    this.accountHeadService.fetchAllAccountHead()
      .subscribe((response) => {
        this.accountHeads = response;
      }, error1 => console.log(error1));
  }

  searchApprover(event: any) {
    this.userService.getAllApprovers(event.query).subscribe(data => {
      this.approver = data;
    });
  }

  searchSupporter(event: any) {
    this.userService.getAllSupporter(event.query).subscribe(data => {
      this.supporter = data;
    });
  }

  fetchAllBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.allBranch = data;
    }, error1 => {
      console.log(error1);
    });
  }

  confirmCreateRequest() {
    this.submitted = true;
    if (this.approvalRequestForm.value.billAmount !== undefined) {
      if (Number.parseFloat(this.approvalRequestForm.value.billAmount) < this.getTotalAmount()) {
        this.showAmountMismatchMessage = true;
        return;
      }
    }
    if (this.approvalRequestForm.invalid) {
      return;
    }
    this.showConfirmationPage = true;
    this.showAmountMismatchMessage = false;

  }

  getTotalAmount() {
    let total: number = 0;
    this.approvalRequestForm.value.transactionDetailRequest.forEach(function (value) {
      total = total + value.amount;
    });
    return total;
  }

  validateFile(file: any) {
    var ext = file.name.substring(file.name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() === 'js' ||
      ext.toLowerCase() === 'php' ||
      ext.toLowerCase() === 'py') {
      return false;
    } else {
      return true;
    }
  }

  onSubmit() {
    this.loading = true;
    const formData = new FormData();
    for (var i = 0; i < this.files.length; i++) {
      if (!this.validateFile(this.files[i])) {
        this.loading = false;
        this.showConfirmationPage = false;
        this.showFileValidationMessage = true;
        return;
      }
      formData.append('files', this.files[i]);
    }
    let detail = {
      preApprovalRequestId: this.activeRoute.snapshot.params.searchId === 'new' ? 0 : this.activeRoute.snapshot.params.searchId,
      budgetDetailRequest: {
        requestedDate: this.approvalRequestForm.value.requestedDate,
        expenditureType: this.approvalRequestForm.value.expenditureType.type,
        month: this.approvalRequestForm.value.month.months,
        fiscalYearId: this.approvalRequestForm.value.fiscalYear.id,
        subject: this.approvalRequestForm.value.subject,
        expenseDetail: this.approvalRequestForm.value.expenseDetail,
        link: this.approvalRequestForm.value.link
      },
      billDetailRequest: {
        vendorId: this.approvalRequestForm.value.vendor.vendorId,
        billingDate: this.approvalRequestForm.value.billingDate,
        billNumber: this.approvalRequestForm.value.billNumber,
        billAmount: this.approvalRequestForm.value.billAmount,
        advance: this.approvalRequestForm.value.advance,
        registrationType: this.approvalRequestForm.value.vendor.registrationType,
        registrationNumber: this.approvalRequestForm.value.vendor.registrationNumber,
        tdsAmount: this.approvalRequestForm.value.tdsAmount,
        vendorAccountNumber: this.approvalRequestForm.value.vendor.accountNumber,
        penaltyAmount: this.approvalRequestForm.value.penaltyAmount,
        rebateAmount: this.approvalRequestForm.value.rebateAmount,
      },
      expenditureTxnDetailRequest: this.getExpenditureTxnDetailRequest(),
      supportedByUserIds: this.getSupportedBYUserIds(),
      approvedByUserIds: this.getApprovedByUsersIds()
    };
    formData.append('detail', JSON.stringify(detail));
    this.billPaymentService.submitBillPaymentRequest(formData, this.activeRoute.snapshot.params.searchId)
      .subscribe(
        data => {
          this.showConfirmationPage = false;
          this.router.navigate(['/bill-payment/all']);
          this.customMessageService.sendSuccessErrorMessage(CustomMessages.BILL_PAYMENT_REQUEST_CREATED_SUCCESSFULLY,
            'success', true);
        },
        error => {
          this.showConfirmationPage = false;
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(error, 'error', true);
        });
  }


  getExpenditureTxnDetailRequest() {
    let expentitureTxnDetail: ExpenditureTxnDetailTequest[] = [];
    this.approvalRequestForm.value.transactionDetailRequest.forEach(function (value) {
      expentitureTxnDetail.push(new ExpenditureTxnDetailTequest(
        value.accountHeadId.accountHeadId,
        value.billAmt,
        value.transactionDetail,
        value.debitBranchId.branchId,
        value.creditBranchId.branchId,
        value.creditAccount,
        value.expenditureTxnDetailId,
        value.tdsAmount
      ));
    });
    return expentitureTxnDetail;
  }

  private getSupportedBYUserIds() {
    let userIds: number[] = [];
    if (this.approvalRequestForm.value.supporters !== null) {
      this.approvalRequestForm.value.supporters.forEach(function (value) {
        userIds.push(value.userId);
      });
    }
    return userIds;
  }

  private getApprovedByUsersIds() {
    let userIds: number[] = [];
    this.approvalRequestForm.value.approvers.forEach(function (value) {
      userIds.push(value.userId);
    });
    return userIds;
  }

  private fetchPreApprovalRequestDetail() {
    this.billPaymentService.fetchPreApprovalDetail(Number(this.expenditureDetailId))
      .subscribe((data) => {
        this.preApprovalDetail = data;
        // this.approvalRequestForm.controls.requestedDate.patchValue(new Date(this.preApprovalDetail.memoDateBillPayment));
        this.approvalRequestForm.controls.month.patchValue(this.getMonth(this.preApprovalDetail.month));
        this.approvalRequestForm.controls.link.patchValue(this.preApprovalDetail.link);
        this.approvalRequestForm.controls.expenditureType.patchValue(
          this.getExpenditureType(this.preApprovalDetail.expenditureType));
        this.approvalRequestForm.controls.fiscalYear.patchValue(
          this.getFiscalYear(this.preApprovalDetail.fiscalYear));
        this.approvalRequestForm.patchValue({
          subject: this.preApprovalDetail.memoSubject,
          expenseDetail: this.preApprovalDetail.memoDetail,
        });
        this.approvalRequestForm.controls.billAmount.patchValue(this.preApprovalDetail.proposedAmount);
        this.approvalRequestForm.controls.advance.patchValue(this.preApprovalDetail.advance);
        this.approvalRequestForm.controls.penaltyAmount.patchValue(this.preApprovalDetail.penalty);
        this.approvalRequestForm.controls.billNumber.patchValue(this.preApprovalDetail.billNumber);
        this.approvalRequestForm.controls.rebateAmount.patchValue(this.preApprovalDetail.rebateAmount);

        const txnEntryDetail = this.approvalRequestForm.get('transactionDetailRequest') as FormArray;
        while (txnEntryDetail.length) {
          txnEntryDetail.removeAt(0);
        }
        data.expenditureTxnDetailResources.forEach(value => txnEntryDetail.push(this.fb.group({
          accountHeadId: this.getAccountHead(value.accountHeadId),
          amount: value.amount,
          creditAccount: value.creditAccount,
          transactionDetail: value.expenseDescription,
          debitBranchId: this.getDebitBranch(value.debitBranch?.branchId),
          creditBranchId: this.getDebitBranch(value.creditBranch?.branchId),
          expenditureTxnDetailId: this.activeRoute.snapshot.params.searchId === 'new' ? 0 : value.expenditureTxnDetailId,
          billAmt: 0,
          tdsAmount: 0
        })));
        if (this.preApprovalDetail.vendorId) {
          setTimeout(() => {
            const vendor: Vendor = this.selectVendor(this.preApprovalDetail.vendorId);
            this.approvalRequestForm.controls.vendor.patchValue(vendor);
            this.renderForm(vendor);
          }, 2000);
        }
      }, error1 => {
        console.log(error1);
      });
  }

  private getExpenditureType(expenditureType: string) {
    let requiredValue: ExpenditureType = null;
    this.expenditureTypes.forEach((value) => {
      if (value.type === expenditureType) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }

// TODO can be made same method
  private getMonth(month: string) {
    if (this.months === undefined) {
      this.fetchAllMonths();
    }
    let requiredValue: Month = null;
    this.months.forEach(value => {
      if (value.months === month) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }

  private getFiscalYear(fiscalYear: string) {
    let requiredValue: FiscalYear = null;
    this.fiscalYears.forEach(value => {
      if (value.year === fiscalYear) {
        requiredValue = value;
      }
    });
    return requiredValue;
  }

  private selectVendor(vendorId: number) {
    // setTimeout(() => {
    if (this.vendors === undefined) {
      this.fetchAllActiveVendors();
    }
    let selectedVendor: Vendor = null;
    this.vendors.forEach(value => {
      if (value.vendorId === vendorId) {
        selectedVendor = value;
      }
    });
    return selectedVendor;
    // }, 2000);
  }

  private getAccountHead(accountHeadId: number) {
    if (this.accountHeads === undefined) {
      this.fetchAllAccountHeads();
    }
    let requiredAccountHead: AccountHead = null;
    this.accountHeads.forEach(value => {
      if (value.accountHeadId === accountHeadId) {
        requiredAccountHead = value;
      }
    });
    return requiredAccountHead;
  }

  private getDebitBranch(branchId: number) {
    let requiredBranch: Branch = null;
    this.allBranch.forEach(value => {
      if (value.branchId === branchId) {
        requiredBranch = value;
      }
    });
    return requiredBranch;
  }

  renderForm(event: any) {
    this.approvalRequestForm.controls.registrationNumber.patchValue(event.registrationNumber);
    this.approvalRequestForm.controls.vendorAccountNumber.patchValue(event.accountNumber);
    this.approvalRequestForm.controls.registrationType.patchValue(event.registrationType);

    this.approvalRequestForm.controls.registrationNumber.disable();
    this.approvalRequestForm.controls.vendorAccountNumber.disable();
    this.approvalRequestForm.controls.registrationType.disable();
  }

  calculateTDSAmount(index: number) {
    if ((this.approvalRequestForm.value.vendor !== null
      && this.approvalRequestForm.value.vendor.registrationType === 'PAN')) {
      this.tdsCalculatorForm.controls.tdsPercentage.patchValue('15');
    } else {
      this.tdsCalculatorForm.controls.tdsPercentage.patchValue('1.5');
    }
    this.selectedIndex = index;
    this.showTdsCalculateForm = true;
    if (this.approvalRequestForm.value.vendor !== null) {
      this.tdsCalculatorForm.controls.registrationType.patchValue(this.approvalRequestForm.value.vendor.registrationType);
    }
  }

  fillForm(value: string) {
    this.tdsCalculatorForm.controls.tdsPercentage.patchValue(value);
  }

  resetTDSForm() {
    this.tdsCalculatorForm.reset();
    this.showTdsCalculateForm = false;
  }

  calculate() {
    this.showTdsCalculateForm = false;
    let tdsAmount: number = 0;
    let totalAmount: number = 0;
    if (this.approvalRequestForm.value.transactionDetailRequest[this.selectedIndex] !== 'Undefined'
      && this.approvalRequestForm.value.transactionDetailRequest[this.selectedIndex].billAmt > 0) {
      let billAmount: number = this.approvalRequestForm.value.transactionDetailRequest[this.selectedIndex].billAmt;
      if (this.tdsCalculatorForm.controls.registrationType.value === 'PAN') {
        totalAmount = (billAmount);
        tdsAmount = (totalAmount * this.tdsCalculatorForm.value.tdsPercentage) / 100;
      } else {
        totalAmount = (billAmount / 1.13);
        tdsAmount = (totalAmount * this.tdsCalculatorForm.value.tdsPercentage) / 100;
      }
      totalAmount = billAmount - tdsAmount;
      this.approvalRequestForm.controls.transactionDetailRequest.patchValue(
        this.setValues(tdsAmount, totalAmount, this.approvalRequestForm.value.transactionDetailRequest));
      this.showTDSTotalAmount = true;
    }
  }

  setValues(tdsAmount: number,
            totalAmount: number,
            expenditureTxnDetailResources: ExpenseDetail[]) {
    let requiredDetail = [];
    expenditureTxnDetailResources.forEach((value, index) => {
      requiredDetail.push({
        amount: (index === this.selectedIndex) ? totalAmount : value.amount,
        tdsAmount: (index === this.selectedIndex) ? tdsAmount : value.tdsAmount
      });
    });
    return requiredDetail;
  }

  resetForm() {
    this.submitted = false;
    this.approvalRequestForm.reset();
  }
}




