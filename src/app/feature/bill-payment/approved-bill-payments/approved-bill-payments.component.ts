import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {PreApprovalResponse} from '../../pre-approval';
import {BillPaymentService} from '../service/bill-payment.service';
import {CommonVariableService} from '../../../shared';
import {ColumnName} from '../../../shared/constant/column-name';
import {Branch, BranchService} from "../../branch";
import {UserMini} from "../../users/model/user-mini.model";
import {UserService} from "../../users/service/user.service";

@Component({
  selector: 'app-approved-bill-payments',
  templateUrl: './approved-bill-payments.component.html',
  styleUrls: ['./approved-bill-payments.component.sass']
})
export class ApprovedBillPaymentsComponent implements OnInit {

  data$: Observable<PreApprovalResponse[]>;
  selectedItems: PreApprovalResponse;
  columnName: any[];
  page = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<any>;
  refCode: string = '';
  totalRecords: number;
  fragment: string;

  minFromDate: Date;
  minToDate: Date;
  fromDate = this.getFromDate();
  toDate = new Date();
  memoCode: string = '';
  selectedBranchId: number = 0;
  allBranch: Branch[] = [];
  requestedUserId: number = 0;
  allUsers: UserMini[];

  constructor(private billPaymentService: BillPaymentService,
              private branchService: BranchService,
              private userService: UserService,
              private commonVariableService: CommonVariableService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.PRE_APPROVAL_REQUST_COLUMN_LIST;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllBranchForReports();
    this.fetchAllUsers();
  }

  getFromDate() {
    var date = new Date();
    date.setDate(date.getDate() - 10);
    return date;
  }

  reloadData(page: number, size: number) {
    this.billPaymentService.fetchAllApprovedBill(this.memoCode,
      page,
      size,
      this.fromDate,
      this.toDate,
      this.selectedBranchId,
      this.requestedUserId )
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
        // this.totalRecords = this.data.length;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    const pageIndex = event.first / event.rows;
    this.page = pageIndex;
    this.reloadData(pageIndex, event.rows);
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage);
  }

  fetchAllBranchForReports() {
    this.branchService.fetchBranchListForReports().subscribe(branchList => {
      this.allBranch = branchList;
    })
  }

  fetchAllUsers() {
    this.userService.findAllReportUsers().subscribe(data => {
      this.allUsers = data;
    })
  }


}
