import {Component, OnInit} from '@angular/core';
import {PreApprovalDetail} from '../../../pre-approval';
import {PreApprovalService} from '../../../pre-approval/service/pre-approval.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfirmationService} from 'primeng/api';
import {BillPaymentService} from '../../service/bill-payment.service';
import {VerifyPaymentDetail} from '../../model/verify-payment-detail.model';
import {MakePaymentService} from "../../../payments/service/make.payment.service";
import {CustomMessageService} from "../../../../shared/service/custom-message.service";
import {ViewportScroller} from "@angular/common";
import {ReportsService} from "../../../reports/service/reports.service";
import {Observable} from "rxjs";
import {ExpenditureModel} from "../../../pre-approval/models/expenditure.model";
import {ColumnName} from "../../../../shared/constant/column-name";

@Component({
  selector: 'app-bill-detail',
  templateUrl: './bill-detail.component.html',
  styleUrls: ['./bill-detail.component.css']
})
export class BillDetailComponent implements OnInit {
  preApprovalDetail: PreApprovalDetail;
  expenditureDetailId: number;
  approvalType: string;
  id: number;
  type: string;
  billNumber: string;
  paymentStatus: string;
  links: string[] = [];
  loading: boolean = false;
  showVendorReport: boolean = false;

  data$: Observable<ExpenditureModel[]>;
  totalRecords: number;

  // columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  columnName: any[];

  constructor(private preApprovalService: PreApprovalService,
              private confirmationService: ConfirmationService,
              private billPaymentService: BillPaymentService,
              private router: Router,
              private activeRoute: ActivatedRoute,
              private makePaymentService: MakePaymentService,
              private customMessageService: CustomMessageService,
              private viewportScroller: ViewportScroller,
              private reportService: ReportsService) {
    this.id = this.activeRoute.snapshot.params.expDetailId;
    this.paymentStatus = this.activeRoute.snapshot.params.paymentStatus;
    this.approvalType = this.activeRoute.snapshot.params.type;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.ALL_MEMO_REPORTS;
    this.expenditureDetailId = this.activeRoute.snapshot.params.expDetailId;
    this.getPreApprovalDetail(this.expenditureDetailId);
    this.type = 'BILL_PAYMENT';
  }

  getPreApprovalDetail(id: number) {
    this.preApprovalService.fetchPreApprovalDetail(id)
      .subscribe((response) => {
        this.preApprovalDetail = response;
        if (this.preApprovalDetail.link !== null) {
          this.preApprovalDetail.link.split(" ").forEach((value => {
            this.links.push(value);
          }))
        }
      }, error1 => console.log(error1));
  }

  getBillNumber(event: any) {
    this.billNumber = event.billNumber;
  }

  confirm(approvalType: string) {
    this.confirmationService.confirm({
      message: approvalType == 'verfiy' ? 'Are you sure that you want to Approve this bill?' :
        'Are you sure that you want to make payment ?',
      accept: () => {
        if (approvalType == 'verify') {
          let request: VerifyPaymentDetail = {
            assignedToUserId: 0,
            expenditureDetailId: this.expenditureDetailId,
            remarks: 'Verfied',
            memoCode: this.preApprovalDetail.referenceNumberBillPayment,
            billNumber: this.billNumber
          };
          this.loading = true;
          this.billPaymentService.initiateVerifyPaymentRequest(request).subscribe(() => {
            this.loading = false;
            this.router.navigate(['/approved-bills/all']);
          })
        } else {
          this.loading = true;
          this.makePaymentService.makePaymentToCBS(this.id)
            .subscribe((data) => {
              this.router.navigate(['/approved-bills/make-payment']);
              setTimeout(() => {
                this.loading = false;
                this.customMessageService.sendSuccessErrorMessage(data.success_message,
                  'success', false);
                this.viewportScroller.scrollToPosition([0, 0]);
              }, 1500)
              // this.reloadComponent();
            }, error => {
              this.loading = false;
              this.customMessageService.sendSuccessErrorMessage(error, 'error',
                false);
              this.viewportScroller.scrollToPosition([0, 0]);
            })
        }
      }
    });
  }

  goToLink() {
    window.open('/doc/pdf/' + this.id, "_blank");
  }

  goToRefLink(link: string) {
    window.open(link, "_blank");
  }

  viewVendorDetail() {
    this.showVendorReport = true;
    this.reloadVendorTable(this.page, this.itemsPerPage);
  }

  reloadVendorTable(page: number, size: number) {
    this.reportService.fetchAllVendorDetailByExpDetailId(this.id, page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1))
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadVendorTable(pageIndex, event.rows);
  }

  goTo() {
    // [routerLink]="['/pre-approval', list.id, 'BILL_PAYMENT']"
    window.open('/pre-approval/' + this.id+'/BILL_PAYMENT', "_blank");
  }

  // reloadComponent() {
  //   this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  //   this.router.onSameUrlNavigation = 'reload';
  //   this.router.navigate(['/approved-bills', this.expenditureDetailId, 'payment', 'PAID']);
  // }

}
