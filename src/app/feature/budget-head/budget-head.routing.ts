import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {NgModule} from '@angular/core';
import {BudgetHeadListComponent} from './budget-head-list/budget-head-list.component';

const routes: Routes = [
  {
    path : 'budget-head',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: BudgetHeadListComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetHeadRouting {

}
