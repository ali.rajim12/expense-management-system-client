import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {BudgetHead} from '..';

@Injectable({
  providedIn: 'root'
})
export class BudgetHeadService {

  constructor(private http: HttpClient) { }


  getAllBudgetHeads(): Observable<BudgetHead[]> {
    return this.http.get<BudgetHead[]>(`${environment.apiUrl}/api/auth/budget_head/all`);
  }

  getAllActiveBudgetHeads(): Observable<BudgetHead[]> {
    return this.http.get<BudgetHead[]>(`${environment.apiUrl}/api/auth/budget_head`);
  }

  updateBudgetHeadStatus(status: string, budgetHeadId: string) {
    return this.http.put(`${environment.apiUrl}/api/auth/budget_head/${budgetHeadId}/${status}`, null);
  }

  submitBudgetHead(value: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/budget_head`, value);
  }
}
