import {UserGroup} from '../../users/model/user-group.model';

export class BudgetHead {
  budgetHead: string;
  userGroupResource: UserGroup;
  code: string;
  createdDate: string;
  budgetHeadId: number;
  status: string;
}

