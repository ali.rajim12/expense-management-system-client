import {Component, OnInit} from '@angular/core';
import {Designation} from '../../users/model/designation.model';
import {DesignationService} from '../../users/service/designation.service';
import {ColumnName} from '../../../shared/constant/column-name';
import {BudgetHeadService} from '../service/budget-head.service';
import {BudgetHead} from '..';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserGroup} from '../../users/model/user-group.model';
import {UserGroupService} from '../../users/service/user-group.service';

@Component({
  selector: 'app-budget-head-list',
  templateUrl: './budget-head-list.component.html',
  styleUrls: ['./budget-head-list.component.sass']
})
export class BudgetHeadListComponent implements OnInit {

  data$: BudgetHead[];
  userGroups: UserGroup[];
  columnName: any[];
  totalRecords: number;
  page: number;
  addEditType: string;
  display: boolean;
  addEditBudgetHeadForm: FormGroup;

  constructor(private budgetHeadService: BudgetHeadService,
              private userGroupService: UserGroupService,
              private fb: FormBuilder) {
    this.addEditBudgetHeadForm = fb.group({
      budgetHead: null,
      userGroupId: null,
      budgetCode: null
    });
  }

  ngOnInit(): void {
    this.columnName = ColumnName.BUDGET_HEAD_TABLE;
    this.reloadData();
  }

  reloadData() {
    this.budgetHeadService.getAllBudgetHeads()
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = this.data$.length;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
  }

  handleChange($event: any, budgetHeadId: string) {
    this.budgetHeadService.updateBudgetHeadStatus($event.checked ? 'ACTIVE' : 'INACTIVE',
      budgetHeadId).subscribe(() => {
      this.reloadData();
    }, error1 => {
      console.log(error1);
    });
  }

  addEditBudgetHead(type: string, value: any) {
    this.addEditType = type;
    this.display = true;
    this.getUserGroups()
    if(type === 'Edit') {
      this.setFormDetail(value)
    }
  }
  setFormDetail(value: any) {
    this.addEditBudgetHeadForm.controls.budgetHead.patchValue(value.budgetHead);
    this.addEditBudgetHeadForm.controls.budgetCode.patchValue(value.code);
  }

  getUserGroups() {
    this.userGroupService.getAllUserGroups()
      .subscribe((data) => {
        this.userGroups = data;
      }, error1 => {
        console.log(error1);
      });
  }

  cancel() {
    this.display = false;
    this.addEditBudgetHeadForm.reset();
  }

  submit() {
    if(this.addEditType === 'Add') {
      this.budgetHeadService.submitBudgetHead(this.addEditBudgetHeadForm.value)
        .subscribe(() => {
            this.display= false;
            this.addEditBudgetHeadForm.reset();
            this.reloadData();
          }, error1 => {
            console.log(error1);
          }
        );
    }
  }

}
