import {LineChartDataSets} from './line-chart-data-sets.model';

export class LineChartReportDetail {
  labels: string[];
  datasets: LineChartDataSets[];
  totalBudget: number;
  totalExpenses: number;
}
