import {RequestCount} from './request-count.model';

export class RequestReport {
  billPayment: RequestCount;
  preApproval: RequestCount;
}
