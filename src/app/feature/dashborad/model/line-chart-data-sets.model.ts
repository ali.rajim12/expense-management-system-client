export class LineChartDataSets {
  label: string;
  data: number[];
  fill: boolean;
  borderColor: string;
}
