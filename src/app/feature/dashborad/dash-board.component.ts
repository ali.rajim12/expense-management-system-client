import {Component, OnInit} from '@angular/core';
import {MessageService} from 'primeng/api';
import {DashBoardService} from './service/dash-board.service';
import {RequestReport} from './model/request-report.model';
import {Month} from '../pre-approval/models/month.model';
import {LineChartReportDetail} from './model/line-chart-report-detail.model';
import {FiscalYear} from '../pre-approval/models/fiscal-year.model';
import {PreApprovalService} from '../pre-approval/service/pre-approval.service';
import {AccountHead, AccountHeadService} from '../account-head';
import {Branch, BranchService} from '../branch';
import {CustomMessageService} from '../../shared/service/custom-message.service';
import {LoadingBarService} from '@ngx-loading-bar/core';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {
  data: any;
  doughnutData: any;
  countDetail: RequestReport;
  totalPreApproval: number;
  totalBillPayments: number;
  months: Month[];
  fiscalYearId: number = 4;
  lineChartDetail: LineChartReportDetail;
  remainingBudget: number;
  fiscalYears: FiscalYear[];
  fromYear: string;
  toYear: string;
  accountHeads: AccountHead[];
  branches: Branch[];
  accountHeadId: number = 0;
  branchId: number = 0;
  currentYear: string;
  loading: boolean =  false;
  loader = this.loadingBar.useRef();
  selectedMonth: string = '';
  branchBudgetDetail: any;


  constructor(private messageService: MessageService,
              private dashBoardService: DashBoardService,
              private preApprovalRequestService: PreApprovalService,
              private accountHeadService: AccountHeadService,
              private branchService: BranchService,
              private customMessageService: CustomMessageService,
              private loadingBar: LoadingBarService) {
  }

  selectData(event) {
    this.messageService.add({
      severity: 'info',
      summary: 'Data Selected',
      'detail': this.data.datasets[event.element._datasetIndex].data[event.element._index]
    });
  }

  ngOnInit(): void {
    this.fetchRequestCountDetail();
    this.fetchLineChartDetail();
    this.fetchFirstFiveExpenseDetail();
    this.fetchAllFiscalYears();
    this.loadData();
    this.getAccountHead();
    this.getAllBranch();
    this.fetchAllMonths();
    this.fetchBranchBudget();
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
        this.fiscalYearId = 4;
        this.fiscalYears.forEach(value => {
          if (value.id === this.fiscalYearId) {
            this.currentYear = value.year;
            this.getToAndFrom(this.currentYear);
          }
        });
      }, error1 => console.log(error1));
  }




  getToAndFrom(currentYear: string) {
    if (currentYear !== undefined) {
      let name = currentYear.split('/');
      this.fromYear = name[0];
      this.toYear = '20' + name[1];
    }
  }

  fetchFirstFiveExpenseDetail() {
    if(this.fiscalYearId) {
      return;
    }
    this.dashBoardService.getFirstFiveExpenses(this.fiscalYearId, this.branchId, this.selectedMonth)
      .subscribe((response) => {
        this.doughnutData = {
          labels: response.labels,
          datasets: [
            {
              data: response.data,
              backgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56',
                '#38de0a',
                '#9c099a'
              ],
              hoverBackgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56',
                '#38de0a',
                '#9c099a'
              ]
            }]
        };
      }, error1 => {
        console.log(error1);
      });
  }

  fetchLineChartDetail() {
    this.loading = true;
    this.dashBoardService.getLineChartDetail(this.fiscalYearId, this.branchId, this.accountHeadId, this.selectedMonth)
      .subscribe((response) => {
        if (response.datasets !== null) {
          this.loading = false;
          this.lineChartDetail = response;
          this.data = {
            labels: response.labels,
            datasets: response.datasets
          };
          this.remainingBudget = Math.round(((response.totalBudget - response.totalExpenses) + Number.EPSILON) * 100) / 100;
        }
      }, error => {
        this.loading = false;
        this.customMessageService.sendSuccessErrorMessage(error, 'error', true);
      });
  }

  fetchBranchBudget() {
    this.dashBoardService.getBranchBudget(this.fiscalYearId, this.branchId, this.accountHeadId, this.selectedMonth)
      .subscribe((response) => {
        if (response.datasets !== null) {
          this.branchBudgetDetail = response;
        }
      }, error => {
      });
  }

  fetchRequestCountDetail() {
    this.dashBoardService.getAllRequestCounts().subscribe((data) => {
      this.countDetail = data;
      this.totalPreApproval = this.countDetail.preApproval.myRequests +
        this.countDetail.preApproval.approveRequests + this.countDetail.preApproval.supportRequests;
      this.totalBillPayments = this.countDetail.billPayment.myRequests +
        this.countDetail.billPayment.approveRequests + this.countDetail.billPayment.supportRequests;
    }, error1 => {
      console.log(error1);
    });
  }

  loadData() {
    if (this.currentYear !== undefined) {
      this.getToAndFrom(this.currentYear);
    }
    this.fetchLineChartDetail();
    this.fetchFirstFiveExpenseDetail();
    this.fetchBranchBudget();
  }

  getAccountHead() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  getAllBranch() {
    this.branchService.fetchBranchListForReports().subscribe((data) => {
      this.branches = data;
    }, error1 => {
      console.log(error1);
    });
  }

  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonths()
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }


}
