import {NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {DashBoardComponent} from './dash-board.component';
import {AuthGuard} from "../../core/_helpers";

const routes: Routes = [
  {
    path: 'dashboard',
    component: MainLayoutComponent,
    // canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: DashBoardComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {

}
