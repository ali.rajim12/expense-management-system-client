import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RequestReport} from '../model/request-report.model';
import {User} from '../../../core/_models/user';
import {environment} from '../../../../environments/environment';
import {LineChartReportDetail} from '../model/line-chart-report-detail.model';
import {FirstFiveExpenses} from '../model/first-five-expenses.model';

@Injectable({providedIn: 'root'})
export class DashBoardService {

  constructor(private http: HttpClient) {
  }

  getAllRequestCounts(): Observable<RequestReport> {
    return this.http.get<RequestReport>(`${environment.apiUrl}/api/auth/dashboard`);
  }
  getLineChartDetail(fiscalYearId: number, branchId: number, accountHeadId: number, month: string): Observable<LineChartReportDetail> {
    let params = new HttpParams();
    params = params.append('fiscal_year_id', fiscalYearId+'');
    params = params.append('branchId', branchId+'');
    params = params.append('account_head_id', accountHeadId+'');
    params = params.append('month', month+'');
    return this.http.get<LineChartReportDetail>(`${environment.apiUrl}/api/auth/dashboard/line_chart`,{
      params: params
    });
  }
  getFirstFiveExpenses(fiscalYearId: number, branchId: number, month: string): Observable<FirstFiveExpenses> {
    let params = new HttpParams();
    params = params.append('fiscal_year_id', fiscalYearId+'');
    params = params.append('branchId', branchId+'');
    params = params.append('month', month+'');
    return this.http.get<FirstFiveExpenses>(`${environment.apiUrl}/api/auth/dashboard/first_five`,{
      params: params
    });
  }

  getBranchBudget(fiscalYearId: number, branchId: number, accountHeadId: number, selectedMonth: string): Observable<any> {
    let params = new HttpParams();
    params = params.append('fiscal_year_id', fiscalYearId+'');
    params = params.append('branchId', branchId+'');
    params = params.append('account_head_id', accountHeadId+'');
    params = params.append('month', selectedMonth+'');
    return this.http.get<LineChartReportDetail>(`${environment.apiUrl}/api/auth/dashboard/branch_budget`,{
      params: params
    });
  }
}
