import {Component, OnInit} from '@angular/core';
import {RentOwnerDetail} from "../model/rent-owner-detail.model";
import {RentService} from "../owner-list/service/rent.service";
import {ActivatedRoute} from "@angular/router";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AttachmentDetail} from "../../pre-approval/models/attachment-detail.model";
import * as FileSaver from "file-saver";
import {Branch, BranchService} from "../../branch";
import {AccountHead, AccountHeadService} from "../../account-head";
import {RentDetailModel} from "../model/rent-detail.model";
import {ColumnName} from "../../../shared/constant/column-name";

@Component({
  selector: 'app-rent-detail',
  templateUrl: './rent-detail.component.html',
  styleUrls: ['./rent-detail.component.css']
})
export class RentDetailComponent implements OnInit {
  rentOwnerDetail: RentOwnerDetail;
  ownerId: number;
  showRentFileUploadForm: boolean = false;
  uploadRentDocumentForm: FormGroup;
  updateStatusForm: FormGroup;
  updatePaymentForm: FormGroup;
  submitted: boolean = false;
  loadingOwnerUpload: boolean = false;
  documents: AttachmentDetail[];
  showAddRentDetailForm: boolean = false;
  addEditRentDetailForm: FormGroup;
  accountHeads: AccountHead[];
  branches: Branch[];

  minDate: Date;
  rentForTypes = [
    {key: 'Branch', value: 'BRANCH'},
    {key: 'ATM', value: 'ATM'},
    {key: 'Extension Counter', value: 'EXTENSION_COUNTER'}
  ];

  incrementPeriods = [
    {key: 'Every Month', value: 'EVERY_MONTH'},
    {key: 'Every Three Month', value: 'EVERY_THREE_MONTH'},
    {key: 'Every Six Month', value: 'EVERY_SIX_MONTH'},
    {key: 'Every Year', value: 'EVERY_YEAR'},
    {key: 'Every Two Year', value: 'EVERY_TWO_YEAR'},
    {key: 'Every Three Year', value: 'EVERY_THREE_YEAR'},
  ];

  incrementTypes = [
    {key: 'Percentage', value: 'PERCENTAGE'},
    {key: 'Flat', value: 'FLAT'}
  ];
  incrementOn = [
    {key: 'Base Amount', value: 'BASE_AMOUNT'},
    {key: 'Last Paid Amount', value: 'LAST_PAID_AMOUNT'}
  ];
  paymentType = [
    {key: 'Regular', value: 'REGULAR'},
    {key: 'Hold', value: 'HOLD'}
  ];
  expenseNature = [
    {key: 'Normal', value: 'NORMAL'},
    {key: 'Prepaid', value: 'PREPAID'}
  ];

  loadingRentDetail: boolean = false;
  currentDate = new Date();
  selectedRentDetailId: number;
  selectedRentDetail: RentDetailModel;
  rentDetails: RentDetailModel[];
  rentDetailType: string = '';
  columnName: any[];
  previewImage: boolean = false;
  imageUrl: string;

  constructor(private rentService: RentService,
              private activeRoute: ActivatedRoute,
              private fb: FormBuilder,
              private customMessageService: CustomMessageService,
              private branchService: BranchService,
              private accountHeadService: AccountHeadService
  ) {
    this.ownerId = this.activeRoute.snapshot.params.ownerId;
    this.uploadRentDocumentForm = this.fb.group({
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
      remarks: [null, [Validators.required, Validators.maxLength(50)]],
    })

    this.updateStatusForm = this.fb.group({
      remarks: [null, [Validators.required, Validators.maxLength(50)]],
    })

    this.updatePaymentForm = this.fb.group({
      tdsPaymentType: [null],
    })

    this.addEditRentDetailForm = this.fb.group({
      rentForType: [null, Validators.required],
      incrementPeriod: [null, Validators.required],
      rentIncrementType: [null, Validators.required],
      incrementOn: [null, Validators.required],
      value: [null, Validators.required],
      rentStartDate: [this.currentDate, Validators.required],
      baseRentAmount: [null, Validators.required],
      taxPercentage: [null, Validators.required],
      expiryDate: [this.currentDate, Validators.required],
      branchId: [null, Validators.required],
      accountHeadId: [null, Validators.required],
      rentDetail: [null, Validators.required],
      rentDescription: [null, Validators.required],
      taxAccountNumber: [null, Validators.required],
      expenseNature: [null, Validators.required],
      prePaidMonth: [0, Validators.required],
      numberOfPaidEMIs: [0, Validators.required],
      prePaidDays: [0, Validators.required],
    })
  }

  ngOnInit(): void {
    this.fetchRentDetail();
    // this.fetchDocuments();
    this.getAllRentDetails();
    this.columnName = ColumnName.RENT_DOCUMENTS;
  }


  fetchRentDetail() {
    this.rentService.fetchRentDetail(this.ownerId)
      .subscribe((data) => {
        this.rentOwnerDetail = data;
      }, error1 => {
        this.customMessageService.sendSuccessErrorMessage(error1, 'error', true);
      });
  }

  get f() {
    return this.uploadRentDocumentForm.controls;
  }

  get ff() {
    return this.addEditRentDetailForm.controls;
  }

  get fff() {
    return this.updateStatusForm.controls;
  }

  get ffff() {
    return this.updatePaymentForm.controls;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadRentDocumentForm.patchValue({
        fileSource: file
      });
    }
  }

  addDocument(detailId: number) {
    this.selectedRentDetailId = detailId;
    this.showRentFileUploadForm = true;
  }

  submitOwnerDocument() {
    this.submitted = true;
    if (this.uploadRentDocumentForm.invalid) {
      return;
    }
    this.loadingOwnerUpload = true;
    const formData = new FormData();
    formData.append('files', this.uploadRentDocumentForm.get('fileSource').value);
    // formData.append('remarks', this.uploadRentDocumentForm.value.remarks);
    this.rentService.uploadOwnerDocument(formData,
      this.ownerId,
      this.selectedRentDetailId,
      this.uploadRentDocumentForm.value.remarks).subscribe((data) => {
      this.loadingOwnerUpload = false;
      this.customMessageService.sendSuccessErrorMessage(
        'File Uploaded successfully.',
        'success',
        true);
      this.uploadRentDocumentForm.reset();
      this.showRentFileUploadForm = false;
      this.getAllRentDetails();
    }, error => {
      this.customMessageService.sendSuccessErrorMessage(error,
        'error', false);
      this.loadingOwnerUpload = false;
      this.uploadRentDocumentForm.reset();
      this.showRentFileUploadForm = false;
    })
    this.submitted = false;
  }

  cancelOwnerUpload() {
    this.showRentFileUploadForm = false;
  }


  download(attachmentId: number, fileName: string) {
    this.rentService.downloadFile(attachmentId, this.ownerId).subscribe(
      (data) => {
        FileSaver.saveAs(data, fileName);
      }, error => {
        console.log(error)
      }
    )
  }


  fetchAllAccountHeads() {
    this.accountHeadService.getAllAccountHeadList()
      .subscribe((data) => {
        this.accountHeads = data;
      }, error1 => {
        console.log(error1);
      });
  }

  getAllBranch() {
    this.branchService.getAllBranch()
      .subscribe((data) => {
        this.branches = data;
      }, error1 => {
        console.log(error1);
      });
  }

  addRentDetail(selectedRentDetailId: number) {
    this.showAddRentDetailForm = true;
    this.fetchAllAccountHeads();
    this.getAllBranch();
    if (selectedRentDetailId === 0) {
      this.selectedRentDetailId = selectedRentDetailId;
    }
  }


  submitRentDetail() {
    this.submitted = true;
    if (this.addEditRentDetailForm.invalid) {
      this.loadingRentDetail = false;
      return;
    }
    this.loadingRentDetail = true;
    this.rentService.addUpdateRentDetail(this.addEditRentDetailForm.value,
      this.ownerId, this.selectedRentDetailId).subscribe((data) => {
      this.loadingRentDetail = false;
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        false);
      this.addEditRentDetailForm.reset();
      this.showAddRentDetailForm = false;
      this.getAllRentDetails();
    }, error => {
      this.customMessageService.sendSuccessErrorMessage(error,
        'error', false);
      this.loadingOwnerUpload = false;
      this.addEditRentDetailForm.reset();
      this.showAddRentDetailForm = false;
    })
    this.submitted = false;
  }

  getAllRentDetails() {
    this.rentService.getAllRentDetails(this.ownerId)
      .subscribe((data) => {
        this.rentDetails = data;
      }, error1 => {
        console.log(error1);
      });

  }

  cancelRentDetail() {
    this.showAddRentDetailForm = false;
    this.loadingRentDetail = false;
    this.submitted = false;
    this.addEditRentDetailForm.reset();
  }

  editRentDetail(rentDetail: any) {
    this.selectedRentDetailId = rentDetail.rentDetailId;
    this.showAddRentDetailForm = true;
    this.rentDetailType = 'Edit';
    this.getAllBranch();
    this.fetchAllAccountHeads();
    this.addEditRentDetailForm.controls.rentForType.patchValue(rentDetail.rentForType);
    this.addEditRentDetailForm.controls.incrementPeriod.patchValue(rentDetail.incrementPeriod);
    this.addEditRentDetailForm.controls.rentIncrementType.patchValue(rentDetail.rentIncrementType);
    this.addEditRentDetailForm.controls.incrementOn.patchValue(rentDetail.incrementOn);
    this.addEditRentDetailForm.controls.value.patchValue(rentDetail.value);
    this.addEditRentDetailForm.controls.rentStartDate.patchValue(new Date());
    this.addEditRentDetailForm.controls.baseRentAmount.patchValue(rentDetail.baseRentAmount);
    this.addEditRentDetailForm.controls.taxPercentage.patchValue(rentDetail.taxPercentage);
    this.addEditRentDetailForm.controls.expiryDate.patchValue(new Date());
    this.addEditRentDetailForm.controls.branchId.patchValue(rentDetail.branchId);
    this.addEditRentDetailForm.controls.accountHeadId.patchValue(rentDetail.accountHeadId);
    this.addEditRentDetailForm.controls.rentDetail.patchValue(rentDetail.rentDetail);
    this.addEditRentDetailForm.controls.rentDescription.patchValue(rentDetail.rentDescription);
  }

  showUpdateStatusForm: boolean = false;
  updatedStatus: string;

  handleChange($event: any, renDetailId: number) {
    this.showUpdateStatusForm = true;
    this.selectedRentDetailId = renDetailId;
    this.updatedStatus = $event.checked ? 'ACTIVE' : 'INACTIVE';


  }

  submitStatusUpdate() {
    if (this.updateStatusForm.invalid) {
      return
    }
    this.rentService.updateRentDetailStatus(this.updatedStatus,
      this.selectedRentDetailId,
      this.ownerId, this.updateStatusForm.value.remarks).subscribe((data) => {
      this.showUpdateStatusForm = false;
      this.getAllRentDetails();
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        true);
    }, error1 => {
      console.log(error1);
    });
  }

  cancelStatusUpdate() {
    this.showUpdateStatusForm = false;
  }

  previewDocument(url: string) {
    this.previewImage = true;
    this.imageUrl = url;
  }

  showUpdatePaymentForm: boolean = false;
  updatePaymentType(detail: any) {
    this.selectedRentDetail = detail;
    this.showUpdatePaymentForm = true;
  }

  updatePaymentTypeStatus() {
    if (this.updatePaymentForm.invalid) {
      return
    }
    this.rentService.updatePaymentTypeStatus(
      this.selectedRentDetail.paymentType,
      this.selectedRentDetail.rentDetailId,
      this.ownerId,
      this.updatePaymentForm.value).subscribe((data) => {
      this.showUpdateStatusForm = false;
      this.getAllRentDetails();
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        true);
      this.showUpdatePaymentForm = false;
    }, error1 => {
      console.log(error1);
    });
  }

  cancelPaymentType() {
    this.showUpdatePaymentForm = false;
  }
}
