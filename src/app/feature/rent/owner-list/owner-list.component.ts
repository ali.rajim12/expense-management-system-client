import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {ExpenditureModel} from "../../pre-approval/models/expenditure.model";
import {Branch, BranchService} from "../../branch";
import {UserMini} from "../../users/model/user-mini.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ReportsService} from "../../reports/service/reports.service";
import {CommonVariableService} from "../../../shared";
import {UserService} from "../../users/service/user.service";
import {Router} from "@angular/router";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {User} from "../../../core/_models/user";
import {ColumnName} from "../../../shared/constant/column-name";
import * as FileSaver from "file-saver";
import {RentOwner} from "./model/rent.owner.model";
import {RentService} from "./service/rent.service";

@Component({
  selector: 'app-owner-list',
  templateUrl: './owner-list.component.html',
  styleUrls: ['./owner-list.component.sass']
})
export class OwnerListComponent implements OnInit {

  data$: Observable<RentOwner[]>;
  selectedItems: RentOwner;
  columnName: any[];
  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  memoCode: string = '';
  loading: boolean = false;
  name: string = '';
  showAddOwnerForm: boolean = false;
  addEditOwnerDetailForm: FormGroup;
  natureOfPayments = [
    {key: 'MC-Cheque', value: 'CHEQUE'},
    {key: 'Account Credit', value: 'ACCOUNT_PAYMENT'}
  ];

  paymentTo = [
    {key: 'Natural Person', value: 'NATURAL_PERSON'},
    {key: 'Institution', value: 'INSTITUTION'}
  ];

  submitted: boolean = false;
  addEditType: string;
  selectedOwner: number;
  branches: Branch[];

  constructor(private rentService: RentService,
              private commonVariableService: CommonVariableService,
              private branchService: BranchService,
              private userService: UserService,
              private router: Router,
              private fb: FormBuilder,
              private customMessageService: CustomMessageService) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.addEditOwnerDetailForm = fb.group({
      name: [null, Validators.required],
      address: [null, Validators.required],
      contactNumber: [null, Validators.required],
      accountNumber: [null, Validators.required],
      panNumber: [null, Validators.required],
      natureOfPayment: [null, Validators.required],
      remarks: [null, Validators.required],
      paymentTo: [null, Validators.required],
      branchId: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.columnName = ColumnName.RENT_OWNER;
    this.reloadData(this.page, this.itemsPerPage);
    this.getAllBranch();
  }

  get f() {
    return this.addEditOwnerDetailForm.controls;
  }

  getFromDate() {
    var date = new Date();
    date.setDate(date.getDate() - 10);
    return date;
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage)
  }

  reloadData(page: number, size: number) {
    this.rentService.fetchAllRentOwners(this.name, page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  addNewOwner(type: string, ownerId: any) {
    this.showAddOwnerForm = true;
    this.addEditType = type;
    if (ownerId !== null) {
      this.selectedOwner = ownerId.rentOwnerId;
      this.addEditOwnerDetailForm.controls.name.patchValue(ownerId.name);
      this.addEditOwnerDetailForm.controls.address.patchValue(ownerId.address);
      this.addEditOwnerDetailForm.controls.contactNumber.patchValue(ownerId.contactNumber);
      this.addEditOwnerDetailForm.controls.accountNumber.patchValue(ownerId.accountNumber);
      this.addEditOwnerDetailForm.controls.panNumber.patchValue(ownerId.panNumber);
      this.addEditOwnerDetailForm.controls.natureOfPayment.patchValue(ownerId.natureOfPayment);
      this.addEditOwnerDetailForm.controls.remarks.patchValue(ownerId.remarks);
    }
  }

  submitForm() {
    this.submitted = true;
    this.loading = true;
    if (this.addEditOwnerDetailForm.invalid) {
      return;
    }
    this.submitted = false;
    this.rentService.addUpdateRentOwner(this.addEditOwnerDetailForm.value, this.selectedOwner)
      .subscribe((data) => {
        this.loading = false;
        this.showAddOwnerForm = false;
        setTimeout(() => {
          this.customMessageService.sendSuccessErrorMessage(
            data.success_message,
            'success', true);
        }, 2000);
        this.reloadData(this.page, this.itemsPerPage);
      }, error1 => {
        this.loading = false;
        this.showAddOwnerForm = false;
        this.customMessageService.sendSuccessErrorMessage(error1, 'error', true);
      });
  }

  cancelSubmit() {
    this.showAddOwnerForm = false;
    this.addEditOwnerDetailForm.reset();
    this.submitted = false;
  }

  getAllBranch() {
    this.branchService.getAllBranch()
      .subscribe((data) => {
        this.branches = data;
      }, error1 => {
        console.log(error1);
      });
  }

// downloadExcel() {
//   this.loading = true;
//   this.reportService.fetchDataForExcel(this.fromDate,
//     this.toDate,
//     this.type,
//     this.memoCode)
//     .subscribe((data) => {
//       this.loading = false;
//       FileSaver.saveAs(data, "detail.xlsx");
//     }, error1 => {
//       this.loading = false;
//       console.log(error1);
//     });
// }
//
// changeType(event: string) {
//   this.type = event;
// }
//
// changeStatus(event: string) {
//   this.selectedRecentStatus = event;
// }
//
// fetchAllBranchForReports() {
//   this.branchService.fetchBranchListForReports().subscribe(branchList => {
//     this.allBranch = branchList;
//   })
// }
//
// fetchAllUsers() {
//   this.userService.findAllReportUsers().subscribe(data => {
//     this.allUsers = data;
//   })
// }
//
// rejectMemo(expenditureDetailId: number) {
//   this.showUploadFilePopUp = true;
//   this.selectedExpenditureDetailId = expenditureDetailId;
//
// }
//
// onFileChange(event) {
//   if (event.target.files.length > 0) {
//     const file = event.target.files[0];
//     this.uploadMemoRejectionForm.patchValue({
//       fileSource: file
//     });
//   }
// }
//
//
// cancelSubmit() {
//   this.showUploadFilePopUp = false;
//   this.uploadMemoRejectionForm.reset();
//   this.submitted = false;
//   this.loadingFileUpload = false;
// }
//
// submitUploadForm() {
//   this.submitted = true;
//   if (this.uploadMemoRejectionForm.invalid) {
//     return;
//   }
//   this.loadingFileUpload = true;
//   const formData = new FormData();
//   formData.append('files', this.uploadMemoRejectionForm.get('fileSource').value);
//   formData.append('detail', this.uploadMemoRejectionForm.value.remarks);
//   this.reportService.rejectMemoDetail(formData,
//     this.selectedExpenditureDetailId,
//     this.uploadMemoRejectionForm.value.remarks).subscribe((data) => {
//     this.loading = false;
//     this.customMessageService.sendSuccessErrorMessage(
//       data.success_message,
//       'success',
//       false);
//     //  this.reloadData(this.page, this.itemsPerPage);
//     this.loadingFileUpload = false;
//     this.uploadMemoRejectionForm.reset();
//     this.showUploadFilePopUp = false;
//     this.reloadData(this.page, this.itemsPerPage);
//   }, error => {
//     this.customMessageService.sendSuccessErrorMessage(error,
//       'error', false);
//     this.loadingFileUpload = false;
//     this.uploadMemoRejectionForm.reset();
//     this.showUploadFilePopUp = false;
//   })
// }

}
