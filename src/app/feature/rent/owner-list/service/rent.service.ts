import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {RentOwnerDetail} from "../../model/rent-owner-detail.model";
import {AttachmentDetail} from "../../../pre-approval/models/attachment-detail.model";
import {RentDetailModel} from "../../model/rent-detail.model";

@Injectable({
  providedIn: 'root'
})
export class RentService {

  constructor(private http: HttpClient) {
  }

  fetchAllRentOwners(name: string, page: number, size: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/rent/owner`, {
      params:
        {
          name: name + '',
          page: page + '',
          size: size + ''
        },
    });
  }

  addUpdateRentOwner(value: any, selectedOwner: number): Observable<any> {
    if (selectedOwner === undefined || selectedOwner === null) {
      return this.http.post(`${environment.apiUrl}/api/auth/rent/owner`, value);
    } else {
      return this.http.post(`${environment.apiUrl}/api/auth/rent/owner/${selectedOwner}`, value);
    }
  }

  fetchRentDetail(ownerId: number): Observable<RentOwnerDetail> {
    return this.http.get<RentOwnerDetail>(`${environment.apiUrl}/api/auth/rent/owner/${ownerId}/detail`);
  }

  uploadOwnerDocument(formData: FormData,
                      ownerId: number,
                      detailId: number,
                      remarks: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/rent_detail/${ownerId}/${detailId}/${remarks}`,
      formData);
  }

  fetchDocuments(ownerId: number): Observable<AttachmentDetail[]> {
    return this.http.get<AttachmentDetail[]>(`${environment.apiUrl}/api/auth/rent/owner/document/${ownerId}`);
  }

  downloadFile(attachmentId: number, ownerId: number) {
    return this.http.get(`${environment.apiUrl}/api/auth/rent/owner/download/${ownerId}/${attachmentId}`,
      {responseType: 'blob'});
  }

  addUpdateRentDetail(value: any, ownerId: number, selectedRentDetailId: number): Observable<any> {
    if (selectedRentDetailId === 0) {
      return this.http.post(`${environment.apiUrl}/api/auth/rent_detail/${ownerId}`,
        value);
    } else {
      return this.http.post(`${environment.apiUrl}/api/auth/rent_detail/${ownerId}/${selectedRentDetailId}/update`,
        value);
    }
  }

  getAllRentDetails(ownerId: number): Observable<any> {
    return this.http.get<RentDetailModel[]>(`${environment.apiUrl}/api/auth/rent_detail/${ownerId}`);
  }

  updateRentDetailStatus(status: string, renDetailId: number, ownerId: number, remarks: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/rent_detail/${ownerId}/${renDetailId}/${status}/update/${remarks}`, null);
  }

  updatePaymentTypeStatus(paymentType: any, rentDetailId: number, ownerId: number, body: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/rent_detail/${ownerId}/${rentDetailId}/${paymentType}/update_payment_type`, body);
  }

  fetchRentPayments(fiscalYearId: number,
                    month: string,
                    page: number,
                    size: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/rent/payment`, {
      params: {
        fiscal_year_id: fiscalYearId + '',
        month: month + '',
        page: page + '',
        size: size + ''
      }
    })
  }

  fetchAllRentPayments(rentPaymentId: number,
                       page: number,
                       size: number) : Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/rent/payment/detail/${rentPaymentId}`
      , {
      params: {
        rent_owner_id: '0',
        rent_detail_id: '0',
        page: page + '',
        size: size + ''
      }
    })
  }

  fetchAllRentOwnerMinDetail(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/rent/owner/owner_min_detail`);
  }

  fetchRentDetailByOwnerId(ownerId: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/rent_detail/${ownerId}/min_detail`);
  }

  addEditRentPaymentDetail(value: any,
                           eventType: string,
                           rentPaymentId: number,
                           selectedPaymentId: number) : Observable<any> {
    if(eventType === 'Add') {
      return this.http.post(`${environment.apiUrl}/api/auth/rent/payment/${rentPaymentId}/rent_payment_detail`,
        value);
    } else {
      return this.http.post(`${environment.apiUrl}/api/auth/rent/payment/${rentPaymentId}/rent_payment_detail/${selectedPaymentId}`,
        value);
    }
  }

  updateRentPaymentDetail(status: string, rentPaymentDetailId: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/rent/payment/detail/${rentPaymentDetailId}/${status}/status`,
      null);
  }

  creatRentPayment(detail: any, rentPaymentId: number): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/rent/payment/${rentPaymentId}/create_payment_request`,
      detail);
  }

  downloadSampleDoc(rentPaymentId: number) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      'Accept': 'application/octet-stream'
    })

    return this.http.get(`${environment.apiUrl}/api/auth/rent/payment/${rentPaymentId}/download`,
      { headers:headers, responseType: 'blob'})
  }

  fetchAllRentMemos(memoCode: string, page: number, size: number) : Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/auth/rent/payment/memos`
      , {
        params: {
          memo_code: memoCode,
          page: page + '',
          size: size + ''
        }
      })
  }
}
