export class RentOwner {
  rentOwnerId: number;
  createdDate: any;
  name: string;
  address: string;
  contactNumber: string;
  panNumber: string;
  status: string;
}
