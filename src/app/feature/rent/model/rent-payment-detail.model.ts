export class RentPaymentDetailModel {
  rentPaymentDetailId: number;
  created: any;
  debitBranch: string;
  debitBranchId: number;
  accountHead: string;
  accountHeadId: number;
  creditBranch: string;
  creditBranchId: number;
  creditAccountNumber: string;
  amount: number;
  tdsAmount: number;
  narration: string;
  status: string;
  rentDetail: string;
  rentDetailId: number;
  rentOwnerDetail: string;
  rentOwnerDetailId: number;
  rentFor: string;

}
