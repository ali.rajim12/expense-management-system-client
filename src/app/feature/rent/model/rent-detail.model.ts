export class RentDetailModel {
  rentDetailId: number;
  createdDate: any;
  rentDetail: string;
  rentDescription: string;
  rentForType: any;
  incrementPeriod: any;
  rentIncrementType: any;
  incrementOn: any;
  value: number;
  rentStartDate: string;
  baseRentAmount: number;
  taxPercentage: number;
  expiryDate: any;
  status: any;
  branch: string;
  accountHead: string;
  rentOwnerDetail: string;
  rentDocumentResources: any;
  taxAccountNumber: string;
  paymentType: any;
  tdsPaymentType: any;
  prePaidMonth: number;
  expenseNature: string;
  prePaidDays: number;
  numberOfPaidEMIs: number;
}
