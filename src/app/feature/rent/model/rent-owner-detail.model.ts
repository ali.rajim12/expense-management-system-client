export class RentOwnerDetail {
  rentOwnerId: number;
  createdDate: any;
  name: string;
  address: string;
  contactNumber: string;
  panNumber: string;
  status: string;
  natureOfPayment: string;
  accountNumber: string;
}
