export class RentPaymentModel {
  rentPaymentId: number;
  created: any;
  remarks: string;
  createdBy: string;
  status: any;
  fiscalYear: string;
  month: string;
}
