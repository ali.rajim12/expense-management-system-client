export class RentDocumentModel {
  fileName: string;
  originalName: string;
  rentDetailId: number;
  status: string;
  remarks: string;
  fileUrl: string
}
