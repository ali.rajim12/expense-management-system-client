import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ExpenditureType} from "../../pre-approval/models/expenditure-type.model";
import {Month} from "../../pre-approval/models/month.model";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {Vendor} from "../../vendors/model/vendor.model";
import {UserMini} from "../../users/model/user-mini.model";
import {ActivatedRoute, Router} from "@angular/router";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {VendorsService} from "../../vendors/service/vendors.service";
import {UserService} from "../../users/service/user.service";
import {PeriodicPaymentService} from "../../periodic-payment/service/periodic.payment.service";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {RentService} from "../owner-list/service/rent.service";


@Component({
  selector: 'app-create-rent-memo',
  templateUrl: './create-rent-memo.component.html',
  styleUrls: ['./create-rent-memo.component.css']
})
export class CreateRentMemoComponent implements OnInit {
  public Editor = ClassicEditor;

  createPaymentRequestForm: FormGroup;
  rentPaymentId: number;
  requestedDate = new Date();
  billingDate = new Date();
  files: string[] = [];
  submitted = false;
  loading: boolean = false;
  minDate: Date;
  expenditureTypes: ExpenditureType[];
  months: Month[];
  fiscalYears: FiscalYear[];
  showConfirmationPage: boolean = false;
  approver: UserMini[];
  supporter: UserMini[];

  constructor(private fb: FormBuilder,
              private activeRoute: ActivatedRoute,
              private preApprovalRequestService: PreApprovalService,
              private userService: UserService,
              private router: Router,
              private customMessageService: CustomMessageService,
              private rentService: RentService) {
    this.rentPaymentId = this.activeRoute.snapshot.params.rentPaymentId;
    this.createPaymentRequestForm = this.fb.group({
      requestedDate: [this.requestedDate, Validators.required],
      expenditureType: [null, Validators.required],
      month: [null, Validators.required],
      fiscalYear: [null, Validators.required],
      subject: [null, Validators.required],
      expenseDetail: [null, Validators.required],
      link: [null],
      supporters: null,
      approvers: [null, Validators.required]
    });
  }
  get f() {
    return this.createPaymentRequestForm.controls;
  }

  ngOnInit(): void {
    this.fetchAllExpenditureTypes();
    this.fetchAllMonths();
    this.fetchAllFiscalYears();
  }


  fetchAllMonths() {
    this.preApprovalRequestService.getAllMonthsForAddEdit(false)
      .subscribe((response) => {
        this.months = response;
      }, error1 => console.log(error1));
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  fetchAllExpenditureTypes() {
    this.preApprovalRequestService.getAllExpenditureTypes()
      .subscribe((response) => {
        this.expenditureTypes = response;
      }, error1 => console.log(error1));
  }

  searchSupporter(event: any) {
    this.userService.getAllSupporter(event.query).subscribe(data => {
      this.supporter = data;
    });
  }

  searchApprover(event: any) {
    this.userService.getAllApprovers(event.query).subscribe(data => {
      this.approver = data;
    });
  }
  confirmCreateRequest() {
    this.submitted = true;
    if(this.createPaymentRequestForm.invalid) {
      return;
    }
    this.showConfirmationPage = true;
  }

  onSubmit() {
    this.loading = true;
    // const formData = new FormData();
    // for (var i = 0; i < this.files.length; i++) {
    //   formData.append('files', this.files[i]);
    // }
    // console.log(this.createPaymentRequestForm.value);
    let detail = {
      requestedDate: this.createPaymentRequestForm.value.requestedDate,
      expenditureType: this.createPaymentRequestForm.value.expenditureType.type,
      month: this.createPaymentRequestForm.value.month.months,
      fiscalYearId: this.createPaymentRequestForm.value.fiscalYear.id,
      subject: this.createPaymentRequestForm.value.subject,
      expenseDetail: this.createPaymentRequestForm.value.expenseDetail,
      link: this.createPaymentRequestForm.value.link,
      supportedByUserIds: this.getSupportedBYUserIds(),
      approvedByUserIds: this.getApprovedByUsersIds()
    };
    // formData.append('detail', JSON.stringify(detail));

    this.rentService.creatRentPayment(detail,
      this.rentPaymentId)
      .subscribe(
        data => {
          this.showConfirmationPage = false;
          this.router.navigate(['/bill-payment/all']);
          this.customMessageService.sendSuccessErrorMessage(
            data.success_message,
            'success', true);
        },
        error => {
          this.showConfirmationPage = false;
          this.loading = false;
          this.customMessageService.sendSuccessErrorMessage(error, 'error',
            true);
        });

  }

  private getSupportedBYUserIds() {
    let userIds: number[] = [];
    if(this.createPaymentRequestForm.value.supporters !== null) {
      this.createPaymentRequestForm.value.supporters.forEach(function (value) {
        userIds.push(value.userId);
      });
    }
    return userIds;
  }

  private getApprovedByUsersIds() {
    let userIds: number[] = [];
    this.createPaymentRequestForm.value.approvers.forEach(function (value) {
      userIds.push(value.userId);
    });
    return userIds;
  }

  renderForm(event: any) {
    this.createPaymentRequestForm.controls.registrationNumber.patchValue(event.registrationNumber);
    this.createPaymentRequestForm.controls.vendorAccountNumber.patchValue(event.accountNumber);
    this.createPaymentRequestForm.controls.registrationType.patchValue(event.registrationType);

    this.createPaymentRequestForm.controls.registrationNumber.disable();
    this.createPaymentRequestForm.controls.vendorAccountNumber.disable();
    this.createPaymentRequestForm.controls.registrationType.disable();
  }

  goTo() {
    this.createPaymentRequestForm.reset();
   // this.router.navigate(['/periodic/payments', this.periodicPaymentTypeId]);
  }

}
