import {Component, OnInit} from '@angular/core';
import {PeriodPaymentDetail} from "../../periodic-payment/model/period.payment.detail.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Branch, BranchService} from "../../branch";
import {AccountHead, AccountHeadService} from "../../account-head";
import {CommonVariableService} from "../../../shared";
import {PeriodicPaymentService} from "../../periodic-payment/service/periodic.payment.service";
import {ActivatedRoute} from "@angular/router";
import {CustomMessageService} from "../../../shared/service/custom-message.service";
import {ColumnName} from "../../../shared/constant/column-name";
import * as FileSaver from "file-saver";
import {from} from "rxjs";
import {filter} from "rxjs/operators";
import {RentPaymentDetailModel} from "../model/rent-payment-detail.model";
import {RentService} from "../owner-list/service/rent.service";
import {RentOwnerMinDetailModel} from "../model/rent-owner.min.detail.model";
import {RentDetailMinDetailModel} from "../model/rent-detail.min.detail.model";

@Component({
  selector: 'app-rent-payment-details',
  templateUrl: './rent-payment-details.component.html',
  styleUrls: ['./rent-payment-details.component.sass']
})
export class RentPaymentDetailsComponent implements OnInit {

  columnName: any[];
  data$: RentPaymentDetailModel[];
  rentPaymentId: number;
  rentOwners: RentOwnerMinDetailModel[];
  rentDetails: RentDetailMinDetailModel[];

  amount: number = 0;
  debitBranchId: number = 0;
  creditBranchId: number = 0;
  accountNumber: string = '';
  page: number = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<number>;
  totalRecords: number;
  selectedItems: PeriodPaymentDetail;
  eventType: string;
  files: string[] = [];
  file: any;
  submitted: boolean = false;
  loading: boolean = false;
  display: boolean = false;
  myForm: FormGroup;
  addEditRentPaymentDetailForm: FormGroup;
  allBranch: Branch[];
  accountHeads: AccountHead[];
  selectedRentPaymentId: number;

  selectedDebitBranchId: number;
  selectedCreditBranchId: number = 0;
  clearDataLoading: boolean = false;


  constructor(private commonVariableService: CommonVariableService,
              private periodicPaymentService: PeriodicPaymentService,
              private activatedRoute: ActivatedRoute,
              private customMessageService: CustomMessageService,
              private fb: FormBuilder,
              private branchService: BranchService,
              private accountHeadService: AccountHeadService,
              private rentService: RentService) {
    this.myForm = this.fb.group({
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])
    });
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
    this.rentPaymentId = this.activatedRoute.snapshot.params.rentPaymentId;
    this.addEditRentPaymentDetailForm = this.fb.group({
      rentOwnerId: [0, Validators.required],
      rentDetailId: [0, Validators.required],
      debitBranchId: [null, Validators.required],
      debitAccountHeadId: [null, Validators.required],
      creditBranchId: [null, Validators.required],
      creditAccountNumber: [null, Validators.required],
      amount: [null, Validators.required],
      tdsAmount: [null, Validators.required],
      narration: [null, Validators.required]
    })
  }

  get f() {
    return this.myForm.controls;
  }

  get ff() {
    return this.addEditRentPaymentDetailForm.controls;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.myForm.patchValue({
        fileSource: file
      });
    }
  }

  ngOnInit(): void {
    this.columnName = ColumnName.RENT_PAYMENT_DETAIL;
    this.reloadData(this.page, this.itemsPerPage);
    this.fetchAllBranch();
    this.fetchAllAccountHeads();
    this.fetchRentOwners();
  }

  fetchRentOwners() {
    this.rentService.fetchAllRentOwnerMinDetail()
      .subscribe((data) => {
        this.rentOwners = data;
      }, error => {
      })
  }

  fetchRentDetail() {
    if(this.addEditRentPaymentDetailForm.value.rentOwnerId) {
      this.rentService.fetchRentDetailByOwnerId(this.addEditRentPaymentDetailForm.value.rentOwnerId)
        .subscribe((data) => {
          this.rentDetails = data;
        }, error => {
        })
    }
  }

  reloadData(page: number, size: number) {
    this.rentService.fetchAllRentPayments(this.rentPaymentId, page, size)
      .subscribe((data) => {
        this.data$ = data.results;
        this.totalRecords = data.totalResult;
      }, error => {
      })
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  handleChange($event: any, rentPaymentDetailId: string) {
    this.rentService.updateRentPaymentDetail(
      $event.checked ? 'ACTIVE' : 'INACTIVE',
      rentPaymentDetailId).subscribe(() => {
      this.reloadData(this.page, this.itemsPerPage);
    }, error1 => {
      console.log(error1);
    });
  }


  // submit() {
  //   this.submitted = true;
  //   if (this.myForm.invalid) {
  //     return;
  //   }
  //   this.loading = true;
  //   const formData = new FormData();
  //   formData.append('files', this.myForm.get('fileSource').value);
  //   this.periodicPaymentService.uploadExcelFile(formData, this.periodicPaymentTypeId)
  //     .subscribe((data) => {
  //       this.loading = false;
  //       this.customMessageService.sendSuccessErrorMessage(
  //         data.success_message,
  //         'success',
  //         false);
  //       this.reloadData(this.page, this.itemsPerPage);
  //     }, error => {
  //       this.customMessageService.sendSuccessErrorMessage(error,
  //         'error', false);
  //       this.loading = false;
  //     })
  // }

  downloadSampleDocument() {
    this.rentService.downloadSampleDoc(this.rentPaymentId)
      .subscribe((data) => {
        FileSaver.saveAs(data, "rent_payment_detail.xlsx");
      }, error1 => {
        console.log(error1);
      });
  }

  downloadSampleData() {
    this.periodicPaymentService.downloadSampleData()
      .subscribe((data) => {
        FileSaver.saveAs(data, "account_head_list.xlsx");
      }, error1 => {
        console.log(error1);
      });
  }

  addEditNewRentPaymentDetail(eventType: string, detail: any) {
    this.display = true;
    this.eventType = eventType;
    console.log(detail);
    if (eventType === 'Edit') {
      this.selectedRentPaymentId = detail.rentPaymentDetailId;
      this.addEditRentPaymentDetailForm.controls.amount.patchValue(detail.amount);
      this.addEditRentPaymentDetailForm.controls.creditAccountNumber.patchValue(detail.creditAccountNumber);
      this.setDebitBranchId(detail.debitBranchId);
      this.addEditRentPaymentDetailForm.controls.debitBranchId.patchValue(
        this.selectedDebitBranchId
      );
      this.setCreditBranchId(detail.creditBranchId);
      this.addEditRentPaymentDetailForm.controls.creditBranchId.patchValue(
        this.selectedCreditBranchId
      );
      this.addEditRentPaymentDetailForm.controls.creditAccountNumber.patchValue(detail.creditAccountNumber);
      this.addEditRentPaymentDetailForm.controls.debitAccountHeadId.patchValue(detail.accountHeadId);
      this.addEditRentPaymentDetailForm.controls.amount.patchValue(detail.amount);
      this.addEditRentPaymentDetailForm.controls.tdsAmount.patchValue(detail.tdsAmount);
      this.addEditRentPaymentDetailForm.controls.narration.patchValue(detail.narration);
    }
  }

  setCreditBranchId(creditBranchId: number) {
    if (this.allBranch === null || this.allBranch.length === 0) {
      this.fetchAllBranch();
    }
    from(this.allBranch)
      .pipe(filter(value => value.branchId === creditBranchId))
      .subscribe(value => {
        this.selectedCreditBranchId = value.branchId;
      })
  }

  setDebitBranchId(debitBranchId: number) {
    if (this.allBranch === null || this.allBranch.length === 0) {
      this.fetchAllBranch();
    }
    from(this.allBranch)
      .pipe(filter(value => value.branchId === debitBranchId))
      .subscribe(value => {
        this.selectedDebitBranchId = value.branchId;
      })
  }

  fetchAllBranch() {
    this.branchService.getAllBranch().subscribe((data) => {
      this.allBranch = data;
    }, error1 => {
      console.log(error1);
    });
  }


  fetchAllAccountHeads() {
    this.accountHeadService.fetchAllAccountHead()
      .subscribe((response) => {
        this.accountHeads = response;
      }, error1 => console.log(error1));
  }

  cancel() {
    this.display = false;
    this.addEditRentPaymentDetailForm.reset();
    this.submitted = false;
    // this.addEditRentPaymentDetailForm.reset();
  }

  submitNewRentPaymentDetail() {
    this.submitted = true;
    if (this.addEditRentPaymentDetailForm.invalid) {
      return;
    }
    this.loading = true;
    this.rentService.addEditRentPaymentDetail(this.addEditRentPaymentDetailForm.value,
      this.eventType,
      this.rentPaymentId,
      this.selectedRentPaymentId).subscribe(data => {
      this.loading = false;
      this.display = false;
      this.customMessageService.sendSuccessErrorMessage(
        data.success_message,
        'success',
        false);
      this.reloadData(0, this.itemsPerPage);
    }, error => {
      this.customMessageService.sendSuccessErrorMessage(error,
        'error', false);
      this.loading = false;
      this.display = false;
    })
  }

  resetForm() {
    this.submitted = false;
    this.myForm.reset();
  }

}
