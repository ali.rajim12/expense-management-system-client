import {Component, OnInit} from '@angular/core';
import {ColumnName} from "../../../shared/constant/column-name";
import {PeriodicExpense} from "../../periodic-payment/model/periodic.expense.model";
import {RentPaymentModel} from "../model/rent-payment.model";
import {RentService} from "../owner-list/service/rent.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PreApprovalService} from "../../pre-approval/service/pre-approval.service";
import {FiscalYear} from "../../pre-approval/models/fiscal-year.model";
import {CustomMessageService} from "../../../shared/service/custom-message.service";

@Component({
  selector: 'app-rent-payment',
  templateUrl: './rent-payment.component.html',
  styleUrls: ['./rent-payment.component.sass']
})
export class RentPaymentComponent implements OnInit {

  columnName: any[];
  totalRecords: number;
  data$: RentPaymentModel[];
  fiscalYearId: number = 0;
  month: string = '';
  page: number = 0;
  itemsPerPage = 25;
  displayGenerateForm: boolean = false;
  generateRentPaymentForm: FormGroup;
  fiscalYears: FiscalYear[];
  submitted: boolean;
  loading: boolean = false;

  constructor(private rentService: RentService,
              private preApprovalRequestService: PreApprovalService,
              private fb: FormBuilder,
              private customMessageService: CustomMessageService) {
    this.generateRentPaymentForm = fb.group({
      // accountHeadId: [null, Validators.required],
      fiscalYearId: [null, Validators.required],
      remarks: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.columnName = ColumnName.RENT_PAYMENTS;
    this.reloadTable(this.page, this.itemsPerPage);
    this.fetchAllFiscalYears();
  }

  reloadTable(page: number, size: number) {
    this.rentService.fetchRentPayments(this.fiscalYearId,
      this.month, page, size)
      .subscribe((data) => {
        this.data$ = data.results;
        this.totalRecords = data.totalResult;
      }, error => {
      })
  }

  get f() {
    return this.generateRentPaymentForm.controls;
  }

  fetchAllFiscalYears() {
    this.preApprovalRequestService.getAllFiscalYears()
      .subscribe((response) => {
        this.fiscalYears = response;
      }, error1 => console.log(error1));
  }

  cancel() {
    this.displayGenerateForm = false;
    this.generateRentPaymentForm.reset();
    this.submitted = false;
    this.loading = false;
  }

  submitGenerateForm() {
    this.submitted = true;
    if(this.generateRentPaymentForm.invalid) {
      return;
    }
    this.loading = true;
    this.preApprovalRequestService.generatePaymentDetail(
      this.generateRentPaymentForm.value.fiscalYearId,
      this.generateRentPaymentForm.value.remarks)
      .subscribe((data) => {
        this.customMessageService.sendSuccessErrorMessage(
          'Generated successfully.',
          'success', true);
        this.reloadTable(this.page, this.itemsPerPage);
      }, error => {
        this.customMessageService.sendSuccessErrorMessage(error,
          'error', false);
        this.generateRentPaymentForm.reset();
      });
    this.loading = false;
    this.displayGenerateForm = false;
  }

  generateRentPaymentReport() {
    this.displayGenerateForm = true;
  }
}
