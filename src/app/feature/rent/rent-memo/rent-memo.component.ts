import { Component, OnInit } from '@angular/core';
import {ExpenditureModel} from "../../pre-approval/models/expenditure.model";
import {ReportsService} from "../../reports/service/reports.service";
import {CommonVariableService} from "../../../shared";

import {ColumnName} from "../../../shared/constant/column-name";
import {RentService} from "../owner-list/service/rent.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-rent-memo',
  templateUrl: './rent-memo.component.html',
  styleUrls: ['./rent-memo.component.sass']
})
export class RentMemoComponent implements OnInit {

  selectedItems: ExpenditureModel;
  columnName: any[];  data$: Observable<ExpenditureModel[]>;

  page: number = 0;
  itemsPerPage: number = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;

  memoCode: string = '';
  loading: boolean = false;

  constructor(private reportService: ReportsService,
              private commonVariableService: CommonVariableService,
              private rentService: RentService
             ) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.ALL_MEMO_REPORTS;
    this.reloadData(this.page, this.itemsPerPage);
  }

  searchData() {
    this.reloadData(0, this.itemsPerPage)
  }

  reloadData(page: number, size: number) {
    this.rentService.fetchAllRentMemos(
      this.memoCode,
      page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    this.page = event.page;
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  downloadExcel() {
    // this.loading = true;
    // this.reportService.fetchDataForExcel(this.fromDate,
    //   this.toDate,
    //   this.type,
    //   this.memoCode)
    //   .subscribe((data) => {
    //     this.loading = false;
    //     FileSaver.saveAs(data, "detail.xlsx");
    //   }, error1 => {
    //     this.loading = false;
    //     console.log(error1);
    //   });
  }

}
