import {RouterModule, Routes} from "@angular/router";
import {LoginOnlyLayoutComponent, MainLayoutComponent} from "../../layout";
import {TransactionEntriesComponent} from "../reports/transaction-entries/transaction-entries.component";
import {ApprovedExpenseComponent} from "../reports/approved-expense/approved-expense.component";
import {AllApprovedTransactionsComponent} from "../reports/all-approved-transactions/all-approved-transactions.component";
import {MemoPdfComponent} from "../reports/memo-pdf/memo-pdf.component";
import {NgModule} from "@angular/core";
import {OwnerListComponent} from "./owner-list/owner-list.component";
import {RentDetailComponent} from "./rent-detail/rent-detail.component";
import {RentPaymentComponent} from "./rent-payment/rent-payment.component";
import {RentPaymentDetailsComponent} from "./rent-payment-details/rent-payment-details.component";
import {CreateRentMemoComponent} from "./create-rent-memo/create-rent-memo.component";
import {RentMemoComponent} from "./rent-memo/rent-memo.component";

const routes: Routes = [
  {
    path: 'rent',
    component: MainLayoutComponent,
    children: [
      {
        path: 'owners',
        component: OwnerListComponent
      },
      {
        path: 'owner/:ownerId',
        component: RentDetailComponent
      },
      {
        path: 'payments',
        component: RentPaymentComponent
      },
      {
        path: 'payments/:rentPaymentId',
        component: RentPaymentDetailsComponent
      },
      {
        path: 'create-payment/:rentPaymentId',
        component: CreateRentMemoComponent
      },
      {
        path: 'memo',
        component: RentMemoComponent
      }
    ]
  },
  {
    path: 'doc',
    component: LoginOnlyLayoutComponent,
    children: [
      {
        path: 'pdf/:expenditureDetailId',
        component: MemoPdfComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RentRouting {
}
