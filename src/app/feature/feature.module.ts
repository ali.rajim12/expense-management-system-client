import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashBoardComponent} from './dashborad/dash-board.component';
import {CreateRequestComponent} from './pre-approval/create-request/create-request.component';
import {RequestListComponent} from './pre-approval/request-list/request-list.component';
import {RequestDetailComponent} from './pre-approval/request-detail/request-detail.component';
import {RouterModule} from '@angular/router';
import {PreApprovalRouting} from './pre-approval/pre-approval.routing';
import {DashboardRoutingModule} from './dashborad';
import {NgxPaginationModule} from 'ngx-pagination';
import {TableModule} from 'primeng/table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CalendarModule} from 'primeng/calendar';
import {PreApprovalService} from './pre-approval/service/pre-approval.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DialogModule} from 'primeng/dialog';
import {ApprovalRequestComponent} from './bill-payment/approval-request/approval-request.component';
import {BillPaymentRouting, BillPaymentService} from './bill-payment';
import {VendorsListComponent} from './vendors/vendors-list/vendors-list.component';
import {VendorsService} from './vendors/service/vendors.service';
import {VendorsRouting} from './vendors';
import {AccountHeadRouting} from './account-head/account-head.routing';
import {AccountHeadService} from './account-head/service/account-head.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {SublayoutComponent} from './pre-approval/sublayout/sublayout.component';
import {MenubarModule} from 'primeng/menubar';
import {SupportListComponent} from './pre-approval/support-list/support-list.component';
import {ApproveListComponent} from './pre-approval/approve-list/approve-list.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {UsersComponent} from './users/users.component';
import {UserRouting} from './users/user.routing';
import {UserService} from './users/service/user.service';
import {NgxDocViewerModule} from 'ngx-doc-viewer';
import {SafePipe} from '../shared/pipe/safe.pipe';
import {NestedlayoutComponent} from './bill-payment/nestedlayout/nestedlayout.component';
import {BillPaymentListComponent} from './bill-payment/bill-payment-list/bill-payment-list.component';
import {ListBranchComponent} from './branch/list-branch/list-branch.component';
import {BranchRouting, BranchService} from './branch';
import {BillPaymentSupportComponent} from './bill-payment/bill-payment-support/bill-payment-support.component';
import {BillPaymentApproveComponent} from './bill-payment/bill-payment-approve/bill-payment-approve.component';
import {AccordionModule} from 'primeng/accordion';
import {ViewBudgetDetailComponent} from './budget/view-budget-detail/view-budget-detail.component';
import {BudgetRouting} from './budget/budget.routing';
import {ViewDocComponent} from './shared/view-doc/view-doc.component';
import {SupporterApproverComponent} from './shared/supporter-approver/supporter-approver.component';
import {ApprovedBillPaymentsComponent} from './bill-payment/approved-bill-payments/approved-bill-payments.component';
import {MakePaymentComponent} from './bill-payment/make-payment/make-payment.component';
import {ExpenseDetailComponent} from './shared/expense-detail/expense-detail.component';
import {RemarksComponent} from './shared/remarks/remarks.component';
import {BillDetailComponent} from './bill-payment/approved-bill-payments/bill-detail/bill-detail.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import {SupportApproveDetailComponent} from './shared/support-approve-detail/support-approve-detail.component';
import {InputSwitchModule} from 'primeng/inputswitch';
import {ListBranchBudgetComponent} from './budget/list-branch-budget/list-branch-budget.component';
import {PaginatorModule} from 'primeng/paginator';
import {UserGroupComponent} from './users/user-group/user-group.component';
import {GroupTypeComponent} from './users/group-type/group-type.component';
import {DepartmentComponent} from './users/department/department.component';
import {DesignationsComponent} from './users/designations/designations.component';
import {AddNewUserComponent} from './users/add-new-user/add-new-user.component';
import {CheckboxModule} from 'primeng/checkbox';
import {CategoriesComponent} from './vendors/categories/categories.component';
import {AddVendorComponent} from './vendors/vendors-list/add-vendor/add-vendor.component';
import {BudgetHeadListComponent} from './budget-head/budget-head-list/budget-head-list.component';
import {BudgetHeadRouting} from './budget-head';
import {AccountHeadListComponent} from './account-head/account-head-list/account-head-list.component';
import {ListBudgetLimitComponent} from './budget/budget-limit/list-budget-limit/list-budget-limit.component';
import {AddEditBudgetLimitComponent} from './budget/budget-limit/add-edit-budget-limit/add-edit-budget-limit.component';
import {ChartModule} from 'primeng/chart';
import {ToastModule} from 'primeng/toast';
import {LoadingBarModule} from '@ngx-loading-bar/core';
import {ViewBillDetailComponent} from './shared/view-bill-detail/view-bill-detail.component';
import {DynamicDialogComponent} from './shared/dynamic-dialog/dynamic-dialog.component';
import {BudgetDetail} from './shared/budget-detail/budget-detail';
import {NumbertowordPipe} from '../shared/pipe/numbertoword.pipe';
import {TransactionDetailComponent} from './payments/transaction-detail/transaction-detail.component';
import {AccountDetail} from "./shared/dynamic-dialog/account-detail/account-detail";
import {MenuPermissionComponent} from './menus/menu-permission/menu-permission.component';
import {MenuListComponent} from './menus/menu-list/menu-list.component';
import {AddMenuComponent} from './menus/add-menu/add-menu.component';
import {MenuRoutingModules} from "./menus/menu-routing.modules";
import {TreeTableModule} from "primeng/treetable";
import {EditPermissionComponent} from './menus/edit-permission/edit-permission.component';
import {PeriodicPaymentRouting} from "./periodic-payment/periodic.payment.routing";
import {ExpenseTypeComponent} from './periodic-payment/expense-type/expense-type.component';
import {PaymentListComponent} from './periodic-payment/payment-list/payment-list.component';
import {CreatePaymentComponent} from './periodic-payment/create-payment/create-payment.component';
import {PreApprovedComponent} from './bill-payment/pre-approved/pre-approved.component';
import {InputNumberModule} from "primeng/inputnumber";
import {SearchedRequestDetailComponent} from './reports/searched-request-detail/searched-request-detail.component';
import {MemoPdfComponent} from './reports/memo-pdf/memo-pdf.component';
import {ReportsRouting} from "./reports";
import {AllMemosComponent} from './reports/all-memos/all-memos.component';
import {CheckerMakerComponent} from './shared/checker-maker/checker-maker.component';
import {ReturnedRequestComponent} from './pre-approval/returned-request/returned-request.component';
import {RejectedRequestComponent} from './pre-approval/rejected-request/rejected-request.component';
import {ReturnedComponent} from './bill-payment/returned/returned.component';
import {RejectedComponent} from './bill-payment/rejected/rejected.component';
import {TransferBudgetComponent} from './budget/transfer-budget/transfer-budget.component';
import {TransferDetailComponent} from './budget/transfer-detail/transfer-detail.component';
import {EditorModule} from "primeng/editor";
import { TransactionEntriesComponent } from './reports/transaction-entries/transaction-entries.component';
import { ApprovedExpenseComponent } from './reports/approved-expense/approved-expense.component';
import { AllApprovedTransactionsComponent } from './reports/all-approved-transactions/all-approved-transactions.component';
import {RentRouting} from "./rent/rent.routing";
import { OwnerListComponent } from './rent/owner-list/owner-list.component';
import {RentService} from "./rent/owner-list/service/rent.service";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import { ProfileComponent } from './users/profile/profile.component';
import {DropdownModule} from 'primeng/dropdown';
import {FilterService} from "./shared";
import { AggregatedBudgetExpenseComponent } from './reports/aggregated-budget-expense/aggregated-budget-expense.component';
import { MyReportsComponent } from './reports/my-reports/my-reports.component';
import { RecentStatusComponent } from './shared/recent-status/recent-status.component';
import { RentDetailComponent } from './rent/rent-detail/rent-detail.component';
import { QrGeneratorComponent } from './qr-generator/qr-generator.component';
import {QrGeneratorRoutingModule} from "./qr-generator/qr-generator-routing.module";
import {NgxQRCodeModule} from "@techiediaries/ngx-qrcode";
import {QrService} from "./qr-generator/service/qr.service";
import { RentPaymentDetailsComponent } from './rent/rent-payment-details/rent-payment-details.component';
import { CreateRentMemoComponent } from './rent/create-rent-memo/create-rent-memo.component';
import { RentMemoComponent } from './rent/rent-memo/rent-memo.component';


@NgModule({
  declarations: [
    DashBoardComponent,
    CreateRequestComponent,
    RequestListComponent,
    RequestDetailComponent,
    ApprovalRequestComponent,
    VendorsListComponent,
    SublayoutComponent,
    SupportListComponent,
    ApproveListComponent,
    UsersComponent,

    SafePipe,
    NumbertowordPipe,

    NestedlayoutComponent,
    BillPaymentListComponent,
    ListBranchComponent,
    BillPaymentSupportComponent,
    BillPaymentApproveComponent,
    ViewBudgetDetailComponent,
    ViewDocComponent,
    SupporterApproverComponent,
    ApprovedBillPaymentsComponent,
    MakePaymentComponent,
    ExpenseDetailComponent,
    RemarksComponent,
    BillDetailComponent,
    SupportApproveDetailComponent,
    ListBranchBudgetComponent,
    UserGroupComponent,
    GroupTypeComponent,
    DepartmentComponent,
    DesignationsComponent,
    AddNewUserComponent,
    CategoriesComponent,
    AddVendorComponent,
    BudgetHeadListComponent,
    AccountHeadListComponent,
    ListBudgetLimitComponent,
    AddEditBudgetLimitComponent,
    ViewBillDetailComponent,
    DynamicDialogComponent,
    BudgetDetail,
    AccountDetail,
    TransactionDetailComponent,
    MenuPermissionComponent,
    MenuListComponent,
    AddMenuComponent,
    EditPermissionComponent,
    ExpenseTypeComponent,
    PaymentListComponent,
    CreatePaymentComponent,
    PreApprovedComponent,
    SearchedRequestDetailComponent,
    MemoPdfComponent,
    AllMemosComponent,
    CheckerMakerComponent,
    ReturnedRequestComponent,
    RejectedRequestComponent,
    ReturnedComponent,
    RejectedComponent,
    TransferBudgetComponent,
    TransferDetailComponent,
    TransactionEntriesComponent,
    ApprovedExpenseComponent,
    AllApprovedTransactionsComponent,
    OwnerListComponent,
    ProfileComponent,
    AggregatedBudgetExpenseComponent,
    MyReportsComponent,
    RecentStatusComponent,
    RentDetailComponent,
    OwnerListComponent,
    QrGeneratorComponent,
    RentPaymentDetailsComponent,
    CreateRentMemoComponent,
    RentMemoComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    CommonModule,
    DashboardRoutingModule,

    PreApprovalRouting,
    BillPaymentRouting,
    VendorsRouting,
    AccountHeadRouting,
    UserRouting,
    BranchRouting,
    BudgetRouting,
    BudgetHeadRouting,
    MenuRoutingModules,
    PeriodicPaymentRouting,
    ReportsRouting,
    RentRouting,
    QrGeneratorRoutingModule,

    NgxPaginationModule,
    TableModule,
    FormsModule,
    DialogModule,
    ReactiveFormsModule,
    CalendarModule,
    ReactiveFormsModule,
    DropdownModule,

    RouterModule.forChild([]),
    MenubarModule,
    AutoCompleteModule,
    NgxDocViewerModule,
    AccordionModule,
    ConfirmDialogModule,
    InputSwitchModule,
    PaginatorModule,
    CheckboxModule,
    ToastModule,
    ChartModule,
    LoadingBarModule,
    TreeTableModule,
    InputNumberModule,
    EditorModule,
    CKEditorModule,
    NgxQRCodeModule
  ],
  exports: [
    DashBoardComponent
  ],
  providers: [
    PreApprovalService,
    VendorsService,
    AccountHeadService,
    UserService,
    BillPaymentService,
    BranchService,
    ConfirmationService,
    MessageService,
    RentService,
    FilterService,
    QrService
  ],
  entryComponents: [
    BudgetDetail
  ]
})
export class FeatureModule {
}
