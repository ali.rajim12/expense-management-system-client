import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PreApprovalDetail} from '../../pre-approval/models/Pre-approval-detail.model';
import {environment} from '../../../../environments/environment';
import {catchError, retry} from 'rxjs/operators';
import {Vendor} from '../model/vendor.model';
import {Category} from '../model/category.model';

@Injectable({
  providedIn: 'root'
})
export class VendorsService {

  constructor(private http: HttpClient) {
  }


  fetchSearchedVendor(vendorName: string): Observable<Vendor[]> {
    let params = new HttpParams();
    params = params.append('q', vendorName);
    return this.http.get<Vendor[]>(`${environment.apiUrl}/api/auth/vendors/search`, {params: params});
  }

  getAllVendors(name: string, page: number, size: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/auth/vendors`, {
      params:
        {name: name, page: page + '', size: size + ''}
    });
  }

  findAllVendorCategories(status: string): Observable<Category[]> {
    return this.http.get<Category[]>(`${environment.apiUrl}/api/auth/vendor_category`, {
      params:
        {status: status}
    });
  }

  submitVendorCategoryDetail(value: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/vendor_category`, value);
  }

  editVendorCategory(vendorCatId: number, value: any) {
    return this.http.put(`${environment.apiUrl}/api/auth/vendor_category/${vendorCatId}`, value);
  }

  createNewVendor(value: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/auth/vendors`, value);
  }

  findVendorDetailById(vendorId: string): Observable<Vendor> {
    return this.http.get<Vendor>(`${environment.apiUrl}/api/auth/vendors/${vendorId}`);
  }

  updateVendor(vendorId: string, value: any) {
    return this.http.put(`${environment.apiUrl}/api/auth/vendors/${vendorId}`, value);
  }

  fetchAllActiveVendors(): Observable<Vendor[]>  {
    return this.http.get<Vendor[]>(`${environment.apiUrl}/api/auth/vendors/all`);
  }

  updateVendorStatus(status: string, vendorId: string) {
    return this.http.put(`${environment.apiUrl}/api/auth/vendors/${vendorId}/${status}`, null);
  }
}
