import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from '../../layout';
import {VendorsListComponent} from './vendors-list/vendors-list.component';
import {CategoriesComponent} from './categories/categories.component';
import {AddVendorComponent} from './vendors-list/add-vendor/add-vendor.component';

const routes: Routes = [
  {
    path : 'vendors',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: VendorsListComponent
      },
      {
        path: 'categories',
        component: CategoriesComponent
      },
      {
        path: ':vendorId',
        component: AddVendorComponent
      },

    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorsRouting {
}
