export class Category {
  name: string;
  code: string;
  categoryId: number;
  createdDate: string;
  status: string;
}
