export class Vendor {
  vendorCategories: string;
  name: string;
  registeredAddress: string;
  presentAddress: string;
  contactPerson: string;
  contactNumber: string;
  registrationType: string;
  registrationNumber: string;
  accountNumber: string;
  status: string;
  vendorId: number;
  createdDate: string;
  panVatNumber: string;
}
