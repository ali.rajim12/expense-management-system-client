export class VendorMiniResource {
  name: string;
  address: string;
  vendorId: number;
}
