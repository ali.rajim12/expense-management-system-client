import {Component, OnInit} from '@angular/core';
import {ColumnName} from '../../../shared/constant/column-name';
import {Category} from '../model/category.model';
import {VendorsService} from '../service/vendors.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass']
})
export class CategoriesComponent implements OnInit {

  data$: Category[];
  columnName: any[];
  totalRecords: number;
  status: string = '';
  display: boolean = false;
  title: string = '';
  addVendorCategory: FormGroup;
  selectedCategoryId: number = null;

  constructor(private vendorService: VendorsService,
              private fb: FormBuilder) {
    this.addVendorCategory = fb.group({
      name: null,
      code: null
    });
  }

  ngOnInit(): void {
    this.columnName = ColumnName.VENDOR_CATEGORY_TABLE;
    this.reloadData();
  }

  reloadData() {
    this.vendorService.findAllVendorCategories(this.status)
      .subscribe((response) => {
        this.data$ = response;
        this.totalRecords = response.length;
      }, error1 => console.log(error1));
  }

  addEditVendorCategory(data: string) {
    this.display = true;
    this.title = 'Add';

  }

  editDetail(data: Category) {
    this.display = true;
    this.title = 'Edit';
    this.selectedCategoryId = data.categoryId;
    this.addVendorCategory.controls.code.patchValue(data.code);
    this.addVendorCategory.controls.name.patchValue(data.name);
  }

  cancel() {
    this.display = false;
    this.addVendorCategory.reset();
  }

  submit() {
    if(this.selectedCategoryId !== null) {
      this.vendorService.editVendorCategory(this.selectedCategoryId,
        this.addVendorCategory.value)
        .subscribe(() => {
          this.display = false;
          this.reloadData();
        }, error1 => console.log(error1));
    } else {
      this.vendorService.submitVendorCategoryDetail(this.addVendorCategory.value)
        .subscribe(() => {
          this.display = false;
          this.reloadData();
        }, error1 => console.log(error1));
    }
  }
}
