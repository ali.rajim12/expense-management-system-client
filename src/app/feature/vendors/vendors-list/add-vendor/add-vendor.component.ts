import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Category} from '../../model/category.model';
import {VendorsService} from '../../service/vendors.service';
import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.sass']
})
export class AddVendorComponent implements OnInit {
  type: string;
  vendorCategories: Category[];
  addVendorsForm: FormGroup;
  registrationTypes: string[] = ['PAN', 'VAT'];
  showConfirmationPage: boolean = false;
  categoryName: string;
  vendorId: string;
  loading: boolean = false;
  submitted: boolean = false;

  constructor(private fb: FormBuilder,
              private vendorService: VendorsService,
              private activeRoute: ActivatedRoute,
              private router: Router) {
    this.addVendorsForm = fb.group({
      vendorCategoryId: [null, Validators.required],
      name: [null, Validators.required],
      registeredAddress: null,
      presentAddress: null,
      contactPerson: [null, Validators.required],
      contactNumber: [null, Validators.required],
      registrationType: [null, Validators.required],
      registrationNumber: [null, Validators.required],
      accountNumber: [null, Validators.required],
      panVatNumber: [null, Validators.required]
    });
  }

  get f() {
    return this.addVendorsForm.controls;
  }
  ngOnInit(): void {
    this.getVendorCategories();
    this.type = this.activeRoute.snapshot.params.vendorId === 'new' ? 'Add' : 'Edit';
    this.vendorId = this.activeRoute.snapshot.params.vendorId;
    if (this.type === 'Edit') {
      this.getVendorDetailById(this.vendorId);
    }
  }

  getVendorDetailById(vendorId: string) {
    this.vendorService.findVendorDetailById(vendorId)
      .subscribe((data) => {
        of(this.vendorCategories)
          .pipe(map(results => results
            .filter(r => r.name === data.vendorCategories)))
          .subscribe(value => {
            this.addVendorsForm.controls.vendorCategoryId.patchValue(value[0].categoryId);
          });
        this.addVendorsForm.controls.name.patchValue(data.name);
        this.addVendorsForm.controls.registeredAddress.patchValue(data.registeredAddress);
        this.addVendorsForm.controls.presentAddress.patchValue(data.presentAddress);
        this.addVendorsForm.controls.contactPerson.patchValue(data.contactPerson);
        this.addVendorsForm.controls.contactNumber.patchValue(data.contactNumber);
        this.addVendorsForm.controls.registrationType.patchValue(data.registrationType);
        this.addVendorsForm.controls.registrationNumber.patchValue(data.registrationNumber);
        this.addVendorsForm.controls.accountNumber.patchValue(data.accountNumber);
        this.addVendorsForm.controls.panVatNumber.patchValue(data.panVatNumber);
      }, error1 => {
        console.log(error1);
      });
  }


  confirmAddVendor() {
    this.submitted = true;
    if (this.addVendorsForm.invalid) {
      return;
    }
    this.showConfirmationPage = true;
    this.getCategoryName();
  }

  getVendorCategories() {
    this.vendorService.findAllVendorCategories('ACTIVE')
      .subscribe((data) => {
        this.vendorCategories = data;
      }, error1 => {
        console.log(error1);
      });
  }

  cancel() {
    this.addVendorsForm.reset();
    this.router.navigate(['/vendors']);
  }

  onSubmit() {
    this.loading = true;
    if (this.type === 'Edit') {
      this.vendorService.updateVendor(this.vendorId, this.addVendorsForm.value)
        .subscribe(() => {
          this.loading = false;
          this.router.navigate(['/vendors']);
          }, error1 => {
          this.loading = false;
          console.log(error1);
          }
        );
    } else {
      this.vendorService.createNewVendor(this.addVendorsForm.value)
        .subscribe(() => {
          this.loading = false;
          this.router.navigate(['/vendors']);
          }, error1 => {
          this.loading = false;
          console.log(error1);
          }
        );
    }
  }

  getCategoryName() {
    return of(this.vendorCategories)
      .pipe(map(results => results
        .filter(r => r.categoryId === this.addVendorsForm.value.vendorCategoryId)))
      .subscribe(value => {
        this.categoryName = (value[0]).name;
      });
  }
}
