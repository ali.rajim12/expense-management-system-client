import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ExpenditureModel} from '../../pre-approval/models/expenditure.model';
import {ColumnName} from '../../../shared/constant/column-name';
import {VendorsService} from '../service/vendors.service';
import {Vendor} from '../model/vendor.model';
import {CommonVariableService} from '../../../shared';

@Component({
  selector: 'app-vendors-list',
  templateUrl: './vendors-list.component.html',
  styleUrls: ['./vendors-list.component.sass']
})
export class VendorsListComponent implements OnInit {

  data$: Observable<Vendor[]>;
  columnName: any[];
  page = 0;
  itemsPerPage = 25;
  rowsPerPageOptions: Array<any>;
  totalRecords: number;
  selectedItems: ExpenditureModel;
  name: string = '';
  checked: boolean = true;

  constructor(private vendorService: VendorsService,
              private commonVariableService: CommonVariableService,) {
    this.rowsPerPageOptions = this.commonVariableService.rowsPerPageOptions;
  }

  ngOnInit(): void {
    this.columnName = ColumnName.VENDOR_DETAIL_TABLE;
    this.reloadData(this.page, this.itemsPerPage);
  }

  reloadData(page: number, size: number) {
    this.vendorService.getAllVendors(this.name, page, size)
      .subscribe((response) => {
        this.data$ = response.results;
        this.totalRecords = response.totalResult;
      }, error1 => console.log(error1));
  }

  paginate(event) {
    const pageIndex = event.first / event.rows;
    this.reloadData(pageIndex, event.rows);
  }

  searchVendor(name: string) {
    this.name = name;
    this.reloadData(0, this.itemsPerPage);
  }

  handleChange($event: any, vendorId: string) {
    this.vendorService.updateVendorStatus($event.checked ? 'ACTIVE' : 'INACTIVE',
      vendorId).subscribe(() => {
      this.reloadData(this.page, this.itemsPerPage);
    }, error1 => {
      console.log(error1);
    });
  }


}
