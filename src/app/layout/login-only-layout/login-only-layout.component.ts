import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-only-layout',
  templateUrl: './login-only-layout.component.html',
  styleUrls: ['./login-only-layout.component.sass']
})
export class LoginOnlyLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
