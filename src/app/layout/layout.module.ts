import {NgModule} from '@angular/core';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {SideBarComponent} from './side-bar/side-bar.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import { LoginOnlyLayoutComponent } from './login-only-layout/login-only-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import {ToastModule} from 'primeng/toast';
import {LoadingBarModule} from '@ngx-loading-bar/core';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations : [
    FooterComponent,
    HeaderComponent,
    SideBarComponent,
    LoginOnlyLayoutComponent,
    MainLayoutComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild([]),
        ToastModule,
        LoadingBarModule,
        ReactiveFormsModule
    ]
})
export class LayoutModule {

}
