import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../auth';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthenticationService) { }

  ngOnInit(): void {
  }

  logout() {
    this.authService.logout();
  }
  toggle(): void {
    const portalBody = $('#portalBody');
    if ( portalBody.hasClass( 'sidebar-collapse' )) {
      portalBody.removeClass('sidebar-collapse');
      portalBody.addClass('sidebar-open');
    } else {
      portalBody.addClass('sidebar-collapse');
    } }

}
