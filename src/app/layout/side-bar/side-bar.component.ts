import {AfterViewInit, Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {MenuService} from '../../core/_services/menu.service';
import {Menu} from '../../core/components/menu';
import {Router} from '@angular/router';
import {User} from '../../core/_models/user';
import * as $ from 'jquery';
import * as AdminLte from 'admin-lte';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ReportsService} from "../../feature/reports/service/reports.service";
import {CustomMessageService} from "../../shared/service/custom-message.service";
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit, AfterViewInit  {
  menus: Menu[];
  fullName: string;
  searchRequestDetailForm: FormGroup;
  constructor(private menuService: MenuService,
              private router: Router,
              private fb: FormBuilder,
              private reportsService: ReportsService,
              private customMessageService: CustomMessageService) {
    this.searchRequestDetailForm = this.fb.group({
      referenceNumber: ''
    })
  }

  ngOnInit(): void {
    this.fullName = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user'))).getValue().fullName;
    this.reloadData();
  }

  ngAfterViewInit(): void {
    $('[data-widget="treeview"]')
    .each(function() {
      AdminLte.Treeview._jQueryInterface.call($(this), 'init');
    });
  }

  reloadData() {
    this.menuService.findAllMyMenus()
      .subscribe((response)=> {
        this.menus = response;
      }, error1 => console.log(error1));
  }

  onSubmit() {
    if(this.searchRequestDetailForm.value.referenceNumber === '') {
      return;
    }
    this.reportsService.getExpenditureDetail(this.searchRequestDetailForm.value.referenceNumber)
      .subscribe((data) => {
        if(this.searchRequestDetailForm.value.referenceNumber.includes('PR')) {
          this.router.navigate(['/pre-approval',
            data.expenditureDetailId,
            'PRE_APPROVAL']);
        } else {
          this.router.navigate(['/pre-approval',
            data.expenditureDetailId,
            'BILL_PAYMENT']);
        }
    }, error => {
        this.customMessageService.sendSuccessErrorMessage(error, 'error',
          false);
      })
  }
}

