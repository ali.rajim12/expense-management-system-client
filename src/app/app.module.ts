import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CoreModule} from './core/core.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ErrorInterceptor} from './core/_helpers/error.interceptor';
import {JwtInterceptor} from './core/_helpers';
import {FeatureModule} from './feature/feature.module';
import {AuthModule} from './auth/auth.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LoadingBarModule} from '@ngx-loading-bar/core';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { RentPaymentComponent } from './feature/rent/rent-payment/rent-payment.component';
import {DialogModule, InputSwitchModule, TableModule} from "primeng";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    RentPaymentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    FeatureModule,
    AuthModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LoadingBarHttpClientModule,
    LoadingBarModule,
    NgxQRCodeModule,
    TableModule,
    InputSwitchModule,
    FormsModule,
    DialogModule,
    ReactiveFormsModule

  ],
  providers: [
    // NumbertowordPipe,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
