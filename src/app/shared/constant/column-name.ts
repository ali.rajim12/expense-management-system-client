export class ColumnName {
  public static PRE_APPROVAL_REQUST_COLUMN_LIST = [
    {name: 'S.N', value: ''},
    {name: 'Memo Code', value: 'memoCode'},
    {name: 'Requested Branch', value: 'billPaymentRequestedBranch'},
    {name: 'Subject', value: ''},
    {name: 'Memo Date', value: 'memoDate'},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Bill Amount', value: 'billAmount'},
    {name: 'Status', value: 'currentPreApprovalStatus'},
    {name: 'Action', value: ''},
  ];

  public static BILL_APPROVAL_REQUEST_COLUMN_LIST = [
    {name: 'S.N', value: ''},
    {name: 'Memo Code', value: 'memoCode'},
    {name: 'Requested By Branch', value: ''},
    {name: 'Subject', value: ''},
    {name: 'Memo Date', value: 'memoDate'},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Bill Amount', value: 'billAmount'},
    {name: 'Status', value: 'currentBillPaymentStatus'},
    {name: 'Action', value: ''},
  ];

  public static COMMENT_COLUMN_LIST = [
    {name: 'Commented Date', value: ''},
    {name: 'Comment', value: ''},
    {name: 'Commented For', value: ''},
    {name: 'Commented By', value: ''},
    {name: 'Status', value: ''}
  ];

  public static MAKE_PAYMENT_TABLE_COLUMN_LIST = [
    {name: 'S.N'},
    {name: 'Memo Code(Bill)', value: ''},
    {name: 'Requested Branch', value: 'billPaymentRequestedBranch'},
    {name: 'Subject', value: ''},
    {name: 'Memo Date', value: ''},
    {name: 'Bill Amount', value: ''},
    {name: 'Verified By', value: ''},
    {name: 'Pushed By', value: ''},
    {name: 'Status', value: 'paymentStatus'},
    {name: 'Action', value: ''},
  ];

  public static VENDOR_DETAIL_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Name', value: 'name'},
    {name: 'Address', value: 'registeredAddress'},
    {name: 'Present Address', value: 'presentAddress'},
    {name: 'Person', value: 'contactPerson'},
    {name: 'Number', value: 'contactNumber'},
    {name: 'Account Number', value: 'accountNumber'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''},
  ];

  public static BRANCH_BUDGET_DETAIL_TABLE = [
    {name: 'S.N'},
    {name: 'Branch', value: 'branchMiniResource'},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Month', value: 'month'},
    {name: 'Account Head', value: 'accountHead'},
    {name: 'Amount', value: 'budgetAmount'},
    {name: 'Expense', value: 'expenseTillDate'},
    {name: 'Pre Approved Amount', value: 'preApprovedAmount'},
    {name: 'Action', value: ''},
  ];

  public static USER_GROUP_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Created Date', value: 'createdDate'},
    {name: 'Code', value: 'code'},
    {name: 'Description', value: 'description'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''},
  ];

  public static USER_GROUP_TYPE_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Created Date', value: 'createdDate'},
    {name: 'Name', value: 'name'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''}
  ];

  public static USER_DEPARTMENT_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Created Date', value: 'createdDate'},
    {name: 'Name', value: 'name'},
    {name: 'Code', value: 'code'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''}
  ];

  public static DESIGNATION_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Name', value: 'name'},
    {name: 'Value', value: 'value'},
  ];

  public static USER_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Name', value: 'fullName'},
    {name: 'Username', value: 'username'},
    {name: 'Department', value: 'department'},
    {name: 'Branch', value: 'branchName'},
    {name: 'Group', value: 'userGroup'},
    {name: 'Type', value: 'groupType'},
    {name: 'Number', value: 'mobileNumber'},
    {name: 'Emp Id', value: 'employeeId'},
    {name: 'Designation', value: 'designation'},
    {name: 'Parent', value: 'parent'},
    {name: 'Action', value: ''},
  ];

  public static VENDOR_CATEGORY_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Created Date', value: 'createdDate'},
    {name: 'Name', value: 'name'},
    {name: 'code', value: 'code'},
    {name: 'status', value: 'status'},
    {name: 'Action', value: ''},
  ];

  public static BUDGET_HEAD_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Created Date', value: 'createdDate'},
    {name: 'Budget Head', value: 'budgetHead'},
    {name: 'code', value: 'code'},
    {name: 'User Group', value: ''},
    {name: 'Status', value: ''},
    {name: 'Action', value: ''},
  ];

  public static ACCOUNT_HEAD_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Description', value: 'description'},
    {name: 'Code', value: 'code'},
    {name: 'Account Number', value: 'accountNumber'},
    {name: 'Budget Head', value: 'budgetHead'},
    {name: 'Status', value: 'status'},
    {name: 'Staff Account', value: 'staffAccount'},
    {name: 'Action', value: ''},
  ];


  public static BUDGET_LIMIT_TABLE = [
    {name: 'S.N', value: ''},
    {name: 'Budget Head', value: 'budgetHead'},
    {name: 'Account Head', value: 'accountHead'},
    {name: 'Group Name', value: 'groupName'},
    {name: 'Monthly Celling (%)', value: 'monthlyCelling'},
    {name: 'Previous Month Expense (%)', value: 'previousMonthExpense'},
    {name: 'Celling Amount', value: 'cellingAmount'},
    {name: 'Fixed Celling', value: 'fixedCellingAmount'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''},
  ];

  public static TRANSACTION_JOURNALS = [
    {name: 'S.N', value: ''},
    {name: 'Date', value: 'transactionDate'},
    {name: 'TxnId', value: 'cbsTxnId'},
    {name: 'From A/c', value: 'fromAccountId'},
    {name: 'To A/c', value: 'toAccountId'},
    {name: 'Amount', value: 'amount'},
    {name: 'From Branch', value: 'fromBranch'},
    {name: 'To Branch', value: 'toBranch'},
    {name: 'Attempts', value: 'attempts'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''},
  ];

  public static MENU_TABLE_HEADINGS = [
    { name: 'Name', value: 'name' },
    { name: 'Url', value: 'url' },
    { name: 'Icon', value: 'icon' },
    { name: 'Status', value: 'status' },
    { name: 'Action', value: '' },
  ];

  public static MENU_PERMISSION_TABLE_HEADINGS = [
    { name: 'Name', value: 'name' },
    { name: 'Url', value: 'url' },
    { name: 'Icon', value: 'icon' },
    { name: 'Status', value: 'status' },
  ];

  public static PERIODIC_PAYMENT_EXPENSE_TYPE = [
    { name: 'S.N', value: ''},
    { name: 'Created', value: 'createdDate'},
    { name: 'Name', value: 'expenseName' },
    { name: 'Period', value: 'period' },
    { name: 'Status', value: 'status' },
    { name: 'Action', value: '' },
    { name: 'Process', value: '' },
  ];

  public static PERIODIC_PAYMENT_DETAILS = [
    { name: 'S.N', value: ''},
    { name: 'Debit Branch', value: 'debitBranch' },
    { name: 'Account Head', value: 'debitAccountHead' },
    { name: 'Credit Branch', value: 'creditBranch' },
    { name: 'Cr Account', value: 'creditAccountNumber' },
    { name: 'Amount', value: 'amount' },
    { name: 'TDS Amt', value: 'tdsAmount' },
    { name: 'Narration', value: 'narration' },
    { name: 'Status', value: 'status' },
    { name: 'Action', value: '' },
  ];

  public static EXPENSE_DETAIL = [
    { name: 'S.N', value: ''},
    { name: 'Branch', value: 'debitBranch' },
    { name: 'Ac Head (Dr)', value: 'accountHead' },
    { name: 'Branch (Cr)', value: 'creditBranch' },
    { name: 'Cr Ac Number', value: 'creditAccount' },
    { name: 'Dr Amt', value: 'amount' },
    { name: 'TDS', value: 'tdsAmount' },
    { name: 'Cr Amt', value: 'tdsAmount' },
    { name: 'Description', value: 'expenseDescription' },
    { name: 'Budget', value: '' }
  ];

  public static TRANSFER_BUDGET_ENTRY = [
    { name: 'S.N', value: ''},
    { name: 'Requested By', value: 'requestedBy' },
    { name: 'Assigned To', value: 'assignedTo' },
    { name: 'Count', value: 'count' },
    { name: 'Remarks', value: 'remarks' },
    { name: 'Transfer Status', value: 'transferStatus' },
    { name: 'Status', value: 'status' },
    { name: 'Action', value: '' }
  ];


  public static TRANSFER_BUDGET_DETAIL = [
    { name: 'S.N', value: ''},
    { name: 'Fr. Fiscal Yr', value: 'fromFiscalYear' },
    { name: 'Month', value: 'fromMonth' },
    { name: 'Branch', value: 'fromBranch' },
    { name: 'Ac Head', value: 'fromAccountHead' },

    { name: 'To Fiscal Yr', value: 'toFiscalYear' },
    { name: 'Month', value: 'toMonth' },
    { name: 'Branch', value: 'toBranch' },
    { name: 'Ac Head', value: 'toAccountHead' },
    { name: 'Amt', value: 'amount' },
    { name: 'Status', value: 'status' },
    { name: 'Transfer Status', value: 'transferStatus' },
    { name: 'Action', value: '' },
  ];


  public static CBS_PAYMENT_ENTRIES = [
    {name: 'S.N', value: ''},
    {name: 'Txn Date', value: 'txnDate'},
    {name: 'Txn Id', value: 'txnId'},
    {name: 'Sol Id(Dr)', value: 'debitSolId'},
    {name: 'A/c(Dr)', value: 'debitAccountNumber'},
    {name: 'A/c Head', value: 'debitHead'},
    {name: 'Sol Id(Cr)', value: 'creditSolId'},
    {name: 'A/c(Cr)', value: 'creditAccountNumber'},
    {name: 'Amount', value: 'amount'},
    {name: 'Memo Code', value: 'memoCode'},
    {name: 'Paid By', value: 'paymentMadeBy'},
    {name: 'Verify By', value: 'verifyBy'},
    {name: 'Status', value: 'paymentStatus'},
    {name: 'Action', value: ''},
  ];


  public static EMS_APPROVED_MEMOS = [
    {name: 'S.N', value: ''},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Month', value: 'month'},
    {name: 'Branch Name', value: 'branchName'},
    {name: 'Sol Id', value: 'solId'},
    {name: 'Description', value: 'description'},
    {name: 'A/c Number', value: 'accountNumber'},
    {name: 'Total Expense', value: 'totalExpense'},
    {name: 'Action', value: ''}
  ];

  public static EMS_APPROVED_MEMO_DETAIL = [
    {name: 'S.N', value: ''},
    {name: 'Branch Name', value: 'branchName'},
    {name: 'Sol Id', value: 'solId'},
    {name: 'Description', value: 'description'},
    {name: 'A/c Number', value: 'accountNumber'},
    {name: 'Month', value: 'month'},
    {name: 'Total Expense', value: 'expenseAmount'},
    {name: 'Memo Code', value: 'billPaymentMemoCode'},
    {name: 'Requested On', value: 'requestedDate'},
    {name: 'Approved On', value: 'billApprovedDate'},
  ];

  public static ALL_EMS_APPROVED_MEMO_DETAIL = [
    {name: 'S.N', value: ''},
    {name: 'Branch Name', value: 'branchName'},
    {name: 'Sol Id', value: 'solId'},
    {name: 'Description', value: 'description'},
    {name: 'A/c Number', value: 'accountNumber'},
    {name: 'Month', value: 'month'},
    {name: 'Total Expense', value: 'expenseAmount'},
    {name: 'Memo Code', value: 'billPaymentMemoCode'},
    {name: 'Requested On', value: 'requestedDate'},
    {name: 'Supported On', value: 'supportedDate'},
    {name: 'Approved On', value: 'billApprovedDate'},
  ];

  public static ALL_MEMO_REPORTS = [
    {name: 'S.N', value: ''},
    {name: 'Memo Code', value: 'memoCode'},
    {name: 'Subject', value: 'subject'},
    {name: 'Status', value: ''},
    {name: 'Memo Date', value: 'memoDate'},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Bill Amount', value: 'billAmount'},
    {name: 'Action', value: ''},
  ];

  public static RENT_OWNER = [
    {name: 'S.N', value: ''},
    {name: 'Created On', value: 'createdDate'},
    {name: 'Name', value: 'name'},
    {name: 'Address', value: 'address'},
    {name: 'Contact Number', value: 'contactNumber'},
    {name: 'Pan Number', value: 'panNumber'},
    {name: 'Payment To', value: 'paymentTo'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''},
  ];

  public static EMS_AGGREGATED_REPORTS = [
    {name: 'S.N', value: ''},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Month', value: 'month'},
    {name: 'SolId', value: 'solId'},
    {name: 'Branch', value: 'branchName'},
    {name: 'Account Head', value: 'accountHead'},
    {name: 'Budget', value: 'budget'},
    {name: 'Expense', value: 'expense'},
    {name: 'Pre Approved', value: 'preApprovedAmount'},
    {name: 'Budget Ytd', value: 'ytdBudget'},
    {name: 'Expense Ytd', value: 'ytdExpense'},
    {name: 'Pre Approved Ytd', value: 'ytdPreApproved'},
    {name: 'Remaining Budget', value: ''}
  ];

  public static MY_REPORTS = [
    {name: 'S.N', value: ''},
    {name: 'Memo Date', value: 'requestDate'},
    {name: 'Memo Code', value: 'memoCode'},
    {name: 'Subject', value: 'subject'},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Amount', value: 'amount'},
    {name: 'Supported On', value: 'supportedDate'},
    {name: 'Approved On', value: 'approvedDate'},
    {name: 'Recent Status', value: 'recentStatus'},
    {name: 'Action', value: ''},
  ];

  public static MY_REPORTS_APPROVE_SUPPORT = [
    {name: 'S.N', value: ''},
    {name: 'Memo Date', value: 'requestDate'},
    {name: 'Memo Code', value: 'memoCode'},
    {name: 'Subject', value: 'subject'},
    {name: 'Fiscal Year', value: 'fiscalYear'},
    {name: 'Amount', value: 'amount'},
    {name: 'Supported On', value: 'supportedDate'},
    {name: 'Approved On', value: 'approvedDate'},
    {name: 'Recent Status', value: 'recentStatus'},
    {name: 'Request For', value: 'requestedForType'},
    {name: 'My Status', value: 'recentMyStatus'},
    {name: 'Action', value: ''},
  ];

  public static QRCODE_TABLE_HEAD = [
    {name: 'S.N', value: ''},
    {name: 'Date', value: 'createdDate'},
    {name: 'First Name', value: 'firstName'},
    {name: 'Last Name', value: 'lastName'},
    {name: 'Title', value: 'title'},
    {name: 'Street', value: 'street'},
    {name: 'Zip Code', value: 'zipCode'},
    {name: 'City', value: 'city'},
    {name: 'Country', value: 'country'},
    {name: 'Company', value: 'company'},
    {name: 'Email', value: 'email'},
    {name: 'Phone (Personal)', value: 'phonePersonal'},
    {name: 'Phone (Mobile)', value: 'phoneMobile'},
    {name: 'Phone (Business)', value: 'phoneBusiness'},
    {name: 'Website', value: 'website'},
    {name: 'Action', value: ''},
  ]

  public static RENT_DOCUMENTS = [
    {name: 'S.N', value: ''},
    {name: 'File Name', value: 'fileName'},
    {name: 'Original Name', value: 'originalName'},
    {name: 'Remarks', value: 'remarks'},
    {name: 'Status', value: 'status'},
    {name: 'Action', value: ''}
  ]

  public static RENT_PAYMENTS = [
    { name: 'S.N', value: '' },
    { name: 'Created', value: 'created' },
    { name: 'Fiscal Year', value: 'fiscalYear' },
    { name: 'Month', value: 'month' },
    { name: 'Remarks', value: 'remarks'},
    { name: 'Generated By', value: 'createdBy'},
    { name: 'Status', value: 'status'},
    { name: 'Action', value: ''}
  ]

  public static RENT_PAYMENT_DETAIL = [
    { name: 'S.N', value: ''},
    { name: 'Created', value: 'created'},
    { name: 'DR Branch', value: 'debitBranch'},
    { name: 'Account Head', value: 'accountHead'},
    { name: 'CR Branch', value: 'creditBranch'},
    { name: 'CR A/C', value: 'creditAccountNumber'},
    { name: 'Amt', value: 'amount'},
    { name: 'TDS Amt', value: 'tdsAmount'},
    { name: 'Narration', value: 'narration'},
    { name: 'Rent For', value: 'rentFor'},
    { name: 'Status', value: 'status'},
    { name: 'Has Increased', value: 'hasRentIncreased'},
    { name: 'Action', value: ''}
  ]





}
