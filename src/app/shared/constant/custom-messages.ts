export class CustomMessages {
  public static PRE_APPROVAL_REQUEST_CREATED_SUCCESSFULLY = "Pre approval request created successfully.";
  public static BILL_PAYMENT_REQUEST_CREATED_SUCCESSFULLY = "Bill Payment request created successfully.";
  public static PERIODIC_PAYMENT_TYPE_CREATED_SUCCESSFULLY = "Periodic payment type created successfully.";
  public static PERIODIC_PAYMENT_TYPE_UPDATED_SUCCESSFULLY = "Periodic payment type updated successfully.";
  public static ACCOUNT_HEAD_ADDED_SUCCESSFULLY = "Account Head Added successfully.";
  public static ACCOUNT_HEAD_UPDATED_SUCCESSFULLY = "Account Head Updated successfully.";
}
