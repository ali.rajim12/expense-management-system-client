import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {NavigationStart, Router} from '@angular/router';
import {ViewportScroller} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class CustomMessageService {
  private subject = new Subject<any>();
  private keepAfterNavigationChange = false;

  constructor(private router: Router,
              private viewportScroller: ViewportScroller) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterNavigationChange) {
          this.keepAfterNavigationChange = false;
        } else {
          this.subject.next();
        }
      }
    });
  }
  sendSuccessErrorMessage(message: string, type: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    if(type === 'error') {
      this.viewportScroller.scrollToPosition([0, 0]);
    }
    this.subject.next({type, text: message});
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
  clearMessages() {
    this.subject.next();
  }
}
