import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonVariableService {

  private rowPerPageOptions: Array<number>;

  get rowsPerPageOptions(): Array<number> {
    return this.rowPerPageOptions;
  }

  set rowsPerPageOptions(value: Array<number>) {
    this.rowPerPageOptions = value;
  }

  constructor() {
    this.rowPerPageOptions = [20, 25, 50, 100, 200];
  }
}
