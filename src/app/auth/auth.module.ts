import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthRouting} from './auth-routing';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    AuthRouting,
    ReactiveFormsModule,

  ],
  exports: [
    LoginComponent
  ]
})
export class AuthModule {

}
