import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LoginOnlyLayoutComponent} from '../layout/login-only-layout/login-only-layout.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginOnlyLayoutComponent,
    children: [
      {path: '', component: LoginComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRouting {
}
